@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}
<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('sweet-alert-notification')
        
      </div>
    </div>
      
</div>
@endsection

@push('scripts')
<script>
  
window.addEventListener('swal:modal', event => { 
    swal({
      title: event.detail.message,
      text: event.detail.text,
      icon: event.detail.type,
    });
});
  
window.addEventListener('swal:confirm', event => { 
    swal({
      title: event.detail.message,
      text: event.detail.text,
      icon: event.detail.type,
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.livewire.emit('remove');
      }
    });
});
 </script>
 @endpush