@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}
<div class="container">
  
    <div class="card">
      <div class="card-body">
        <h2>Add Student Skills:</h2>
        @livewire('student-skill',['student_id'=>$student_id])
        
      </div>
    </div>
      
</div>
@endsection