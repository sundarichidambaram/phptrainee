<div>
  
 <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step {{ $currentStep != 1 ? 'btn-default' : 'btn-success' }}">
            <a href="#step-1" type="button" class="btn btn-circle {{ $currentStep != 1 ? 'btn-default' : 'btn-primary' }}">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step {{ $currentStep != 2 ? 'btn-default' : 'btn-success' }}">
            <a href="#step-2" type="button" class="btn btn-circle {{ $currentStep != 2 ? 'btn-default' : 'btn-primary' }}">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step {{ $currentStep != 3 ? 'btn-default' : 'btn-success' }}">
            <a href="#step-3" type="button" class="btn btn-circle {{ $currentStep != 3 ? 'btn-default' : 'btn-primary' }}" >3</a>
            <p>Step 3</p>
        </div>
        <div class="stepwizard-step {{ $currentStep != 4 ? 'btn-default' : 'btn-success' }}">
            <a href="#step-4" type="button" class="btn btn-circle {{ $currentStep != 4 ? 'btn-default' : 'btn-primary' }}">4</a>
            <p>Step 4</p>
        </div>
        <div class="stepwizard-step {{ $currentStep != 5 ? 'btn-default' : 'btn-success' }}">
            <a href="#step-5" type="button" class="btn btn-circle {{ $currentStep != 5 ? 'btn-default' : 'btn-primary' }}" disabled="disabled">5</a>
            <p>Step 5</p>
        </div>
    </div>
</div> 
  {{-- @if($students==null) --}}
    <div class="row setup-content {{ $currentStep != 1 ? 'displayNone' : '' }}" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 1</h3>
                @if($students!=null)
                
                <span class="text-primary">You Already filled</span>
                <button class="btn btn-primary nextBtn btn-lg pull-right"  wire:click="firstStepSubmit" type="button" >Next</button>
                @else           
                <div class="form-group">
                    <label for="title">Student Name:</label>
                    <input type="text" wire:model="name" class="form-control" id="taskTitle">
                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="description">Email:</label>
                    <input type="text" wire:model="email" class="form-control" id="productAmount"/>
                    @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
               <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="firstStepSubmit" type="button" >Next</button>
                 @endif  
            </div>
        </div>
    </div>
    {{-- @endif --}}
    <div class="row setup-content {{ $currentStep != 2 ? 'displayNone' : '' }}" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 2</h3>
                @if($students!=null)   
                  @if($students->mobileno!=null)
                 <span class="text-primary">You Already filled</span>
                 <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" wire:click="secondStepSubmit">Next</button>
                 <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(1)">Back</button>
                 @else
                <div class="form-group">
                    <label for="title">Contact No:</label>
                    <input type="number" wire:model="contact" class="form-control">
                    @error('contact') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
  
                <div class="form-group">
                    <label for="description">Gender</label><br/>
                    <label class="radio-inline"><input type="radio" wire:model="gender" value="male" {{{ $gender == 'male' ? "checked" : "" }}}> Male</label>
                    <label class="radio-inline"><input type="radio" wire:model="gender" value="female" {{{ $gender == 'female' ? "checked" : "" }}}> Female</label>
                    @error('gender') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
  
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" wire:click="secondStepSubmit">Next</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(1)">Back</button>
                @endif
                 @endif
            </div>
        </div>
    </div>
    <div class="row setup-content {{ $currentStep != 3 ? 'displayNone' : '' }}" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 3</h3>
                @if($students!=null)
                 @if($students->address!=null)
                <span class="text-primary">You Already filled</span>
                <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="thirdStepSubmit" type="button">Next</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(2)">Back</button>
                @else 
                  <div class="form-group">
                    <label for="description">Address:</label>
                    <textarea type="text" wire:model="address" class="form-control" >{{{ $address ?? '' }}}</textarea>
                    @error('address') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
               
                <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="thirdStepSubmit" type="button">Next</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(2)">Back</button>
                 @endif 
                 @endif
            </div>
        </div>
    </div>

     <div class="row setup-content {{ $currentStep != 4 ? 'displayNone' : '' }}" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 4</h3>
                @if($students!=null)
                 @if($students->image!=null)
                <span class="text-primary">You Already filled</span>
                 <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="fourthStepSubmit" type="button">Next</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(3)">Back</button>
                @else 
                  <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" wire:model="photo" class="form-control" id="productAmount"/>
                    @error('photo') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
  
                <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="fourthStepSubmit" type="button">Next</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(3)">Back</button>
               @endif 
               @endif
            </div>
        </div>
    </div>

     <div class="row setup-content {{ $currentStep != 5 ? 'displayNone' : '' }}" id="step-5">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 5</h3>
                @if($students!=null)
                @if($students->url!=null)
                <span class="text-primary">You Already filled</span>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(4)">Back</button>
                @else 
                <div class="form-group">
                    <label for="title">Url Name:</label>
                   <textarea type="text" wire:model="url_name" class="form-control" id="taskDescription">{{{ $url_name ?? '' }}}</textarea>
                    @error('url_name') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
             
                <button class="btn btn-success btn-lg pull-right" wire:click="submitForm" type="button">Finish!</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(4)">Back</button>
                @endif
                @endif 
            </div>
        </div>
    </div>

</div>
