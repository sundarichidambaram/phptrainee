{{-- <div>
    <h1>Image Upload</h1>
<form wire:submit.prevent="submit" method="post" enctype="multipart/form-data">
    @if ($photo)
        Photo Preview:
        <img src="{{ $photo->temporaryUrl() }}">
    @endif
 
    <input type="file" wire:model="photo" name="photo">
 
    @error('photo') <span class="error">{{ $message }}</span> @enderror
 
    <button type="submit">Save Photo</button>
</form>
</div> --}}


<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h2 id="heading">Sign Up Your User Account</h2>
                <p>Fill all form field to go to next step</p>
                <div id="msform">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="account"><strong>Info</strong></li>
                        <li id="personal"><strong>Personal</strong></li>
                        <li id="payment"><strong>Image</strong></li>
                        <li id="confirm"><strong>Address</strong></li>
                    </ul>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br> <!-- fieldsets -->
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Student Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 1 - 4</h2>
                                </div>
                            </div> 
                            @if($students!=null)
                            <span class="text-success">You Already filled</span>
                            <button wire:click="firstStepSubmit" name="next" class="next action-button" value="Next" />Next</button>
                            @else
                             <label class="fieldlabels">Name: *</label> 
                             <input type="text" wire:model="name" name="uname" placeholder="UserName" />
                              @error('name') <span class="text-danger">{{ $message }}</span> @enderror   
                            <label class="fieldlabels">Email: *</label>
                             <input type="email" wire:model="email" name="email" placeholder="Email Id" />
                              @error('email') <span class="text-danger">{{ $message }}</span> @enderror                            
                            {{-- <label class="fieldlabels">Password: *</label>
                              <input type="password" name="pwd" placeholder="Password" /> --}}
                         </div>
                         <button wire:click="firstStepSubmit" name="next" class="next action-button" value="Next" />Next</button>
                         @endif
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Personal Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 2 - 4</h2>
                                </div>
                            </div> 
                            <label class="fieldlabels">Contact No.: *</label> 
                            <input type="text" name="phno" placeholder="Contact No." /> 

                        </div> <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Image Upload:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 3 - 4</h2>
                                </div>
                            </div> <label class="fieldlabels">Upload Your Photo:</label>
                             <input type="file" wire:model="photo" name="pic" accept="image/*"> 
                        </div> <input type="button" name="next" class="next action-button" value="Submit" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                    </fieldset>


                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Address:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 4 - 4</h2>
                                </div>
                            </div> <label class="fieldlabels">Address:</label> 
                            <textarea type="text" wire:model="address"></textarea>
                            <!-- <input type="file" name="pic" accept="image/*">  -->
                        </div> <input type="button" name="next" class="next action-button" value="Submit" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>