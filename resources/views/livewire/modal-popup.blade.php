
<!-- Modal -->
<div>
<div wire:ignore.self class="modal fade" id="contactModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Add Contacts</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      
      <form wire:submit.prevent="saveContact">
      <div class="modal-body">

        <div class="mb-3">
        <label>Name</label>
        <input type="text" class="form-control" placeholder="Enter name" wire:model="name">
       @error('name') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="mb-3">
        <label>Email</label>
        <input type="text" class="form-control" placeholder="Enter email" wire:model="email">
        @error('email') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="mb-3">
        <label>MobileNo</label>
        <input type="text" class="form-control"  placeholder="Enter mobileno" wire:model="mobileno">
        @error('mobileno') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  
    <div class="mb-3">
        <label>Address</label>
        <textarea class="form-control" placeholder="Enter Address" wire:model="address"></textarea>
        @error('address') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="mb-3">
        <label>Date Of Birth</label>
         <input type="date" class="form-control" wire:model="date_of_birth">
        @error('date_of_birth') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
        <button type="submit" value="save" class="btn btn-primary">save</button> 
      </div>
  </form>

    </div>
  </div>
</div>
</div>