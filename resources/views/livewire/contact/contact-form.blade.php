
<div>
    @if(!$contact_id=='')
     <h1>Edit Contact</h1>
    <form wire:submit.prevent="update" >
        @csrf
        @else
           <h1>Add Contact</h1>
    <form wire:submit.prevent="submit" >
        @endif
        <input type="hidden" class="form-control" id="exampleInputName"  wire:model="contact_id">
    <div class="form-group">
        <label for="exampleInputName">Name</label>
        <input type="text" class="form-control" id="exampleInputName" placeholder="Enter name" wire:model="name">
       @error('name') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputEmail">Email</label>
        <input type="text" class="form-control" id="exampleInputEmail" placeholder="Enter email" wire:model="email">
        @error('email') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputMobile">MobileNo</label>
        <input type="text" class="form-control" id="exampleInputMobile" placeholder="Enter mobileno" wire:model="mobileno">
        @error('mobileno') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
  
    <div class="form-group">
        <label for="exampleInputbody">Address</label>
        <textarea class="form-control" id="exampleInputbody" placeholder="Enter Address" wire:model="address"></textarea>
        @error('address') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

   {{-- <div class="form-group">
            <div class="g-recaptcha" wire:model="g_recaptcha" data-sitekey="6Ld1EOcjAAAAAC0uzoObDlWUuDuZ2HKZPTgW_HT8" name="g_recaptcha"></div>
        <span style="color:red">
            @if(\Session::has('g_recaptcha'))
                {{\Session::get('g_recaptcha')}}
                
            {{\Session::forget('g_recaptcha')}}
                @endif
            </span>
    </div> --}}

    <div class="form-group">
        <label for="exampleInputbody">Date Of Birth</label>
         <input type="date" class="form-control" id="exampleInputDate" wire:model="date_of_birth">
        @error('date_of_birth') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="form-group">
        <label for="exampleInputName">Image</label>
        <input type="file" class="form-control" id="exampleInputName" wire:model="photo" name="photo">
       @error('photo') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
@if(!$contact_id=='')
     <img src="{{   asset('storage/'.$photo )}}" width="80" height="90">   
@else 
<div>
     @if ($photo)
        Photo Preview:
        <img src="{{ $photo->temporaryUrl() }}" width="80" height="90">
    @endif
</div>
@endif
    <button type="submit" class="btn btn-primary">Save Contact</button>
</form>
</div>
