

<div>
     <h1>Country and State Details</h1>
    <form wire:submit.prevent="submit" >
        @csrf
    {{--<div class="form-group row">
        <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

        <div class="col-md-6">
            <select wire:model="selectedCountry" class="form-control">
                <option value="" selected>Choose country</option>
                @foreach($countries as $country)
                    <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                @endforeach
            </select>
        </div>
    </div> --}}

    
        <div class="form-group row">
            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

            <div class="col-md-6">
                <select wire:model="selectedState" class="form-control">
                    <option>Choose state</option>
                    @foreach($states as $state)
                        <option value="{{ $state->id }}">{{ ucfirst($state->name) }}</option>
                    @endforeach
                </select>
            </div>

        </div>

    <div class="form-group row">
        <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

        <div class="col-md-6">
            <select wire:model="selectedCity" class="form-control">
                <option value="" selected>Choose city</option>
                @foreach($cities as $city)
                    <option value="{{ $city->id }}">{{ ucfirst($city->name) }}</option>
                @endforeach
            </select>

             <button  type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>    

    
</div>
