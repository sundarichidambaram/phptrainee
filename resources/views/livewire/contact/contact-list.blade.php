<div>
    
    <h1>Contact Lists:</h1>
    <a href="{{url('/admin/contact')}}">
      <button class="btn btn-primary btn-sm">Add</button>
   </a>
   <a href="{{url('/admin/popup')}}">
      <button class="btn btn-primary btn-sm">Add Contact</button>
   </a>

    <button wire:click="restoreall" class="btn btn-primary btn-sm">RestroeAll</button>
    
   <a href="{{url('/admin/restore/list')}}">
     <button class="btn btn-primary btn-sm">Delete list</button>
   </a>
   

    <form wire:submit.prevent="submit" >
        @csrf
    <input wire:model="search" type="search" id="search" name="search">
    <label>From Date</label>
    <input wire:model="start_date" type="date" name="start_date">
    <label>To Date</label>
    <input wire:model="end_date" type="date" name="end_date">
    @error('end_date') <span class="text-danger">{{ $message }}</span> @enderror

     <input type="submit" value="submit">

</form><br>
 
    <table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Mobileno</th>
      <th scope="col">Address</th>
      <th scope="col">Date Of Birth</th>
      <th scope="col">Image</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  @if($contacts->count() > 0)
        @foreach($contacts as $value)
  <tbody>
    <tr>
      <td>{{$value->name}}</td>
      <td>{{$value->email}}</td>
      <td>{{$value->mobileno}}</td>
      <td>{{$value->address}}</td>
      <td>{{$value->dob}}</td>
      @if($value->image)
       <td><img src="{{  url('storage/'. $value->image) }}" width="50" height="60"></td> 
      @else
        <td>No Images</td>
      @endif  
       
      <td>
        <a href="{{url('/admin/edit/contact',['id'=>$value->id])}}">
        <button class="btn btn-primary btn-sm">Edit</button>
        </a>
        {{-- <a href="{{url('/admin/delete/contact/'.$value->id)}}">
        <button class="btn btn-danger btn-sm">Delete</button>
        </a> --}}

        <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm">Delete</button>
     </td>
    </tr>
  </tbody>
   @endforeach

     @else
        <tr>
          <td colspan="4" class="text-center text-base">No Records Found</td>
        </tr>
        @endif
        
</table>
{{--{{$contacts->links()}} --}}
 <!-- <div>
     
 </div> -->
        
</div>

 

 
   

