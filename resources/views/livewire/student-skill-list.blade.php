<div>

<h1>Student Skill Lists:</h1>

    <a href="{{url('/admin/form/list')}}">
      <button class="btn btn-secondary btn-sm">Student List</button>
   </a>
     @if($skill_id)
       <button wire:click="deleteall" class="btn btn-danger btn-sm">DeleteSelected({{count($skill_id)}})</button>
     @endif
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col"><input type="checkbox" wire:model="selectAll">SelectAll</th>
      <th scope="col">Course Name</th>
      <th scope="col">Image</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  @if($skill->count() > 0)
        @foreach($skill as $value)
  <tbody>
    <tr>
      <td><input type="checkbox" value="{{$value->id}}" wire:model="skill_id"></td>  
      <td>{{$value->course_name}}</td>
      @if($value->image)
       <td><img src="{{  url('storage/'. $value->image) }}" width="50" height="60"></td> 
      @else
        <td>No Images</td>
      @endif 
      <td>
        <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm">Delete</button>
</td>
    </tr>
  </tbody>
   @endforeach

     @else
        <tr>
          <td colspan="6" class="text-center text-base">No Records Found</td>
        </tr>
        @endif
        
</table>

</div>
