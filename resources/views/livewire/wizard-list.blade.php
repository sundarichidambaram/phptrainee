<div>
    
    <h1>Student Lists:</h1>
    <a href="{{url('/admin/student/detail')}}">
      <button class="btn btn-primary btn-sm">Add</button>
   </a>

    {{-- <a href="{{$students->url}}"><i class="fa fa-facebook-f" style="font-size:24px"></i></a> --}}
 
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Mobileno</th>
      <th scope="col">Address</th>
      <th scope="col">gender</th>
      <th scope="col">Image</th>
      <th colspan="3">Action</th>
    </tr>
  </thead>
  @if($student->count() > 0)
        @foreach($student as $value)
  <tbody>
    <tr>
      <td>{!!$value->present()->fullName!!}</td>
      <td>{{$value->email}}</td>
      <td>{{$value->present()->mobilenumber}}</td>
      <td>{{$value->present()->firstAddress}}</td>
      <td>{{$value->gender}}</td>
      {{--<td>{{$value->present()->getFilePathForDisplay()}}</td>--}}
      @if($value->image)
       <td><img src="{{  url('fileupload/'. $value->image) }}" width="50" height="60"></td> 
      @else
        <td>No Images</td>
      @endif 
       
      <td>
        <a href="{{url('/admin/student/info')}}">
        <button class="btn btn-primary btn-sm">Edit</button>
        </a>
     </td>
     <td>
        <a href="{{url('/admin/add/skill/'.$value->id)}}">
        <button class="btn btn-success btn-sm">Skills</button>
        </a>
     </td>
     <td>
        <a href="{{url('/admin/skill/list/'.$value->id)}}">
        <button class="btn btn-secondary btn-sm">Skill List</button>
        </a>
     </td>
    </tr>
  </tbody>
   @endforeach

     @else
        <tr>
          <td colspan="6" class="text-center text-base">No Records Found</td>
        </tr>
        @endif
        
</table>
{{--{{$student->links()}} --}}
 <!-- <div>
     
 </div> -->
        
</div>

 

 
   



 

 
   

