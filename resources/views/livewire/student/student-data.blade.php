 <div>
    @include('livewire.student.multi-step')

 {{-- @if($students==null) --}}
    <div class="row setup-content {{ $currentStep != 1 ? 'displayNone' : '' }}" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 1</h3><br>
                @if($students!=null)
                
                <span class="text-primary">You Already filled</span>
                <button class="btn btn-primary nextBtn btn-lg pull-right"  wire:click="firstStepSubmit" type="button" >Next</button>
                @else           
                <div class="form-group">
                    <label for="title">Student Name:</label>
                    <input type="text" wire:model="name" class="form-control" id="taskTitle">
                    @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="description">Email:</label>
                    <input type="text" wire:model="email" class="form-control" id="productAmount"/>
                    @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                
               <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="firstStepSubmit" type="button" >Next</button>
             

                 @endif  
            </div>
        </div>
    </div>
    {{-- @endif --}}
</div>
