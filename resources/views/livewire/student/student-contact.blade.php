 <div>
    @include('livewire.student.multi-step')

<div class="row setup-content {{ $currentStep != 2 ? 'displayNone' : '' }}" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 2</h3><br>
               {{-- @if($students!=null)   
                  @if($students->mobileno!=null)
                 <span class="text-primary">You Already filled</span>
                 <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" wire:click="secondStepSubmit">Next</button>
                 <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(1)">Back</button>
                 @else --}}
                <div class="form-group">
                    <label for="title">Contact No:</label>
                    <input type="number" wire:model="contact" class="form-control">
                    @error('contact') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
  
                <div class="form-group">
                    <label for="description">Gender</label><br/>
                    <label class="radio-inline"><input type="radio" wire:model="gender" value="male" {{{ $gender == 'male' ? "checked" : "" }}}> Male</label>
                    <label class="radio-inline"><input type="radio" wire:model="gender" value="female" {{{ $gender == 'female' ? "checked" : "" }}}> Female</label>
                    @error('gender') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
  
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" wire:click="secondStepSubmit">Next</button>
                <button class="btn btn-secondary nextBtn btn-lg pull-right" type="button" wire:click="back(1)">Back</button>
                {{-- @endif
                 @endif --}}
            </div>
        </div>
    </div>
</div>

