 <div>
   @include('livewire.student.multi-step')

<div class="row setup-content {{ $currentStep != 4 ? 'displayNone' : '' }}" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 4</h3>
                {{-- @if($students!=null)
                 @if($students->image!=null)
                <span class="text-primary">You Already filled</span>
                 <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="fourthStepSubmit" type="button">Next</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(3)">Back</button>
                @else --}}
                  <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" wire:model="photo" class="form-control" id="productAmount"/>
                    @error('photo') <span class="text-danger">{{ $message }}</span> @enderror
                </div>  
                <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="fourthStepSubmit" type="button">Next</button>
                <button class="btn btn-secondary nextBtn btn-lg pull-right" type="button" wire:click="back(3)">Back</button>
               {{-- @endif 
               @endif --}}
            </div>
        </div>
    </div>
</div>    
