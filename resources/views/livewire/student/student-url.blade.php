
 <div>
    @include('livewire.student.multi-step')

<div class="row setup-content {{ $currentStep != 5 ? 'displayNone' : '' }}" id="step-5">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 5</h3>
                {{-- @if($students!=null)
                @if($students->url!=null)
                <span class="text-primary">You Successfully submitted</span>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(4)">Back</button>
                @else --}}

                <div class="form-group">
                    <label for="title">web Url:</label>
                   <textarea type="text" wire:model="url_link" class="form-control" id="taskDescription">{{{ $url_name ?? '' }}}</textarea>
                    @error('url_link') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="title">Url Name:</label>
                   <textarea type="text" wire:model="url_name" class="form-control" id="taskDescription">{{{ $url_name ?? '' }}}</textarea>
                    @error('url_name') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <button class="btn btn-success btn-lg pull-right" wire:click="submitForm" type="button">Finish!</button>
                <button class="btn btn-secondary nextBtn btn-lg pull-right" type="button" wire:click="back(4)">Back</button>
                {{-- @endif
                @endif --}}
            </div>
        </div>
    </div>
</div>