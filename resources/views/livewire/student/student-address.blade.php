  <div>
    @include('livewire.student.multi-step')

 <div class="row setup-content {{ $currentStep != 3 ? 'displayNone' : '' }}" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 3</h3><br>
                {{-- @if($students!=null)
                 @if($students->address!=null)
                <span class="text-primary">You Already filled</span>
                <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="thirdStepSubmit" type="button">Next</button>
                <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(2)">Back</button>
                @else --}}
                  <div class="form-group">
                    <label for="description">Address:</label>
                    <textarea type="text" wire:model="address" class="form-control" >{{{ $address ?? '' }}}</textarea>
                    @error('address') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
               
                <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="thirdStepSubmit" type="button">Next</button>
                <button class="btn btn-secondary nextBtn btn-lg pull-right" type="button" wire:click="back(2)">Back</button>
                 {{-- @endif 
                 @endif --}}
            </div>
        </div>
    </div>
</div>    