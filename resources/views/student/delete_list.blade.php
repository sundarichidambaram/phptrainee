@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

<h1>Delete Lists:</h1>
<div class="container">
    <a href="{{url('/admin/students')}}">
     <button class="btn btn-info btn-sm">Student List</button>
   </a>
    <div class="card">
      <div class="card-body">
          <table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Mobileno</th>
      <th scope="col">Age</th>
      <th scope="col">Gender</th>
      <th scope="col">Date Of Birth</th>
      <th scope="col">Country</th>
      <th scope="col">State</th>
    </tr>
  </thead>
  @if($students->count() > 0)
        @foreach($students as $value)
  <tbody>
    <tr>
      <td>{{$value->studentname}}</td>
      <td>{{$value->email}}</td>
      <td>{{$value->mobile}}</td>
      <td>{{$value->age}}</td>
      <td>{{$value->gender}}</td>
      <td>{{$value->dob}}</td>
      <td>{{$value->country}}</td>
      <td>{{$value->state}}</td>
      <td>
        <a href="{{url('/admin/restore/student/'.$value->id)}}">
        <button class="btn btn-primary btn-sm">Restore</button>
        </a>
        <a href="{{url('/admin/force/delete/'.$value->id)}}">
        <button class="btn btn-danger btn-sm">delete</button>
        </a>
     </td>
    </tr>
  </tbody>
   @endforeach

     @else
        <tr>
          <td colspan="8" class="text-center text-base">No Records Found</td>
        </tr>
        @endif
        
</table>
        
      </div>
    </div>

      
</div>
@endsection