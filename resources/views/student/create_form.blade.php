@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}

     <div class="container">
	<h1>Student Details:</h1>
{{-- <span style="color:red">
@if(\Session::has('successmessage'))
	{{\Session::get('successmessage')}}
	
{{\Session::forget('successmessage')}}
	@endif
</span> --}}
	<form action="{{ url('/admin/student/') }}" method="post" enctype="multipart/form-data"  id="contact_us_form">
			@csrf
                <div class="form-group">
		<label for="sname">Studentname:</label>
		<input class="form-control" type="text" id="sname" name="sname" value="{{old('sname')}}">
    <div style="color:red">{{ $errors->first('sname') }}</div>
                </div>
         <div class="form-group">
        <lable>Choose one:</lable>
                <select class="form-control" id="dropdown" name="dropdown">
             <option value="">select</option>       
             <option value="phone_id" @if(old('dropdown')=='phone_id') selected @endif>MobileNo</option>
            <option value="email_id" @if(old('dropdown')=='email_id') selected @endif>Email</option>
        </select>
         <div style="color:red">{{ $errors->first('dropdown') }}</div>
        </div>       
                      <div class="form-group"> 
		<label for="email">Email:</label>
		<input class="form-control" type="email" id="email" name="email" value="{{old('email')}}">
    <div style="color:red">{{ $errors->first('email') }}</div>
                </div>
          
                    <div class="form-group">
		<label for="password">Password:</label>
		<input class="form-control" type="password" name="password" value="{{old('password')}}">
    <div style="color:red">{{ $errors->first('password') }} </div>
                    </div>
                    <div class="form-group">
		<label for="mobileno">Mobileno:</label>
		<input class="form-control" type="text" name="mobileno" value="{{old('mobileno')}}">
    <div style="color:red">{{ $errors->first('mobileno') }}</div>
                  </div>
             <div class="form-group">
		<label for="age">Age:</label>
		<input class="form-control" type="text" name="age" size="4" value="{{old('age')}}">
    <div style="color:red">{{ $errors->first('age') }}</div>
              </div>
             <div class="form-group"> 
        <label for="gender">Gender:</label><br>
		<input type="radio" id="male" name="gender" value="male" @if(old('gender')=='male') checked @endif>
		<label for="male">Male</label><br>
		<input type="radio" id="female" name="gender" value="female"@if(old('gender')=='female') checked @endif >
		<label for="female">Female</label>
    <div style="color:red">{{ $errors->first('gender') }}</div>
       </div>
       <div class="form-group">
		<label for="dob">Dob:</label>
		<input class="form-control" type="date"  id="dob" name="dob"  value="{{old('dob')}}" min="1990-01-01" max="2000-12-31">
    <div style="color:red">{{ $errors->first('dob') }}</div>
    </div>
		<div class="form-group">
		<label for="image">Choose your image:</label>
	        <input class="form-control" type="file" id="image" name="image">
	        <div style="color:red">{{ $errors->first('image') }}</div>
            </div>
           <div class="form-group">
	        <label>Choose the Country:</label>
	        <select class="form-control" id="country" name="country" class="form-select" aria-label="Default select example">
	         <option style="background-color:darkgray">--country--</option>
	        	@foreach($countries as $key=>$country)
			<option value="{{$country->name}}"  {{ (collect(old('country'))->contains($country->name)) ? 'selected':'' }} >{{$country->name}}</option>
			@endforeach

		</select>
		<div style="color:red">{{ $errors->first('country') }}</div>
	</div>
	    <div class="form-group">
		<lable>Choose the State:</lable>
                <select class="form-control" id="state" name="state" class="form-select" aria-label="Default select example">
	         <option style="background-color:darkgray">--states--</option>
	        	@foreach($state as $key=>$value)
			<option value="{{$value->name}}" {{ (collect(old('state'))->contains($value->name)) ? 'selected':'' }}>{{$value->name}}</option>
			@endforeach
		</select>
		<div style="color:red">{{ $errors->first('state') }}</div>
        </div>
		<div class="form-group">
            <div class="g-recaptcha" data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}" name="g_recaptcha"></div>
            <div style="color:red">{{ $errors->first('g-recaptcha-response') }}</div>

        {{-- <span style="color:red">
@if(\Session::has('g_recaptcha'))
	{{\Session::get('g_recaptcha')}}
	
{{\Session::forget('g_recaptcha')}}
	@endif
</span>--}}
    </div>

	
		<input type="submit" class="btn btn-primary" value="Submit">
	</form>
</div>
	

@endsection