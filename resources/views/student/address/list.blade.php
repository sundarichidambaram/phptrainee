@extends('layouts.main')
@section('content')
	<h1>Student Details:</h1>
	<table class="table table-striped">
		<tr>
			<th>Address</th>
			<th>Name</th>
			<th>Email</th>
			<th>Mobileno</th>
			<th>Age</th>
			<th>Gender</th>
			<th>Dob</th>
			<th>Country</th>
			<th>Image</th>


			
		
		</tr>
		@foreach($address as $value)
		<tr>
		<td>{{$value->address}}</td>
		<td>{{$value->Student->studentname}}</td>
		<td>{{$value->Student->email}}</td>
		<td>{{$value->Student->mobile}}</td>
		<td>{{$value->Student->age}}</td>
		<td>{{$value->Student->gender}}</td>
		<td>{{$value->Student->dob}}</td>
		<td>{{$value->Student->country}}</td>
        <td><img border="0" style="padding: 15px;" 
     src="{{ url('fileupload/' . $value->Student->image) }}" 
     alt="image"  width="100" height="70" class="imgshadow-fx"></td>

	</tr>
	@endforeach
    </table>

@endsection