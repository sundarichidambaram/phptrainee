
@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

  <h1>Student Details:</h1>
  <a href="{{url('/admin/student')}}">
      <button class="btn btn-success btn-sm">Add</button>
   </a>
    
   <a href="{{ url('/admin/restore/students') }}">  <button class="btn btn-primary btn-sm">RestoreAll</button></a>
   <a href="{{url('/admin/delete/list')}}">
      <button class="btn btn-danger btn-sm">DeleteList</button>
   </a> 
  <table class="table">
    <tr>

      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Mobile</th>
      <th scope="col">Age</th>
      <th scope="col">Gender</th>
      <th scope="col">Dob</th>
      <th scope="col">Country</th>
      <th scope="col">States</th>
      <th scope="col">Image</th>
      <th colspan="8" style="text-align: center">Action</th>
    </tr>
    @if($student->count() > 0)
    @foreach($student as $value)
    <tr>
      <td>{{$value->studentname}}</td>
      <td>{{$value->email}}</td>
      <td>{{$value->mobile}}</td>
      <td>{{$value->age}}</td>
      <td>{{$value->gender}}</td>
      <td>{{$value->dob}}</td>
      <td>{{$value->country}}</td>
      <td>{{$value->state}}</td>
      <td><img border="0" style="padding: 15px;" 
     src="{{ url('fileupload/' . $value->image) }}" 
     alt="image"  width="100" height="70" class="imgshadow-fx"></td>
      <td>
        <a href="{{url('/admin/student/edit/' .$value->id)}}"><button class="btn btn-secondary btn-sm">Edit</button></a>
      </td>
      <td>
          <a href="{{ url('/admin/delete-student/'.$value->id) }}"><button class="btn btn-danger btn-sm">Delete</button></a>
      </td>
      <td><a href="{{ url('/admin/student/address/'.$value->id) }}"><button class="btn btn-success btn-sm">Add address</button></a></td>
      <td><a href="{{ url('/admin/profile/'.$value->id) }}"><button class="btn btn-success btn-sm">profile</button></a></td>
      <td><a href="{{ url('/admin/addresslist/'.$value->id) }}"><button class="btn btn-info btn-sm">list</button></a></td>

      <td><a href="{{ url('/admin/profile/list/'.$value->id) }}"><button class="btn btn-info btn-sm">profilelist</button></a></td>

      <td><a href="{{ url('/admin/address/list/'.$value->id) }}"><button class="btn btn-info btn-sm">addresslist</button></a></td>

      <td><a href="{{ url('/admin/profile/list/student/'.$value->id) }}"><button class="btn btn-info btn-sm">show profile</button></a></td>
      <!-- <td><a href="{{ url('/admin/restore/student/'.$value->id) }}"><button class="btn btn-primary btn-sm">Restore</button></a></td> -->

      
      
      </tr>
    @endforeach
    @else
    <tr>
      <td colspan="17" class="text-center text-base">No Records Found</td>
    </tr>
    @endif 
    
  </table>
  
    {{$student->links()}}
  


@endsection