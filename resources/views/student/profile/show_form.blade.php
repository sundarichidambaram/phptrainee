@extends('layouts.main')
@section('content')

	<h1>Student Details:</h1>
	<table class="table table-striped">
		<tr>
			<th>name</th>
			<th>email</th>
			<th>phoneno</th>
			<th>city</th>
			<th>gender</th>
			<th>department</th>	
		</tr>
		@if($profilelist->count() > 0)
		<tr>
			<td>{{$profilelist->Profile->studentname}}</td>
		    <td>{{$profilelist->Profile->email}}</td>
			<td>{{$profilelist->Profile->phoneno}}</td>
			<td>{{$profilelist->Profile->city}}</td>
			<td>{{$profilelist->Profile->gender}}</td>
			<td>{{$profilelist->Profile->department}}</td>

		</tr> 
		@else
    <tr>
      <td colspan="6" class="text-center text-base">No Records Found</td>
    </tr>
    @endif 	
    </table>
@endsection