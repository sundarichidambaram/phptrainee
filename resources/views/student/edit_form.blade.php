
@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}
      <div class="container">
    <h1>Update the student details</h1>
    {{-- <span style="color:red">

    @if(\Session::has('successmessage'))
      {{\Session::get('successmessage')}}
  
        {{\Session::forget('successmessage')}}
      @endif
  </span> --}}
    <form action="{{url('/admin/edit-students/'.$student->id)}}" method="post" enctype="multipart/form-data">
      @csrf
    <div class="form-group">  
    <label for="sname">Studentname:</label>
    <input type="text" class="form-control" id="sname" name="sname" value="{{$student->studentname}}">
    <div style="color:red">{{ $errors->first('sname') }}</div>
    </div>
    <div class="form-group">
    <label for="email">Student Email:</label>
    <input type="email" class="form-control" name="email" value="@if($errors->first('email')==''){{$student->email}} @else {{old('email')}} @endif">
    <div style="color:red">{{ $errors->first('email') }}</div>
    </div>
    <div class="form-group">
    <label for="password">Student Password:</label>
    <input type="password" class="form-control" name="password" value="{{$student->password}}">
    <div style="color:red">{{ $errors->first('password') }}</div>
  </div>
    <div class="form-group">
    <label for="mobileno">Student Mobileno:</label>
    <input type="text" class="form-control" name="mobileno" size="12" maxlength="10" value="@if($errors->first('mobileno')==''){{$student->mobile}} @else {{old('mobileno')}} @endif">
    <div style="color:red">{{ $errors->first('mobileno') }}</div>
    </div>
    <div class="form-group">
    <label for="age">Student Age:</label>
    <input type="text" class="form-control" name="age" size="4" value="@if($errors->first('age')==''){{$student->age}} @else {{old('age')}} @endif">
    <div style="color:red">{{ $errors->first('age') }}</div>
    </div>
    <div class="form-group">
        <label for="gender">Student Gender:</label><br>
    <input type="radio" id="male" name="gender" value="male" {{($student->gender=='male'?'checked':'')}} @if(old('gender')=='male') checked @endif>
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female" {{($student->gender=='female'?'checked':'')}} @if(old('gender')=='female') checked @endif>
    <label for="female">Female</label>
    <div style="color:red">{{ $errors->first('gender') }}</div>
    </div>
    <div class="form-group">
    <label for="dob">Student Dob:</label>
    <input type="date" class="form-control" id="dob"name="dob" value="{{$student->dob}}" min="1990-01-01" max="2000-12-31">
    <div style="color:red">{{ $errors->first('dob') }}</div>
    </div>
    <!--label for="country">Student Country:</label>
    <select id="country" name="country" value="{{$student->country}}">
      <option value="india" {{($student->country=='india'?'selected':'')}}  @if(old('country')=='india') selected @endif>India</option>

      <option value="russia" {{($student->country=='russia'?'selected':'')}}  @if(old('country')=='russia') selected @endif>Russia</option>

      <option  value="america" {{($student->country=='america'?'selected':'')}} @if(old('country')=='america') selected @endif>America</option>

      <option value="china" {{($student->country=='china'?'selected':'')}} @if(old('country')=='') selected @endif>China</option>

    </select><br><br-->
         <div class="form-group">
     <label>Choose the Country:</label>
          <select id="country" class="form-control" name="country" class="form-select" aria-label="Default select example">
           <option style="background-color:darkgray ;">--select--</option>
            @foreach($countries as $key=>$country)
      <option value="{{$country->id}}" {{ (collect(old('country'))->contains($country->name)) ? 'selected':'' }}>{{$country->name}}</option>
        <div style="color:red">{{ $errors->first('country') }}</div>
      @endforeach
    </select>
        </div>
        <div class="form-group">
    <label for="image">Choose your image:</label>
      <input type="file" class="form-control" id="image" name="image" value="{{$student->image}}" >
      <div style="color:red">{{ $errors->first('image') }}</div>
      <img border="0" src="{{ url('fileupload/' . $student->image) }}" alt="image" width="300" height="200">
      </div>
    
    <input type="submit" class="btn btn-primary" value="Update student">
    </form>
</div>    
@endsection