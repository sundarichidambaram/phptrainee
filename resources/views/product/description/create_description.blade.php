@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

		<h1>Product Description</h1>
		{{-- <span style="color:red">
		@if(\Session::has('successmessage'))
	    {{\Session::get('successmessage')}}
	
        {{\Session::forget('successmessage')}}
	    @endif
	    </span> --}}
		<form action="{{url('/admin/product/description/'.$description->id)}}" method="post" enctype="multipart/form-data">
			@csrf
			 <div class="form-group">
			<label for="descript">Description:</label>
			<textarea class="form-control" id="descript" name="descript" rows="4" cols="40" values="{{old('descript')}}" autofocus>
				
			</textarea>
		    <div class="alert alert-danger" style="color:red">{{ $errors->first('descript') }}</div>
		   </div>
			<input type="submit" value="save">
			<input type="reset" value="cancel">

			

		</form>
@endsection