@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

			<h1>Edit Product</h1>
			{{-- @if(\Session::has('successmessage'))
	        {{\Session::get('successmessage')}}
	
            {{\Session::forget('successmessage')}}
	        @endif --}}
			<form action="{{ url('/admin/product/update/'.$product->id) }}" method="post" enctype="multipart/form-data">
				@csrf
				<lable for="name">Product Name:</lable>
				<input type="text" id="name" name="name" value="@if($errors->first('name')==''){{$product->productname}} @else {{old('name')}} @endif">
			    <div class="alert alert-danger" style="color:red">{{ $errors->first('name') }}</div>

				<lable for="price">Price:</lable>
				<input type="text" id="price" name="price" value="@if($errors->first('price')==''){{$product->price}} @else {{old('price')}} @endif">
				<div class="alert alert-danger" style="color:red">{{ $errors->first('price') }}</div>
				<lable for="price">ProductCode:</lable>
				<input type="text" name="product_code" value="@if($errors->first('product_code')==''){{$product->product_code}} @else {{old('product_code')}} @endif">
				<div class="alert alert-danger" style="color:red">{{ $errors->first('product_code') }}</div>

                <input type="submit">
                <input type="reset" value="cancel">


			</form>
@endsection