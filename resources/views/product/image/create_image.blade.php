@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

         <h1>Image upload</h1>
         {{-- <span style="color:red">
         @if(\Session::has('successmessage'))
         {{\Session::get('successmessage')}}
   
         {{\Session::forget('successmessage')}}
         @endif
         </span> --}}
         <form action="{{ url('/admin/product/image/'.$image->id) }}" method="post" enctype="multipart/form-data">
            @csrf
         <label for="image">Choose your image:</label><br>
         <input type="file" id="image" name="image">
         <div class="alert alert-danger" style="color:red">{{ $errors->first('image') }}</div>
<br><br>
         <input type="submit">
         </form>
  @endsection