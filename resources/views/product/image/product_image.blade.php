@extends('layouts.main')
@section('content')

	<h1>Product Image:</h1>
	<table class="table table-striped">
		<tr>
			<th>ProductName</th>
			<th>Price</th>
			<th>Image</th>
		</tr>
		@foreach($images as $value)
		<tr>
		<td>{{$value->Product->productname}}</td>
		<td>{{$value->Product->price}}</td>
        <td><img border="0" style="padding: 15px;" 
     src="{{ url('imageupload/' . $value->image) }}" 
     alt="image"  width="100" height="80" class="imgshadow-fx"></td>

	</tr>
	@endforeach
    </table>
@endsection