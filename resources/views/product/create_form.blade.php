@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

			<h1>Product</h1>
			{{-- <span style="color:red">
			@if(\Session::has('successmessage'))
	        {{\Session::get('successmessage')}}
	
            {{\Session::forget('successmessage')}}
	        @endif
	        </span> --}}
			<form action="{{ url('/admin/product/') }}" method="post" enctype="multipart/form-data">
				@csrf
				<lable for="name">Product Name:</lable>
				<input type="text" id="name" name="name" value="{{old('name')}}">
			    <div class="alert alert-danger" style="color:red">{{ $errors->first('name') }}</div>
				<lable for="price">Price:</lable>
				<input type="text" id="price" name="price" value="{{old('price')}}">
				<div class="alert alert-danger" style="color:red">{{ $errors->first('price') }}</div>
				<lable for="price">ProductCode:</lable>
				<input type="text" name="product_code" value="{{old('product_code')}}">
				<div class="alert alert-danger" style="color:red">{{ $errors->first('product_code') }}</div>
                <input type="submit">
                <input type="reset" value="cancel">

     @endsection
		