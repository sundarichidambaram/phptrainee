@extends('layouts.main')
@section('content')

	<h1>Product Details:</h1>
	<table class="table table-striped">
		<thead>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>ProductCode</th>
			<th>Edit</th>
			<th>Images</th>
			<th>Description</th>
			<th>Show image</th>
			<th>Show description</th>
			<th>Product_Image</th>
			<th>Product_Decription</th>
			
		</tr>
	</thead>
		 @foreach($product as $value)
		 <tbody>
		 <tr>
		 		<td>{{$value->productname}}</td>
		 		<td>{{$value->price}}</td>
		 		<td>{{$value->product_code}}</td>
		 			 	<td>
		 		<a href="{{url('/admin/product/edit/' .$value->id)}}" style="color:red">Edit</a>

		 	</td>
		 		<td>
		 			<a href="{{url('/admin/product/image/' .$value->id)}}">images</a>

		 		</td>
		 		<td>
		 			<a href="{{url('/admin/product/description/' .$value->id)}}">description</a>

		 		</td>
		 	
		 		
		 		<td>
		 		<a href="{{url('/admin/product/images/' .$value->id)}}">show image</a>
		 	</td>

		 		
		 	<td>
		 		<a href="{{url('/admin/product/descriptions/' .$value->id)}}">show desc</a>

		 	</td>
		 		
		 		<td>
		 		<a href="{{url('/admin/product/image/list/' .$value->id)}}">imagelist</a>

		 	</td>

	<td>
		 		<a href="{{url('/admin/product/descriptions/list/' .$value->id)}}">descriptionlist</a>

		 	</td>


		 </tr>
		</tbody>
		 @endforeach
	</table>
@endsection