@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

  <h1>Employee Details:</h1>
  <a href="{{url('/admin/employee')}}">
      <button class="btn btn-success btn-sm">Add</button>
   </a>
    
   {{-- <a href="{{ url('/admin/employee/restore') }}">  <button class="btn btn-primary btn-sm">RestoreAll</button></a> --}}
   <a href="{{url('/admin/employee/trashlist')}}">
      <button class="btn btn-danger btn-sm">DeleteList</button>
   </a> 
  <table class="table">
    <tr>

      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Address</th>
      <th colspan="1" style="text-align: center">Action</th>
    </tr>
    @if($employee->count() > 0)
    @foreach($employee as $value)
    <tr>
      <td>{{$value->name}}</td>
      <td>{{$value->designation}}</td>
     <td>{{$value->employeelink->address}}</td>
      <td>
        <a href="{{url('/admin/employee/edit/' .$value->id)}}"><button class="btn btn-secondary btn-sm">Edit</button></a>
      </td>
      <td>
          <a href="{{ url('/admin/employee/delete/'.$value->id) }}"><button class="btn btn-danger btn-sm">Delete</button></a>
      </td>    
      </tr>
    @endforeach
    @else
    <tr>
      <td colspan="3" class="text-center text-base">No Records Found</td>
    </tr>
    @endif 
    
  </table>
  <span>
    {{$employee->links()}}
  </span>


@endsection