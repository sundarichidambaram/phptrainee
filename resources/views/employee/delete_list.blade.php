@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

<h1>Delete Lists:</h1>
<div class="container">
    <a href="{{url('/admin/employees')}}">
     <button class="btn btn-info btn-sm">Employee List</button>
   </a>
    <div class="card">
      <div class="card-body">
          <table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
    </tr>
  </thead>
  @if($employee->count() > 0)
        @foreach($employee as $value)
  <tbody>
    <tr>
      <td>{{$value->name}}</td>
      <td>{{$value->designation}}</td>
      <td>
        <a href="{{url('/admin/employee/restored/'.$value->id)}}">
        <button class="btn btn-primary btn-sm">Restore</button>
        </a>
        <a href="{{url('/admin/employee/fdelete/'.$value->id)}}">
        <button class="btn btn-danger btn-sm">delete</button>
        </a>
     </td>
    </tr>
  </tbody>
   @endforeach

     @else
        <tr>
          <td colspan="8" class="text-center text-base">No Records Found</td>
        </tr>
        @endif
        
</table>
        
      </div>
    </div>

      
</div>
@endsection