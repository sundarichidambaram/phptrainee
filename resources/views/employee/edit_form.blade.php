@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}

     <div class="container">
	<h1>Student Details:</h1>
	<form action="{{ url('/admin/employee/updated/'.$employee->id) }}" method="post" enctype="multipart/form-data"  id="contact_us_form">
			@csrf
     <div class="form-group">
		<label for="sname">EmployeeName:</label>
		<input class="form-control" type="text" id="name" name="name" value="{{$employee->name}}">
    <div style="color:red">{{ $errors->first('name') }}</div>
                </div>
      
    <div class="form-group"> 
		<label for="email">Designation:</label>
		<input class="form-control" type="text" name="designation" value="{{$employee->designation}}">
    <div style="color:red">{{ $errors->first('designation') }}</div>
                </div>
    <div class="form-group">            
    <label for="address">Address:</label>
			<textarea class="form-control" id="address" name="address" rows="3" cols="20">
				{{$employee_link->address}}
			</textarea>
			    <div style="color:red">{{ $errors->first('address') }}</div>  
	</div>		              
		<input type="submit" class="btn btn-primary" value="Submit">
	</form>
</div>
	

@endsection
