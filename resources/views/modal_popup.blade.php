@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}
<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('modal-popup')
        
      </div>
    </div>
      
</div>
@endsection

@push('scripts')
<script type="text/javascript">
        window.addEventListener('close-modal', event => {
          $('#contactModal').modal('hide');
    });
    </script>

@endpush
 