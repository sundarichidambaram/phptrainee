@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}
<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('edit-contact',['id'=>$id])
        
      </div>
    </div>
      
</div>
@endsection