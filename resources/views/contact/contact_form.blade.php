@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}
<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('contact-form',['id'=>$id])
        
      </div>
    </div>
      
</div>
@endsection

@push('scripts')
<script>
    window.addEventListener('show-add-confirmation', event => {
        Swal.fire({
            title: 'Save?',
            text: "You want save this!",
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#30d633',
            cancelButtonColor: '#d6305c',
            confirmButtonText: 'Save!'
            }).then((result) => {
                if (result.isConfirmed) {
                   livewire.emit('saveConfirmed');
                }
        })
    });

    window.addEventListener('contactSaved', event => {
        Swal.fire(
              'Saved!',
              'Contact has been Saved successfully.',
              'success'
               )
    });
    // window.addEventListener('contactRestored', event => {
    //     Swal.fire(
    //           'Restored!',
    //           'Contacts has been Restored successfully.',
    //           'success'
    //            )
    // });
    // window.addEventListener('notRestored', event => {
    //     Swal.fire(
    //           'Restored!',
    //           'Contacts has been Restored successfully.',
    //           'success'
    //            )
    // });
  

 </script>
@endpush

