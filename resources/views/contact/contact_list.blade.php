@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}

<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('contact-list')
        
      </div>
    </div>

      
</div>
@endsection

@push('scripts')
<style type="text/css">
   form{
  position: absolute;
  top: 30px;
  right: 30px;
  word-spacing: 5px;
}
</style>
<script>
    window.addEventListener('show-delete-confirmation', event => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                   livewire.emit('deleteConfirmed');
                }
        })
    });

    window.addEventListener('contactDeleted', event => {
        Swal.fire(
              'Deleted!',
              'Contact has been deleted successfully.',
              'success'
               )
    });
    window.addEventListener('contactRestored', event => {
        Swal.fire(
              'Restored!',
              'Contacts has been Restored successfully.',
              'success'
               )
    });
    window.addEventListener('notRestored', event => {
        Swal.fire(
              'Restored!',
              'Contacts has been Restored successfully.',
              'success'
               )
    });
  

 </script>
@endpush