@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

<h1>Delete Lists:</h1>
<div class="container">
    <a href="{{url('/admin/contacts')}}">
     <button class="btn btn-primary btn-sm">Contact List</button>
   </a>
    <div class="card">
      <div class="card-body">
          <table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Mobileno</th>
      <th scope="col">Address</th>
      <th scope="col">Date Of Birth</th>
      <th scope="col">Edit</th>
    </tr>
  </thead>
  @if($contacts->count() > 0)
        @foreach($contacts as $value)
  <tbody>
    <tr>
      <td>{{$value->name}}</td>
      <td>{{$value->email}}</td>
      <td>{{$value->mobileno}}</td>
      <td>{{$value->address}}</td>
      <td>{{$value->dob}}</td>
      <td>
        <a href="{{url('/admin/restore/contact',['id'=>$value->id])}}">
        <button class="btn btn-primary btn-sm">Restore</button>
        </a>
     </td>
    </tr>
  </tbody>
   @endforeach

     @else
        <tr>
          <td colspan="4" class="text-center text-base">No Records Found</td>
        </tr>
        @endif
        
</table>
        
      </div>
    </div>

      
</div>
@endsection