@extends('layouts.main')
@section('content')

  <!--sidebar-->
    @include('template3.student.sidebar')
			<!--start-->
    <div class="w-full">
         
         <div class="p-7">

          <!-- heading -->
          <div class="">
            <h1 class="text-xl lg:text-3xl md:text-3xl font-bold">Student Profile</h1>
            <ul class="list-reset flex items-center breadcrumbs text-xs text-gray-500 my-1">
              <li class="relative mr-4"><a href="{{ url('/admin/students/') }}">Student List</a></li>
              <li class="relative mr-4">Add Profile </li>
            </ul>
          </div>
          <!-- heading -->

         <!--  section start  -->
         <div class="bg-white shadow-sm p-6 my-4 leading-loose">
           <div>
             <form action="{{ url('/admin/student/') }}" method="post" enctype="multipart/form-data">
             	@csrf
               <div class="grid grid-cols-1 lg:grid-cols-2 md:grid-cols-2 gap-8 w-full lg:w-2/3 md:w-2/3">
                <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Student Name</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full" name="sname" value="{{old('sname')}}">
                       </div>
                        <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('sname')}}</span>
                   </div>
                   <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Email</label>
                       <div class="pt-1">
                         <input type="email" class="bg-white px-2 py-2 rounded border text-sm w-full" name="email" value="{{old('email')}}">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('email')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Mobile Number</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full"  name="phoneno" value="{{old('phoneno')}}">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('phoneno')}}</span>
                   </div>
                  <!-- **** -->
                  <!-- **** -->
                  <div>
                       <label class="text-xs font-medium text-gray-600">Age</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full" name="city" size="4" value="{{old('city')}}">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('city')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                  <div>
                       <label class="text-xs font-medium text-gray-600">Gender</label>
                       <div class="pt-1">
                        
                        <div class="border px-2 py-2 rounded flex items-center">
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="gender" value="male" @if(old('gender')=='male') checked @endif>
                           <span class="mx-2">Male</span>
                        </div>
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="gender" value="female" @if(old('gender')=='female') checked @endif>
                           <span class="mx-2">Female</span>
                        </div>
                        </div>

                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('gender')}}</span>
                   </div>
                   <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Gender</label>
                       <div class="pt-1">
                        
                        <div class="border px-2 py-2 rounded flex items-center">
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="department" value="cse" @if(old('department')=='cse') checked @endif>
                           <span class="mx-2">CSE</span>
                        </div>
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="department" value="mech" @if(old('department')=='mech') checked @endif>
                           <span class="mx-2">MECH</span>
                        </div>
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="department" value="civil" @if(old('department')=='civil') checked @endif>
                           <span class="mx-2">CIVIL</span>
                        </div>
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="department" value="ece" @if(old('department')=='ece') checked @endif>
                           <span class="mx-2">ECE</span>
                        </div>
                        </div>

                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('department')}}</span>
                   </div>
               </div>

               <div class="flex items-center my-3 pt-6">
                 <div><button class="bg-blue-500 rounded text-white px-8 py-2 text-sm mr-5">Save</button></div>
                 <div><button class="bg-gray-200 rounded text-black px-8 py-2 text-sm mr-5"><a href="list-of-church.html">Cancel</a></button></div>
               </div>
             </form>
           </div>
         </div>
         <!--  section end -->
         
         </div>
       </div>

@endsection
