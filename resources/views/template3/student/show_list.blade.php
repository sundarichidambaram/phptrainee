@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

@include('template3.student.sidebar')
<!-- start -->
       <div class="w-full">
         
         <div class="p-3 lg:p-7 md:p-7">

          <!-- heading -->
          <div class="flex flex-col lg:flex-row md:flex-row lg:items-center md:items-center justify-between">
            <div class="flex items-center mb-2 lg:mb-0 md:mb-0">
            <h1 class="text-lg lg:text-3xl md:text-3xl font-bold">List of Students</h1>
          </div>
            <div class="flex items-center">
              <div class="lg:ml-2 md:ml-2">
                <a href="{{url('/admin/student')}}">
                <button class="bg-green-500 rounded text-white px-3 py-2 text-sm flex items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg> 
                     <span class="ml-1">Add Student</span>
                </button>
              </a>
              </div>
              <div class="lg:ml-2 md:ml-2">
                <a href="{{url('/admin/restore/students')}}">
                <button class="bg-green-500 rounded text-white px-3 py-2 text-sm flex items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg> 
                     <span class="ml-1">Restore Student</span>
                </button>
              </a>
              </div>
              <div class="lg:ml-2 md:ml-2">
                <a href="{{url('/admin/delete/list')}}">
                <button class="bg-green-500 rounded text-white px-3 py-2 text-sm flex items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg> 
                     <span class="ml-1">DeleteList</span>
                </button>
              </a>
              </div>
              
            </div>
          </div>
          <!-- heading -->

          <!-- table section start -->
               <div class="my-4 py-5 overflow-x-auto">
                  <table class="w-full text-sm custom-table bg-white">
                    <thead class="bg-light">
                      <tr>
                        <th class="px-4 py-3 text-left">Name</th>
                        <th class="px-4 py-3 text-left">Email</th>
                        <th class="px-4 py-3 text-left">Mobile</th>
                        <th class="px-4 py-3 text-left">Age</th>
                        <th class="px-4 py-3 text-left">Gender</th>
                        <th class="px-4 py-3 text-left">Dob</th>
                        <th class="px-4 py-3 text-left">Country</th>
                        <th class="px-4 py-3 text-left">States</th>
                        <th class="px-4 py-3 text-left">Image</th>
                        <th class="px-4 py-3 text-left" width="5%">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                    	@if($student->count() > 0)
                         @foreach($student as $value)
                      <tr>
                        <td>{{$value->studentname}}</td>
				        <td>{{$value->email}}</td>
				        <td>{{$value->mobile}}</td>
				        <td>{{$value->age}}</td>
				        <td>{{$value->gender}}</td>
				        <td>{{$value->dob}}</td>
				        <td>{{$value->country}}</td>
				        <td>{{$value->state}}</td>
				       <td><img border="0" style="padding: 15px;" 
				     src="{{ url('fileupload/' . $value->image) }}" 
				     alt="image"  width="100" height="70" class="imgshadow-fx"></td>
                        <td>
                          <ul class="list-reset flex items-center">
                            <li class="mr-2">
                              <a href="{{url('/admin/student/edit/' .$value->id)}}" title="Edit">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 hover:text-gray-800">
                                  <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                                </svg>
                              </a>
                            </li>
                            <li class="mr-2">
                              <a href="{{ url('/admin/student/address/'.$value->id) }}" title="Address">
                             <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 hover:text-gray-800">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 10.5v6m3-3H9m4.06-7.19l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z" />
                              </svg>
                            </a>
                            </li>
                            <li class="mr-2">
                              <a href="{{ url('/admin/addresslist/'.$value->id) }}" title="Address View">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 hover:text-gray-800">
                                  <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z" />
                                  <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                              </a>
                            </li>
                            <li class="mr-2">
                              <a href="{{ url('/admin/profile/'.$value->id) }}" title="Profile">
                             <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 hover:text-gray-800">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 10.5v6m3-3H9m4.06-7.19l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z" />
                              </svg>
                            </a>
                            </li>
                            <li class="mr-2">
                              <a href="{{ url('/admin/profile/list/'.$value->id) }}" title="Profile View">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 hover:text-gray-800">
                                  <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z" />
                                  <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                              </a>
                            </li>
                            <li>
                              <a href="{{ url('/admin/delete-student/'.$value->id) }}" title="Delete">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9.75L14.25 12m0 0l2.25 2.25M14.25 12l2.25-2.25M14.25 12L12 14.25m-2.58 4.92l-6.375-6.375a1.125 1.125 0 010-1.59L9.42 4.83c.211-.211.498-.33.796-.33H19.5a2.25 2.25 0 012.25 2.25v10.5a2.25 2.25 0 01-2.25 2.25h-9.284c-.298 0-.585-.119-.796-.33z" />
                          </svg>

                            </a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                      
                       @endforeach
					    @else
					    <tr>
					      <td colspan="10" class="text-center text-base">No Records Found</td>
					    </tr>
					    @endif
                    </tbody>
                  </table>
               </div>
               {{$student->links()}}
          <!-- table section end -->

         </div>
       </div>
   <!-- end -->	
@endsection