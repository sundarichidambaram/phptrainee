@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

@include('template3.student.sidebar')
<!-- start -->
       <div class="w-full">
         
         <div class="p-3 lg:p-7 md:p-7">

          <!-- heading -->
          <div class="flex flex-col lg:flex-row md:flex-row lg:items-center md:items-center justify-between">
            <div class="flex items-center mb-2 lg:mb-0 md:mb-0">
            <h1 class="text-lg lg:text-3xl md:text-3xl font-bold">Deleted Students</h1>
          </div>
            <div class="flex items-center">
              <div class="lg:ml-2 md:ml-2">
                <a href="{{url('/admin/students')}}">
                <button class="bg-green-500 rounded text-white px-3 py-2 text-sm flex items-center">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg> 
                     <span class="ml-1">Student List</span>
                </button>
              </a>
              </div>
              
            </div>
          </div>
          <!-- heading -->

          <!-- table section start -->
               <div class="my-4 py-5 overflow-x-auto">
                  <table class="w-full text-sm custom-table bg-white">
                    <thead class="bg-light">
                      <tr>
                        <th class="px-4 py-3 text-left">Name</th>
                        <th class="px-4 py-3 text-left">Email</th>
                        <th class="px-4 py-3 text-left">Mobile</th>
                        <th class="px-4 py-3 text-left">Age</th>
                        <th class="px-4 py-3 text-left">Gender</th>
                        <th class="px-4 py-3 text-left">Dob</th>
                        <th class="px-4 py-3 text-left">Country</th>
                        <th class="px-4 py-3 text-left">States</th>
                        <th class="px-4 py-3 text-left">Image</th>
                        <th class="px-4 py-3 text-left" width="5%">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if($students->count() > 0)
                         @foreach($students as $value)
                      <tr>
                        <td>{{$value->studentname}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->mobile}}</td>
                <td>{{$value->age}}</td>
                <td>{{$value->gender}}</td>
                <td>{{$value->dob}}</td>
                <td>{{$value->country}}</td>
                <td>{{$value->state}}</td>
               <td><img border="0" style="padding: 15px;" 
             src="{{ url('fileupload/' . $value->image) }}" 
             alt="image"  width="100" height="70" class="imgshadow-fx"></td>
                        <td>
                          <ul class="list-reset flex items-center">
                            <li class="mr-2">
                              <a href="{{url('/admin/restore/student/'.$value->id)}}" title="Restore">
                             <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 hover:text-gray-800">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 10.5v6m3-3H9m4.06-7.19l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z" />
                              </svg>
                            </a>
                            </li>
                        </td>
                        <td><a href="{{url('/admin/force/delete/'.$value->id)}}" title="Profile View">
                        <button class="inline-flex items-center px-4 py-2 bg-red-600 hover:bg-red-700 text-white text-sm font-medium rounded-md">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
          </svg>
        </a>

          Delete
          </button></td>
                      </tr>
                      
                       @endforeach
              @else
              <tr>
                <td colspan="10" class="text-center text-base">No Records Found</td>
              </tr>
              @endif
                    </tbody>
                  </table>
               </div>
          <!-- table section end -->

         </div>
       </div>
   <!-- end --> 
@endsection