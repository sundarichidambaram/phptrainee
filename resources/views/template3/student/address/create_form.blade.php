@extends('layouts.main')
@section('content')

<!--sidebar-->
  @include('template3.student.sidebar')
  <!-- start -->
       <div class="w-full">
         
         <div class="p-7">
         	<div class="">
            <h1 class="text-xl lg:text-3xl md:text-3xl font-bold">Student Address</h1>
          </div>
         	<!--  section start  -->
         <div class="bg-white shadow-sm p-6 my-4 leading-loose">
           <div>
             <form action="{{url('/admin/address/'.$address->id) }}" method="post" enctype="multipart/form-data">
             	@csrf
               <div class="grid grid-cols-1 lg:grid-cols-2 md:grid-cols-2 gap-8 w-full lg:w-2/3 md:w-2/3">
               	<div>
                       <label class="text-xs font-medium text-gray-600">Address<span class="text-red-500">*</span></label>
                       <div class="pt-1">
                         <textarea class="bg-white px-2 py-2 rounded border text-sm w-full"  rows="3"
                         name="address" value="{{old('address')}}"></textarea>
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('address')}}</span>
                   </div>
               </div>
               <div class="flex items-center my-3 pt-6">
                 <div><button class="bg-blue-500 rounded text-white px-8 py-2 text-sm mr-5">Save</button></div>
           </form>
            </div>
          </div>     
         </div>
        </div>		
@endsection