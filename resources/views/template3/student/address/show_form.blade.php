@extends('layouts.main')
@section('content')

@include('template3.student.sidebar')

 <div class="w-full">
         
         <div class="p-7">

          <!-- heading -->
          <div class="">
            <h1 class="text-xl lg:text-3xl md:text-3xl font-bold">Address List</h1>
            <ul class="list-reset flex items-center breadcrumbs text-gray-500 my-1">
            	@foreach($addresslist->StudentAddress as $value)
              <li class="relative mr-4 font-bold">{{$value->address}}</li>
              @endforeach 
            </ul>
          </div>
        </div>
    </div>  
@endsection