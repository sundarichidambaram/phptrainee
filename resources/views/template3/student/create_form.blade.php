@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}


  @include('template3.student.sidebar')
<!-- start -->
       <div class="w-full">
         
         <div class="p-7">

          <!-- heading -->
          <div class="">
            <h1 class="text-xl lg:text-3xl md:text-3xl font-bold">Add Student</h1>
            <ul class="list-reset flex items-center breadcrumbs text-xs text-gray-500 my-1">
              <li class="relative mr-4"><a href="{{ url('/admin/students/') }}">Student List</a></li>
              <li class="relative mr-4">Add </li>
            </ul>
          </div>
          <!-- heading -->

         <!--  section start  -->
         <div class="bg-white shadow-sm p-6 my-4 leading-loose">
           <div>
             <form action="{{ url('/admin/student/') }}" method="post" enctype="multipart/form-data">
             	@csrf
               <div class="grid grid-cols-1 lg:grid-cols-2 md:grid-cols-2 gap-8 w-full lg:w-2/3 md:w-2/3">
                <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Student Name</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full" name="sname" value="{{old('sname')}}">
                       </div>
                        <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('sname')}}</span>
                   </div>
                   <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Choose One</label>
                       <div class="pt-1">
                         <select class="bg-white px-2 py-2 rounded border text-sm w-full" name="dropdown">
                           <option value="">select</option>       
                           <option value="phone_id" @if(old('dropdown')=='phone_id') selected @endif>MobileNo</option>
                           <option value="email_id" @if(old('dropdown')=='email_id') selected @endif>Email</option>
                         </select>
                       </div>
                        <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('dropdown')}}</span>
                   </div>
                  <!-- **** -->
                  <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Email</label>
                       <div class="pt-1">
                         <input type="email" class="bg-white px-2 py-2 rounded border text-sm w-full" name="email" value="{{old('email')}}">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('email')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Password</label>
                       <div class="pt-1">
                         <input type="password" class="bg-white px-2 py-2 rounded border text-sm w-full" name="password" value="{{old('password')}}">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('password')}}</span>
                   </div>
                  <!-- **** -->
                  <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Mobile Number</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full"  name="mobileno" value="{{old('mobileno')}}">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('mobileno')}}</span>
                   </div>
                  <!-- **** -->
                  <!-- **** -->
                  <div>
                       <label class="text-xs font-medium text-gray-600">Age</label>
                       <div class="pt-1">
                         <input type="number" class="bg-white px-2 py-2 rounded border text-sm w-full" name="age" size="4" value="{{old('age')}}">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('age')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                  <div>
                       <label class="text-xs font-medium text-gray-600">Gender</label>
                       <div class="pt-1">
                        
                        <div class="border px-2 py-2 rounded flex items-center">
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="gender" value="male" @if(old('gender')=='male') checked @endif>
                           <span class="mx-2">Male</span>
                        </div>
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="gender" value="female" @if(old('gender')=='female') checked @endif>
                           <span class="mx-2">Female</span>
                        </div>
                        </div>

                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('gender')}}</span>
                   </div>
                   <!-- **** -->
                   <!-- **** -->
                   <div class="mb-2">
                       <label class="text-xs font-medium text-gray-600" >Date</label>
                       <div class="pt-1">
                         <input type="date" class="bg-white px-2 py-2 rounded border text-sm w-full" name="dob"  value="{{old('dob')}}" min="1990-01-01" max="2000-12-31">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('dob')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Image</label>
                       <div class="pt-1">
                         <input type="file" class="bg-white px-2 py-2 rounded border text-sm w-full" name="image">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('image')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Country</label>
                       <div class="pt-1">
                         <select class="bg-white px-2 py-2 rounded border text-sm w-full" name="country">
                           <option style="background-color:darkgray">-Select-</option>
                              @foreach($countries as $key=>$country)
                              <option value="{{$country->id}}"  {{ (collect(old('country'))->contains($country->name)) ? 'selected':'' }} >{{$country->name}}</option>
                              @endforeach
                         </select>
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('country')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">State</label>
                       <div class="pt-1">
                         <select class="bg-white px-2 py-2 rounded border text-sm w-full" name="state">
                           <option style="background-color:darkgray">-Select-</option>
                              @foreach($state as $key=>$value)
                              <option value="{{$value->id}}" {{ (collect(old('state'))->contains($value->name)) ? 'selected':'' }}>{{$value->name}}</option>
                              @endforeach
                         </select>
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('state')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Captcha</label>
                       <div class="pt-1">
                         <div class="g-recaptcha" data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}" name="g_recaptcha"></div>
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('g-recaptcha-response')}}</span>
                   </div>
                  <!-- **** -->
               </div>

               <div class="flex items-center my-3 pt-6">
                 <div><button class="bg-blue-500 rounded text-white px-8 py-2 text-sm mr-5">Save</button></div>
                 <div><button class="bg-gray-200 rounded text-black px-8 py-2 text-sm mr-5"><a href="list-of-church.html">Cancel</a></button></div>
               </div>
             </form>
           </div>
         </div>
         <!--  section end -->
         
         </div>
       </div>      
@endsection