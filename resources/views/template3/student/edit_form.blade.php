@extends('layouts.main')
@section('content')
{!! Toastr::message() !!}

@include('template3.student.sidebar')
<!-- start -->
       <div class="w-full">
         
         <div class="p-7">

          <!-- heading -->
          <div class="">
            <h1 class="text-xl lg:text-3xl md:text-3xl font-bold">Edit Student</h1>
            <ul class="list-reset flex items-center breadcrumbs text-xs text-gray-500 my-1">
              <li class="relative mr-4"><a href="{{ url('/admin/students/') }}">Student List</a></li>
              <li class="relative mr-4">Edit </li>
            </ul>
          </div>
          <!-- heading -->

         <!--  section start  -->
         <div class="bg-white shadow-sm p-6 my-4 leading-loose">
           <div>
             <form action="{{url('/admin/edit-students/'.$student->id)}}" method="post" enctype="multipart/form-data">
             	@csrf
               <div class="grid grid-cols-1 lg:grid-cols-2 md:grid-cols-2 gap-8 w-full lg:w-2/3 md:w-2/3">
                <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Student Name</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full" name="sname" value="@if($errors->first('sname')==''){{$student->studentname}} @else {{old('sname')}} @endif">
                       </div>
                        <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('sname')}}</span>
                   </div>
                   <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Email</label>
                       <div class="pt-1">
                         <input type="email" class="bg-white px-2 py-2 rounded border text-sm w-full" name="email" value="@if($errors->first('email')==''){{$student->email}} @else {{old('email')}} @endif">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('email')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Password</label>
                       <div class="pt-1">
                         <input type="password" class="bg-white px-2 py-2 rounded border text-sm w-full" name="password" value="@if($errors->first('password')==''){{$student->password}} @else {{old('password')}} @endif">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('password')}}</span>
                   </div>
                  <!-- **** -->
                  <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Mobile Number</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full"  name="mobileno" value="@if($errors->first('mobileno')==''){{$student->mobile}} @else {{old('mobileno')}} @endif">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('mobileno')}}</span>
                   </div>
                  <!-- **** -->
                  <!-- **** -->
                  <div>
                       <label class="text-xs font-medium text-gray-600">Age</label>
                       <div class="pt-1">
                         <input type="text" class="bg-white px-2 py-2 rounded border text-sm w-full" name="age" size="4" value="@if($errors->first('age')==''){{$student->age}} @else {{old('age')}} @endif">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('age')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                  <div>
                       <label class="text-xs font-medium text-gray-600">Gender</label>
                       <div class="pt-1">
                        
                        <div class="border px-2 py-2 rounded flex items-center">
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="gender" value="male" {{($student->gender=='male'?'checked':'')}} @if(old('gender')=='male') checked @endif>
                           <span class="mx-2">Male</span>
                        </div>
                        <div class="text-sm flex items-center px-3 text-gray-600 font-medium">
                           <input type="radio" class="" name="gender" value="female" {{($student->gender=='female'?'checked':'')}} @if(old('gender')=='female') checked @endif>
                           <span class="mx-2">Female</span>
                        </div>
                        </div>

                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('gender')}}</span>
                   </div>
                   <!-- **** -->
                   <!-- **** -->
                   <div class="mb-2">
                       <label class="text-xs font-medium text-gray-600" >Date</label>
                       <div class="pt-1">
                         <input type="date" class="bg-white px-2 py-2 rounded border text-sm w-full" name="dob"  value="{{$student->dob}}" min="1990-01-01" max="2000-12-31">
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('dob')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   
                   <div>
                       <label class="text-xs font-medium text-gray-600">Country</label>
                       <div class="pt-1">
                         <select class="bg-white px-2 py-2 rounded border text-sm w-full" name="country">
                           <option style="background-color:darkgray">-Select-</option>
                              @foreach($countries as $key=>$country)
                              @php
                                $selected = '';
                                if(old('country')!='')
                                {
                                  if($country->id==old('country'))
                                  {
                                    $selected='selected';
                                  }
                                }
                                else
                                {
                                  if($country->id==$student->country)
                                  {
                                    $selected='selected';
                                  }
                                }
                                @endphp
                              <option value="{{$country->id}}" {{$selected}}>{{$country->name}}</option>
                              @endforeach
                         </select>
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('country')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                    <div>
                       <label class="text-xs font-medium text-gray-600">State</label>
                       <div class="pt-1">
                         <select class="bg-white px-2 py-2 rounded border text-sm w-full" name="state">
                           <option style="background-color:darkgray">-Select-</option>
                              @foreach($state as $key=>$value)
                              @php
                                $selected = '';
                                if(old('state')!='')
                                {
                                  if($value->id==old('state'))
                                  {
                                    $selected='selected';
                                  }
                                }
                                else
                                {
                                  if($value->id==$student->country)
                                  {
                                    $selected='selected';
                                  }
                                }
                                @endphp
                              <option value="{{$value->id}}" {{$selected}}>{{$value->name}}</option>
                              @endforeach
                         </select>
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('state')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   <div>
                       <label class="text-xs font-medium text-gray-600">Image</label>
                       <div class="pt-1">
                         <input type="file" class="bg-white px-2 py-2 rounded border text-sm w-full" name="image">
                       </div>
                       <img border="0" src="{{ url('fileupload/' . $student->image) }}" alt="image" width="100" height="50">
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('image')}}</span>
                   </div>
                  <!-- **** -->
                   <!-- **** -->
                   {{--<div>
                       <label class="text-xs font-medium text-gray-600">Captcha</label>
                       <div class="pt-1">
                         <div class="g-recaptcha" data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}" name="g_recaptcha"></div>
                       </div>
                       <span class="text-xs font-medium text-gray-600" style="color:red">{{$errors->first('g-recaptcha-response')}}</span>
                   </div>--}}
                  <!-- **** -->
               </div>

               <div class="flex items-center my-3 pt-6">
                 <div><button class="bg-blue-500 rounded text-white px-8 py-2 text-sm mr-5">Save</button></div>
                 <div><button class="bg-gray-200 rounded text-black px-8 py-2 text-sm mr-5"><a href="list-of-church.html">Cancel</a></button></div>
               </div>
             </form>
           </div>
         </div>
         <!--  section end -->
         
         </div>
       </div>
@endsection