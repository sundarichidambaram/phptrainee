
<html>
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
</head>
<body>
​
<div style="width: 50%;margin: auto;background-color: #8ddbf5;padding:10px;">
   <div style="background-color: #fff;">
    <div style="padding: 20px 35px 0 35px;">
    <div style="margin-left: 17px;margin-right: 17px;">
    <table style="width: 100%;">
      <tbody>
        <tr>
          <td><img src="images/logo.png" alt="App Marketing logo" style="height: 60px;"></td>
          <td style="text-align: right;">
            <div style="font-size: 18px;">
              <h2 class="harngtonFont" style="color: #851313;text-transform: uppercase;margin: unset;padding-bottom: 7px;">Valentine Day</h2>
              <h2 class="harngtonFont" style="color: #0b5b76;margin: unset;">Special <span style="text-transform: uppercase;">Offer</span></h2>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
     </div>
   
​
     <div>
        
    
          <div style="position: relative;">
             <img src="images/newsletter.png" alt="APP Marketing Plus" style="width: 100%;">
             <div style="text-align: center;margin-top: 15px;"><img src="images/package-offer.png" alt="APP Marketing Plus" style="margin: auto;"></div>
          </div>
       
     </div>
     </div>
​
     <div style="background: url('images/bottom-bg.png');background-size: contain;background-position: right bottom;background-repeat: no-repeat;">
     <div style="padding: 0 35px 45px;">
        <p class="infromanFont" style="margin-bottom: 0;color: #5d5353;font-size:20px;text-align: center;">The Special Offers Packages Includes</p>
        <div style="margin-top: 20px;">
          <table style="color: #5d5353;font-size: 15px;line-height: 1.7;">
            <tbody>
            <tr>
              <td><img src="images/tick.png" style="height: 16px;"></td>
              <td style="padding-left: 8px;"><span>A Professionally written Press release about your app.</span></td>
            </tr>
            <tr>
              <td><img src="images/tick.png" style="height: 16px;"></td>
              <td style="padding-left: 8px;"><span>Press Release Distribution to 100+ channels with guaranteed inclusion of Google News & Bing News.</span></td>
            </tr>
            <tr>
              <td><img src="images/tick.png" style="height: 16px;"></td>
              <td style="padding-left: 8px;"><span>Positive review writing by the professional writers (10 review articles)</span></td>
            </tr>
            <tr>
              <td><img src="images/tick.png" style="height: 16px;"></td>
              <td style="padding-left: 8px;"><span>Guaranteed review publication of your app on 10 websites.</span></td>
            </tr>
            <tr>
              <td><img src="images/tick.png" style="height: 16px;"></td>
              <td style="padding-left: 8px;"><span>We will create a landing page website or Promotional Video for 60 Seconds.</span></td>
            </tr>
            <tr>
              <td><img src="images/tick.png" style="height: 16px;"></td>
              <td style="padding-left: 8px;"><span>App Store Rating and Reviews.</span></td>
            </tr>
            <tr>
              <td><img src="images/tick.png" style="height: 16px;"></td>
              <td style="padding-left: 8px;"><span>App Installs (1000) - Any One Platform (iOS or Android).</span></td>
            </tr>
          </tbody>
          </table>
        </div> 
​
        
         <div style="background-color: #115c75; padding: 10px 27px;border-radius: 50px;width: fit-content;margin:25px 0;"> <a href="https://appmarketingplus.com/special-offer-package/" style="color: #fff;font-size: 14px;text-decoration: none;font-weight: 400;">Sign Up Now</a></div>
       
      
      <div style="line-height: 1.7;color: #5d5353;margin-top: 18px;font-size: 14px;font-weight: 500;">
      Thanks & Regards <br/>
      Earnest<br/>
      Account Manager<br/>
      <a href="https://wri8.com" style="text-decoration: none;color: #3d6ac7;">www.appmarketingplus.com</a><br/>
      <p style="margin: 0;color: #eb5050;">+91 9843033406</p>
      </div>
    </div>
​
​
     </div>
   </div>
​
   <div style="background-color: #115c75;padding: 25px 20px 10px">
     
   </div>
</div>
</body>
​
<style>
  body {
    font-family: 'Roboto', sans-serif;
  }
  @font-face {
   font-family: harngtonFont;
   src: url("fonts/FontsFree-Net-HARNGTON.ttf");
}
@font-face {
   font-family: infromanFont;
   src: url("fonts/INFROMAN.ttf");
}
.harngtonFont {
  font-family: harngtonFont !important;
}
.infromanFont {
  font-family: infromanFont !important;
}
</style>
</html>
