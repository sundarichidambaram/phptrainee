 @extends('layouts.main')
@section('content')

{!! Toastr::message() !!}

     <div>
  
        @livewire('student-skill-list',['student_id'=>$id])
        
</div>
@endsection


@push('scripts')
<script type="text/javascript">

	window.addEventListener('show-delete-confirmation', event => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#8e918c',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                   livewire.emit('deleteConfirmed');
                }
        })
    });

  window.addEventListener('show-delete-confirm', event => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#8e918c',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                   livewire.emit('deleteConfirm');
                }
        })
    });  

    
</script>
@endpush



       

 

 
   



 

 
   

