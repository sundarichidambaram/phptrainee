<!--  sidebar start -->
       <div class="w-16 lg:w-16 bg-violet-600 sidebar">
         <div class="flex flex-col justify-between h-full">
           <ul class="list-reset py-2">
            <li class="py-3 my-4">
              <a href="dashboard.html">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white mx-auto sidebar-icon">
  <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 16.875h3.375m0 0h3.375m-3.375 0V13.5m0 3.375v3.375M6 10.5h2.25a2.25 2.25 0 002.25-2.25V6a2.25 2.25 0 00-2.25-2.25H6A2.25 2.25 0 003.75 6v2.25A2.25 2.25 0 006 10.5zm0 9.75h2.25A2.25 2.25 0 0010.5 18v-2.25a2.25 2.25 0 00-2.25-2.25H6a2.25 2.25 0 00-2.25 2.25V18A2.25 2.25 0 006 20.25zm9.75-9.75H18a2.25 2.25 0 002.25-2.25V6A2.25 2.25 0 0018 3.75h-2.25A2.25 2.25 0 0013.5 6v2.25a2.25 2.25 0 002.25 2.25z" />
</svg>
</a>

            </li>
             <li class="py-3 my-4 active">
              <a href="list-of-church.html">
                        <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
 width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000"
 preserveAspectRatio="xMidYMid meet" class="w-6 h-6 text-white fill-current mx-auto sidebar-icon">

<g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
 stroke="none">
<path d="M2480 4828 c-70 -48 -75 -62 -78 -239 l-3 -159 -137 0 c-75 0 -153
-5 -174 -10 -49 -14 -104 -76 -113 -129 -9 -53 25 -126 74 -158 32 -21 45 -23
192 -23 l159 0 0 -259 0 -259 -464 -412 c-307 -273 -469 -423 -480 -446 -13
-29 -16 -70 -16 -239 l0 -204 -292 -3 -293 -3 -60 -32 c-73 -38 -133 -99 -171
-173 l-29 -55 -3 -717 -3 -718 -92 0 c-76 0 -101 -4 -137 -22 -66 -34 -93 -79
-88 -149 5 -66 31 -109 85 -135 36 -17 130 -18 2203 -18 2073 0 2167 1 2203
18 54 26 80 69 85 135 5 70 -22 115 -88 149 -36 18 -61 22 -137 22 l-92 0 -3
718 -3 717 -34 63 c-39 71 -107 138 -178 173 -47 23 -56 24 -340 27 l-293 3 0
204 c0 169 -3 210 -16 239 -11 23 -173 173 -480 446 l-464 412 0 259 0 259
155 0 c169 0 195 6 234 56 52 66 47 155 -12 212 -46 44 -81 52 -239 52 l-137
0 -3 159 c-3 143 -5 162 -24 188 -35 48 -82 73 -134 73 -32 0 -57 -7 -80 -22z
m508 -1903 l372 -330 0 -1002 0 -1003 -320 0 -319 0 -3 479 c-3 469 -3 480
-24 508 -73 98 -195 98 -268 0 -21 -28 -21 -39 -24 -508 l-3 -479 -319 0 -320
0 0 1003 0 1002 373 331 372 331 56 -1 56 -1 371 -330z m-1548 -1645 l0 -690
-265 0 -265 0 0 678 c0 514 3 681 12 690 9 9 82 12 265 12 l253 0 0 -690z
m2758 678 c9 -9 12 -176 12 -690 l0 -678 -265 0 -265 0 0 690 0 690 253 0
c183 0 256 -3 265 -12z"/>
</g>
</svg>
</a>
</li>

<li class="py-3  my-4">
  <a href="collections.html">
  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white mx-auto sidebar-icon">
  <path stroke-linecap="round" stroke-linejoin="round" d="M21 7.5l-9-5.25L3 7.5m18 0l-9 5.25m9-5.25v9l-9 5.25M3 7.5l9 5.25M3 7.5v9l9 5.25m0-9v9" />
</svg>
</a>
</li>
           </ul>

           <ul class="list-reset py-2">
               <li class="py-3  my-3">
                <a href="#">
                 <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white mx-auto sidebar-icon">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
                </svg>
              </a>
               </li>
           </ul>
         </div>
       </div>
       <!-- end -->