@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}

<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('student-data')
        
      </div>
    </div>

      
</div>
@endsection

