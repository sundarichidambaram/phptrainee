@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}

<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('student-info')
        
      </div>
    </div>

      
</div>
@endsection

