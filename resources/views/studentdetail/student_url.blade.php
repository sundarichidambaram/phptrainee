@extends('layouts.main')
@section('content')

{!! Toastr::message() !!}

<div class="container">
  
    <div class="card">
      <div class="card-body">
        @livewire('student-url')
        
      </div>
    </div>     
</div>
@endsection

