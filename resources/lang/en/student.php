<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name_req' => 'please enter the name',
    'password_req' => 'please enter the password',
    'email_req' => 'please enter the email',
    'dob_req' => 'please choose date',
    'image_req'=> 'choose your image',
    'age_req' => 'please enter your age above 23',
    'captcha_req' => 'please check captcha',
    'country_req' => 'please select your country',
    'state_req' => 'please select your state',
    'gender_req' => 'choose your gender',
    'phone_req' => 'please enter the mobile number',
    // 'image_check' => '',
    'dob_before' => 'please enter the date before 2000',
    'email_check' => 'email is already exist',
    'dropdown_req' => 'please select the dropdown',

    'data_success' => 'Succefully Created',
    'data_error' => 'Server Error',
    'name_check' => 'the name is already exist',

    'email_valid' => 'your email does not match',
    'password_valid' =>'your password does not match',

    'course_req'=>'please enter your course',
    'skill_req' =>'please add the skill',
    'skill_check'=>'your skill is invalid',
    'course_check'=>'this course name already exist',
 
];
