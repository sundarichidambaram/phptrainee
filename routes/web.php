<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ImageuploadController;
use App\Http\Controllers\Admin\AddressController;
use App\Http\Controllers\Admin\ProfileController;

use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\DescriptionController;
use App\Http\Controllers\Admin\CollectionController;
use App\Http\Controllers\Admin\TestController;
use App\Http\Controllers\Admin\StringController;
use App\Http\Controllers\LivewireController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\StudentSkillController;
use App\Http\Controllers\AuthorizeController;
// use App\Http\Livewire\Admin\EditContact;







Auth::routes();




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix' => 'admin',
   'middleware' => ['auth','user1'],
   'namespace' => 'User',
],
     function () 
     {
        // livewire
        Route::get('/student/info', [LivewireController::class, 'info']);
        Route::get('/student/detail', [LivewireController::class, 'detail']);
        Route::get('/student/contact', [LivewireController::class, 'contact']);
        Route::get('/student/address', [LivewireController::class, 'address']);
        Route::get('/student/image', [LivewireController::class, 'image']);
        Route::get('/student/url', [LivewireController::class, 'urlname']);
        Route::get('/alert', [LivewireController::class, 'sweetalert']);
        Route::get('/popup', [LivewireController::class, 'popup']);

        // employee
        Route::get('/employee', [EmployeeController::class, 'create']);
        Route::post('/employee', [EmployeeController::class, 'store']);
        Route::get('/employees', [EmployeeController::class, 'index']);
        Route::get('/employee/edit/{id}', [EmployeeController::class, 'edit']);
        Route::post('/employee/updated/{id}', [EmployeeController::class, 'updated']);
        Route::get('/employee/delete/{id}', [EmployeeController::class, 'delete']);
        Route::get('/employee/restore', [EmployeeController::class, 'restore']);
        Route::get('/employee/trashlist', [EmployeeController::class, 'deletelist']);
        Route::get('/employee/restored/{id}', [EmployeeController::class, 'restored']);
        Route::get('/employee/fdelete/{id}', [EmployeeController::class, 'forcedelete']);

        // student skills   
        Route::get('/add/skill/{id}', [StudentSkillController::class, 'create']);
        Route::get('/skill/list/{id}', [StudentSkillController::class, 'index']);
        Route::get('/skill/delete/{id}', [StudentSkillController::class, 'destroy']);



      // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
      Route::get('/student', [StudentController::class, 'create']);
      Route::post('/student', [StudentController::class, 'store']);
      Route::get('/students', [StudentController::class, 'show']);
      Route::get('/student/edit/{id}', [StudentController::class, 'edit']);
      Route::post('/edit-students/{id}', [StudentController::class, 'update']);
      Route::get('/delete-student/{id}', [StudentController::class, 'destroy']);
      Route::get('/restore/students', [StudentController::class, 'restores']);
      Route::get('/delete/list', [StudentController::class, 'restorelist']);
      Route::get('/restore/student/{id}', [StudentController::class, 'restored']);
      Route::get('/force/delete/{id}', [StudentController::class, 'forcedelete']);

      Route::get('/student/address/{id}', [AddressController::class, 'create']);
      Route::post('/address/{id}', [AddressController::class, 'store']);
      Route::get('/profile/{id}', [ProfileController::class, 'create']);
      Route::post('/profile/student/{id}', [ProfileController::class, 'store']);
      Route::get('/add/form', [ProfileController::class, 'addform']);

      // <-- livewire -->
      Route::get('/contact', [StudentController::class, 'contact']);
      // Route::get('/contacts', [StudentController::class, 'contacts']);
      Route::get('/country', [StudentController::class, 'dropdown']);
      Route::get('/image/upload', [StudentController::class, 'imageupload']);
      Route::get('/edit/contact/{id}', [StudentController::class, 'editcontact']);
      Route::get('/delete/contact/{id}', [StudentController::class, 'deletecontact']);
      Route::get('/restore/list/', [StudentController::class, 'deletelist']);
      Route::get('/restore/contact/{id}', [StudentController::class, 'restore']);
      Route::get('/forms', [StudentController::class, 'wizard']);
      Route::get('/form/list', [StudentController::class, 'wizardlist']);
      Route::get('/multi/form', [StudentController::class, 'multiform']);


      Route::get('/product', [ProductController::class, 'create']);
      Route::post('/product', [ProductController::class, 'store']);
      Route::get('/product/edit/{id}', [ProductController::class, 'edit']);
      Route::post('/product/update/{id}', [ProductController::class, 'update']);
      Route::get('/product/image/{id}', [ImageController::class, 'create']);
      Route::post('/product/image/{id}', [ImageController::class, 'store']);
      Route::get('/product/description/{id}', [DescriptionController::class, 'create']);
      Route::post('/product/description/{id}', [DescriptionController::class, 'store']);



// });

// Route::group([
//     //'middleware' => ['guest']
// ],
//      function () 
//      {
      Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
      Route::get('/students', [StudentController::class, 'show']);//->middleware('guest');
      Route::get('/addresslist/{id}', [AddressController::class, 'show']);
      Route::get('/address/list/{id}', [AddressController::class, 'belong']);
      Route::get('/profile/list/{id}', [ProfileController::class, 'show']);
      Route::get('/profile/list/student/{id}', [ProfileController::class, 'index']);
     
      Route::get('/products', [ProductController::class, 'show']);
      Route::get('/product/images/{id}', [ImageController::class, 'show']);
      Route::get('/product/image/list/{id}', [ImageController::class, 'index']);
      Route::get('/product/descriptions/{id}', [DescriptionController::class, 'show']);
      Route::get('/product/descriptions/list/{id}', [DescriptionController::class, 'index']);

});


Route::get('/user', [UserController::class, 'index']);
Route::get('/user/{id}', [UserController::class, 'show']);

Route::get('/image', [ImageuploadController::class, 'create']);
Route::post('/upload', [ImageuploadController::class, 'store']);

Route::get('/collect/', [CollectionController::class, 'collection']);

Route::get('/collect/', [TestController::class, 'index']);
Route::get('/collects', [TestController::class, 'collection']);
Route::get('/collect/asc', [TestController::class, 'asc']);
Route::get('/collect/min', [TestController::class, 'minimum']);
Route::get('/collect/max', [TestController::class, 'maximum']);
Route::get('/collect/avg', [TestController::class, 'average']);
Route::get('/collect/sum', [TestController::class, 'sum']);

Route::get('/string/', [StringController::class, 'string']);
Route::get('/string/count/', [StringController::class, 'count']);

Route::get('/authorize', [AuthorizeController::class, 'create']);
Route::post('/authorize', [AuthorizeController::class, 'store']);


Route::get('/contacts', [StudentController::class, 'contacts']);











