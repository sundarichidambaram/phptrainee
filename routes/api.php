<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ApiStudentController;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\SkillController;
use App\Http\Controllers\API\CurlController;
use App\Http\Controllers\API\JobUserDetailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     // return $request->user();
// });

   Route::post('register',  [RegisterController::class, 'register']);
     Route::post('login',  [LoginController::class, 'login']);
     Route::post('logout',  [LoginController::class, 'logout'])->middleware('auth:sanctum');
// Route::post('/add/jobuser', [JobUserDetailController::class, 'store']);

Route::group([
    // 'prefix' => 'admin',
   'middleware' => ['auth:sanctum'],
   // 'namespace' => 'User',
],
     function () 
     {
     Route::post('/add/student', [ApiStudentController::class, 'store']);
     Route::get('/students', [ApiStudentController::class, 'index']);
     Route::post('/add/skill/{id}', [SkillController::class, 'store']);
     Route::get('/student/skills', [SkillController::class, 'index']);

     Route::post('/add/jobuser', [JobUserDetailController::class, 'store']);
     Route::post('/add/jobuserdetail', [JobUserDetailController::class, 'stored']);
});   

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/add/news', [CurlController::class, 'store']);     