<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Amaravati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Itanagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Dispur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Patna',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Raipur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),  
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Panaji',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),   
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Gandhinagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Chandigarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Shimla',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Srinagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Ranchi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),   
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Bengaluru',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Thiruvananthapuram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Bhopal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Mumbai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Imphal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '17',
        'name'          => 'Shillong',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Aizawl',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '19',
        'name'          => 'Kohima',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Bhubaneswar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Chandigarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Jaipur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '23',
        'name'          => 'Gangtok',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Chennai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);
        
      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '25',
        'name'          => 'Hyderabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '26',
        'name'          => 'Agartala',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Lucknow',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '28',
        'name'          => 'Dehradun',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Kolkata',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Coimbatore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Madurai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Trichy',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),    
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Pune',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),     
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Cochin',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);



      // new cities added//

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Visakhapatnam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Vijayawada',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Guntur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Nellore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Rajamahendravaram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Kurnool',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Kakinada',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Tirupati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Kadapa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Eluru',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Vizianagaram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Anantapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Nandyal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Ongole',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Adoni',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Madanapalle',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Machilipatnam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Tenali',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Proddatur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Chittoor',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Hindupur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Srikakulam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Bhimavaram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Tadepalligudem',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Guntakal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Dharmavaram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Gudivada',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Narasaraopet',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Kadiri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Tadipatri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Mangalagiri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '1',
        'name'          => 'Chilakaluripet',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

     //arunachal pradesh

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Aalo',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Anini',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Basar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Boleng',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Bomdila',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Changlang',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Daporijo',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Deomali',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Dirang',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Hawai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Jairampur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Khonsa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Koloriang',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Longding',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Miao',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Naharlagun',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Namsai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Pasighat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Roing',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Rupa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Sagalee',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Seppa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Tawang',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Tezu',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Yingkiong',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '2',
        'name'          => 'Ziro',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]); 

      //Assam

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Dhuburi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Dibrugarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Guwahati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Jorhat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Nagaon',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Sivasagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Silchar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Tezpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '3',
        'name'          => 'Tinsukia',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Bihar

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Muzaffarpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Gaya',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Purnia',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Begusarai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Bhagalpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Bihar Sharif',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Darbhanga',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Arrah',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Katihar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Munger',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Munger',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Chhapra',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Mehsi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Danapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Bettiah',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Saharsa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Hajipur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Sasaram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Dehri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Siwan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Motihari',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Nawada',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Bagaha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Buxar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Kishanganj',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Sitamarhi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Jamalpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Jehanabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '4',
        'name'          => 'Aurangabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       //chattisgarh

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Bhilai ',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Bilaspur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Korba',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Raj Nandgaon',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Raigarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Jagdalpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Ambikapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Dhamtari',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Chirmiri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Bhatapara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Mahasamund',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Dalli-Rajhara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Kawardha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Champa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Naila Janjgi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Kanker',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Dongragarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Tilda Neora',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Mungeli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Manendragarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Kondagaon',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Gobranawapara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Bemetara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '5',
        'name'          => 'Baikunthpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
       //Goa

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Bicholim',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Canacona',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Cuncolim',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Curchorem',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Mapusa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Margao',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Mormugao',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Pernem',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Ponda',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Quepem',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Sanguem',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Sanquelim',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '6',
        'name'          => 'Valpoi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

       //Gujarat

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Ahmedabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Surat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Vadodara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Rajkot',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]); 

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Bhavnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Jamnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Junagadh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Gandhidham',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Anand',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Navsari',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Morbi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Nadiad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Surendranagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Bharuch',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Mehsana',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Bhuj',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Porbandar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Palanpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Valsad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Vapi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Gondal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Veraval',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Godhra',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Patan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Kalol',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Dahod',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Botad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Amreli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Deesa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '7',
        'name'          => 'Jetpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Haryana

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Faridabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Rohtak',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Hisar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Panchkula',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Bhiwani',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Sirsa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Bahadurgarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Jind',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Thanesar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Kaithal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Rewari',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Narnaul',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Gurugram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Panipat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Ambala',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Yamunanagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Karnal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Sonipat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Pundri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '8',
        'name'          => 'Kosli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
      //Himachal pradesh

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Bilaspur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Chamba',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Dalhousie',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Dharmshala',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Hamirpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Kangra',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Kullu',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Mandi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Nahan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '9',
        'name'          => 'Una',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
       //Jammu & kashmir

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Anantnag',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Baramula',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Doda',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Gulmarg',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Jammu',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Kathua',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Punch',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Rajouri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '10',
        'name'          => 'Udhampur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Jharkhand

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Bokaro',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Chaibasa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Deoghar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Dhanbad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Dumka',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Giridih',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Hazaribag',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Jamshedpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Jharia',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Rajmahal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '11',
        'name'          => 'Saraikela',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
      //Karnataka

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Badami',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Ballari',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Belagavi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Bhadravati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Bidar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Chikkamagaluru',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Chitradurga',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Davangere',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Halebid',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Hassan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Hubballi-Dharwad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Kalaburagi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Kolar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Madikeri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Mandya',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Mangaluru',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Mysuru',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Raichur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Shivamogga',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Shravanabelagola',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Shrirangapattana',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Tumakuru',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '12',
        'name'          => 'Vijayapura',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Kerala

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Alappuzha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Vatakara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Idukki',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Kannur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Kollam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Kottayam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Kozhikode',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Mattancheri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Palakkad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Thalassery',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '13',
        'name'          => 'Thrissur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
      //Madya pradesh

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Balaghat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Barwani',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Betul',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Bharhut',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Bhind',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Bhojpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Burhanpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Chhatarpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Chhindwara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Damoh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Datia',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Dewas',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Dhar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Dr. Ambedkar Nagar (Mhow)',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Guna',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Gwalior',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Hoshangabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Indore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Itarsi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Jabalpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Jhabua',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Khajuraho',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Khandwa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Khargone',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Maheshwar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Mandla',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Mandsaur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Morena',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Murwara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Narsimhapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Narsinghgarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Narwar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Neemuch',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Nowgong',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Orchha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Panna',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Raisen',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Rajgarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Ratlam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Rewa',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Sagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Sarangpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Satna',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Sehore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Seoni',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Shahdol',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Shajapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Sheopur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Shivpuri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Ujjain',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '14',
        'name'          => 'Vidisha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Maharastra

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Ahmadnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Akola',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Amravati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Aurangabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Bhandara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Bhusawal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);


      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Bid',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Buldhana',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Chandrapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Daulatabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Dhule',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Jalgaon',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Kalyan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Karli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Kolhapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Mahabaleshwar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Malegaon',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Matheran',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Nagpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Nanded',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Nashik',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Osmanabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Pandharpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Parbhani',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
       

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Ratnagiri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Sangli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Satara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Sevagram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Solapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Thane',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Ulhasnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Vasai-Virar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Wardha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '15',
        'name'          => 'Yavatmal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

     //manipur

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Bishnupur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Chandel',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Churachandpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Senapati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Tamenglong',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Thoubal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '16',
        'name'          => 'Ukhrul',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
      //megalaya

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '17',
        'name'          => 'Cherrapunjee',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '17',
        'name'          => 'Tura',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '17',
        'name'          => 'Nongpoh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '17',
        'name'          => 'Nongstoin',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '17',
        'name'          => 'Mairang',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '17',
        'name'          => 'Mankachar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //mizoram

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Bairabi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Biate',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Champhai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Darlawn',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Hnahthial',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Khawhai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Khawzawl',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Kolasib',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Lawngtlai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Lengpui',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Lunglei',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Mamit',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'N. Kawnpui',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'North Vanlaiphai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Saiha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Sairang',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Saitual',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Serchhip',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Thenzawl',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Tlabung',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Vairengte',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '18',
        'name'          => 'Zawlnuam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

     //Nagaland

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '19',
        'name'          => 'Mon',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '19',
        'name'          => 'Phek',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '19',
        'name'          => 'Wokha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '19',
        'name'          => 'Zunheboto',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
       
       //Odisha

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Balangir',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Baripada',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Baleshwar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Brahmapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Cuttack',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Dhenkanal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Kendujhar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Konark',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Koraput',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Paradip',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Phulabani',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Puri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Sambalpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '20',
        'name'          => 'Udayagiri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
      //Punjab

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Amritsar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Batala',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Faridkot',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Firozpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Gurdaspur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Hoshiarpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Jalandhar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Kapurthala',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Ludhiana',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Nabha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Patiala',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Rupnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '21',
        'name'          => 'Sangrur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Rajastan

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Abu',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Ajmer',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Alwar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Amer',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Barmer',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Beawar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Bharatpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Bhilwara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Bikaner',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Bundi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Chittaurgarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Churu',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Dhaulpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Dungarpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Ganganagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Hanumangarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Jaisalmer',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Jalor',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Jhalawar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Jhunjhunu',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Jodhpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Kishangarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Kota',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Merta',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Nagaur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Nathdwara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Pali',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Phalodi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Pushkar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Sawai Madhopur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Shahpura',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Sikar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Sirohi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Tonk',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '22',
        'name'          => 'Udaipur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
 
    //Sikkim

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '23',
        'name'          => 'Gyalshing',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '23',
        'name'          => 'Lachung',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '23',
        'name'          => 'Mangan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Tamilnadu

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Arcot',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Chengalpattu',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Chidambaram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Cuddalore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Dharmapuri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Dindigul',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Erode',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Kanchipuram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Kanniyakumari',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Kodaikanal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Kumbakonam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Mamallapuram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Nagappattinam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Nagercoil',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Palayamkottai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Pudukkottai',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Rajapalayam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Ramanathapuram',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Salem',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Thanjavur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Tirunelveli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Tiruppur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Thoothukudi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Udhagamandalam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '24',
        'name'          => 'Vellore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);
      
      //Telungana

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '25',
        'name'          => 'Karimnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '25',
        'name'          => 'Khammam',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '25',
        'name'          => 'Mahbubnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '25',
        'name'          => 'Nizamabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '25',
        'name'          => 'Sangareddi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '25',
        'name'          => 'Warangal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //Uttra pradesh

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Agra',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Aligarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Amroha',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Ayodhya',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Azamgarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Bahraich',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Ballia',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Banda',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Bara Banki',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Bareilly',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Basti',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Bijnor',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Bithur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Budaun',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Bulandshahr',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Deoria',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Etah',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Etawah',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Faizabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Farrukhabad-cum-Fatehgarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Fatehpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Fatehpur Sikri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Ghaziabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Ghazipur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Gonda',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Gorakhpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Hamirpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Hardoi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Hathras',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Jalaun',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Jaunpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Jhansi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Kannauj',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Kanpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Lakhimpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Lalitpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Mainpuri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Mathura',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Meerut',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Mirzapur-Vindhyachal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Moradabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Muzaffarnagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Partapgarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Pilibhit',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Prayagraj',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Rae Bareli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Rampur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Saharanpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Sambhal',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Shahjahanpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Sitapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Sultanpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Tehri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '27',
        'name'          => 'Varanasi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //uttrakhand

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '28',
        'name'          => 'Almora',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '28',
        'name'          => 'Haridwar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '28',
        'name'          => 'Mussoorie',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '28',
        'name'          => 'Nainital',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '28',
        'name'          => 'Pithoragarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      //west bengal

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Alipore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Alipur Duar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Asansol',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Baharampur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Bally',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Balurghat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Bankura',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Baranagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Barasat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Barrackpore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Basirhat',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Bhatpara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Bishnupur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Budge Budge',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Burdwan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Chandernagore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Darjeeling',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Diamond Harbour',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Dum Dum',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Durgapur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Halisahar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Haora',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Hugli',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Ingraj Bazar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Jalpaiguri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Kalimpong',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Kamarhati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Kanchrapara',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Kharagpur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Cooch Behar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Krishnanagar',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Malda',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Midnapore',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Murshidabad',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Nabadwip',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Palashi',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Panihati',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Purulia',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Raiganj',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Santipur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Shantiniketan',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Shrirampur',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Siliguri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Siuri',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Tamluk',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

      DB::table('cities')->Insert([
        'country_id'    => '7',
        'state_id'      => '29',
        'name'          => 'Titagarh',
        'status'        => '1',
        'created_at'    => date("Y-m-d H:i:s"),
        'updated_at'    => date("Y-m-d H:i:s"),      
      ]);

    }

}
