<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class MailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('mail_templates')->insert([
            'name' =>'student_add',
            'subject' => 'Add Student',
            'content' => 'Hi , <br>
                             <p><h1>Student details are below,</h1><br>
                               <b> Name : :student_name,<br>
                                 Email : :student_email,</b><br>
                           <i>Student Added successfully,<br>
                                Thanks & Regards <br>
                                Administration Team </i><br></p>',
        ]);

        DB::table('mail_templates')->insert([
            'name' =>'student_profile',
            'subject' => 'Add profile',
            'content' => 'Hi , <br>
                          <p><h1>Student Profile  are below,</h1><br><br>
                                <b>Name : :student_name,<br>
                                 Email : :student_email,<br>
                                 MobileNo : :student_phoneno,<br>
                                 City : :student_city,<br>
                                 Gender : :student_gender,<br>
                                 Department : :student_department,<br>
                           <i> Student Added successfully,</b><br>
                                Thanks & Regards <br>
                                Administration Team </i><br></p>',
        ]);

            DB::table('mail_templates')->insert([
            'name' =>'product',
            'subject' => 'Add Product',
            'content' => 'Hello , <br>
                         <p><h1> Product details are below,</h1><br>
                             <b>Name : :product_name,<br>
                                Price(rupees) : :product_price,<br>
                                product_Code : :product_code,</b><br><br>
                            <i> Product Added successfully,<br>
                                Thanks & Regards <br>
                                Administration Team </i><br></p>',
        ]);


                 DB::table('mail_templates')->insert([
            'name' =>'addresess',
            'subject' => 'Add Address',
            'content' => 'Hi , <br>
                          <p><h1>Address details are below,</h1><br>
                            <b> Name : :student_name,<br> 
                                Email: :student_email,<br>
                                Address: :address_name,</b><br>
                            <i> Address Added successfully,<br>
                                Thanks & Regards <br>
                                Administration Team </i><br></p>',
        ]);

             DB::table('mail_templates')->insert([
            'name' =>'student_info',
            'subject' => 'Add Students',
            'content' => 'Hi , <br>
                          <p><h1>Student details are below,</h1><br>
                            <b> Name : :student_name,<br> 
                                Email: :student_email,<br>
                                MobileNo: :student_mobile,<br>
                                Gender: :student_gender,<br>
                                Address: :student_address,</b><br>
                            <i> Student Added successfully,<br>
                                Thanks & Regards <br>
                                Administration Team </i><br></p>',
        ]);
    }
}
