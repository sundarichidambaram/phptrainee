<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => "admin@mailinator.com",
            'password' => bcrypt("adminhide"),
            'user_group_id' => 1,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
           
        ]);
        DB::table('users')->insert([
            'name' => 'User',
            'email' => "user@mailinator.com",
            'password' => bcrypt("userhidden"),
            'user_group_id' => 2,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
           
        ]);
    }
}
