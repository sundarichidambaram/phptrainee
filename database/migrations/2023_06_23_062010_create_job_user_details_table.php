<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_user_details', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('age');
            $table->text('address')->nullable();
            $table->string('email');
            $table->string('mobileno')->nullable();
            $table->text('multiple_mobile')->nullable();
            $table->text('message')->nullable();
            $table->string('district');
            $table->string('state');
            $table->string('country');
            $table->text('software_language')->nullable();
            $table->enum('gender',['male','female'])->nullable();
            $table->text('language_known')->nullable();
            $table->date("dob");
            $table->text('file')->nullable();
            $table->text('multiple_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_user_details');
    }
}
