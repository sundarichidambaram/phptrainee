<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentprofileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentprofile', function (Blueprint $table) {
            $table->id();
             $table->unsignedBigInteger('student_id'); 
             $table->foreign('student_id')->references('id')->on('students');
             $table->string('studentname')->nullable();
             $table->string('email');
             $table->string('phoneno');
             $table->string('city');
             $table->string('gender');
             $table->string('department');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studentprofile');
    }
}
