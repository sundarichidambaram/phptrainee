<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Mailtemplate;


class ProductMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $product;
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
           /* $product='Congratulations! Your product has been created.';
        return $this->markdown('emails.product',[
            'product'=>$product
        ]);*/


        $mail_template = Mailtemplate::where('name','product')->first();
        $subject =  $mail_template->subject;
        $mail_content = $mail_template->content;
        $mail_content = str_replace(":product_name",$this->product->productname,$mail_content);
        $mail_content = str_replace(":product_price",$this->product->price,$mail_content);
        $mail_content = str_replace(":product_code",$this->product->product_code,$mail_content);
       

      
     

        return $this->markdown('emails.mail_content')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                        ]);

    }
}
