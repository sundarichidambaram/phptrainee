<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Mailtemplate;


class ProfileMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
        protected $profile;

    public function __construct($profile)
    {
        //
        $this->profile = $profile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       /* return $this->view('view.name');
          $profile='Congratulations! Your profile has been created.';
        return $this->markdown('emails.profile',[
            'profile'=>$profile
        ]);*/

        $mail_template = Mailtemplate::where('name','student_profile')->first();
        $subject =  $mail_template->subject;
        $mail_content = $mail_template->content;
        $mail_content = str_replace(":student_name",$this->profile->studentname,$mail_content);
        $mail_content = str_replace(":student_email",$this->profile->email,$mail_content);
        $mail_content = str_replace(":student_phoneno",$this->profile->phoneno,$mail_content);
        $mail_content = str_replace(":student_city",$this->profile->city,$mail_content);
        $mail_content = str_replace(":student_gender",$this->profile->gender,$mail_content);
        $mail_content = str_replace(":student_department",$this->profile->department,$mail_content);


      
     

        return $this->markdown('emails.mail_content')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                        ]);
    }
}
