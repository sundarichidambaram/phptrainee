<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Mailtemplate;


class AddressMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $address;
    public function __construct($address)
    {
        //
        $this->address = $address;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      /* return $this->view('view.name');
          $message='Congratulations! Your address has been created.';
        return $this->markdown('emails.address',[
            'message'=>$message
        ]);
        */
        $mail_template = Mailtemplate::where('name','addresess')->first();
        $subject =  $mail_template->subject;
        $mail_content = $mail_template->content;
        $mail_content = str_replace(":address_name",$this->address->address,$mail_content);
        $mail_content = str_replace(":student_name",optional($this->address)->student->studentname,$mail_content);
        $mail_content = str_replace(":student_email",$this->address->student->email,$mail_content);
         dd($mail_content);

        return $this->markdown('emails.mail_content')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                        ]);
    }
}
