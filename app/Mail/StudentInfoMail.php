<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Mailtemplate;

class StudentInfoMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
      protected $student;
    public function __construct($student)
    {
        $this->student = $student;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_template = Mailtemplate::where('name','student_info')->first();
        $subject =  $mail_template->subject;
        $mail_content = $mail_template->content;
        $mail_content = str_replace(":student_name",$this->student->name,$mail_content);
        $mail_content = str_replace(":student_email",$this->student->email,$mail_content);
        $mail_content = str_replace(":student_mobile",$this->student->mobileno,$mail_content);
        $mail_content = str_replace(":student_gender",$this->student->gender,$mail_content);
        $mail_content = str_replace(":student_address",$this->student->address,$mail_content);

      
     

        return $this->markdown('emails.mail_content')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                        ]);
    }
}
