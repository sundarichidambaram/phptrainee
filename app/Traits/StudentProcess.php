<?php

namespace App\Traits;
use App\Models\StudentDetail;
use App\Models\TestImage;
use App\Models\JobUserDetail;
use Auth;
use Log;
use Exception;
use App\Helpers\FileHelper;

trait StudentProcess {   

    public function createStudent($request,$user_id)
    {
    \DB::beginTransaction(); 
      try{
        $path='';
        if($request->image!=null){
            $file =$request->image;
        // dd($file);
         $path = FileHelper::uploadFile($file);
        // $path = FileHelper::uploadFile($file);
        }
        
        $create=[
            'name'=>$request->name,
            'email'=>$request->email,
            'mobileno'=>$request->mobileno,
            'gender'=>$request->gender,
            'address'=>$request->address,
            'user_id'=>$user_id,
            'image'=>$path,
         ];
         $student =StudentDetail::create($create);

         $this->imageupload($path,$student);
         \DB::commit();

         return $student;
      }
      catch(Exception $e)
      {

        Log::info($e->getMessage());
        \DB::rollBack();
      }
    }

    public function imageupload($path,$student)
    {
        // dd($path);
        $student_detail =StudentDetail::where('id',$student->id)->first();
          
        if($student_detail!=null){
            if($path!=null)
            {
                $create=[
            'student_id' =>$student->id,       
            'image'=>$path,
           ]; 
               TestImage::create($create); 
            }
        }
        
    }

    public function createUserjob($request){

      try{
        $path='';
        $multi_path='';
        // dd($request->file);
        // if($request->file!=null){
        //     $file =$request->file;
        //  $path = FileHelper::uploadFile($file);

        // }
        // if($request->multipleFiles!=null){
        //     $multi_file =$request->multipleFiles;
        //  $multi_path = FileHelper::uploadFile($file);

        // }
         // dd($request);
        // $multi_path=array();
        // // dd($request->multipleFiles);
        // if($request->multipleFiles!=null){
        //     $multiple_file =$request->multipleFiles;
            
        //     foreach($multiple_file as $value){
        //         $multiple_path = FileHelper::uploadFile($value);
        //         $multi_path[]=$multiple_path;
        //     }
        //    $upload_file=implode(" ",$multi_path);
        // }
        if($request->multiphonenumber!=null){
        $multiple_mobile=$request->multiphonenumber;
        $implode_mobile =implode(",",$multiple_mobile);
            // $implode_mobile =json_encode($request->multiPhNumbers);
        // dd($implode_mobile);
        }
        if($request->softwareLanguage!=null){
        $soft_lang=$request->softwareLanguage;
        $implode_software =implode(",",$soft_lang);
        }
        if($request->languagesKnown!=null){
        $lang_known=$request->languagesKnown;
        $implode_language =implode(",",$lang_known);
        }
        $create=[
            'first_name'=>$request->firstName,
            'last_name'=>$request->lastName,
            'age'=>$request->age,
            'address'=>$request->address,
            'email'=>$request->email,
            'mobileno'=>$request->phonenumber,
            'multiple_mobile'=>$implode_mobile,
            'message'=>$request->message,
            'district'=>$request->district,
            'state'=>$request->state,
            'country'=>$request->country,
            'software_language'=>$implode_software,
            'gender'=>$request->gender,
            'language_known'=>$implode_language,
            'dob'=>$request->dob,
            // 'file'=>$path,
            // 'multiple_file'=>$multi_path,
         ];
         // dd($create);
         $job_user =JobUserDetail::create($create);



         return $job_user;
      }
      catch(Exception $e)
      {

        Log::info($e->getMessage());
      }

    }
    
    public function createUserjobdetail($request){
    try{
        $path='';
        $multi_path='';
        if($request->file!=null){
            $file =$request->file;
         $path = FileHelper::uploadFile($file);

        }
        if($request->multipleFiles!=null){
            $multi_file =$request->multipleFiles;
         $multi_path = FileHelper::uploadFile($file);

        }
         // dd($request);
        // $multi_path=array();
        // // dd($request->multipleFiles);
        // if($request->multipleFiles!=null){
        //     $multiple_file =$request->multipleFiles;
            
        //     foreach($multiple_file as $value){
        //         $multiple_path = FileHelper::uploadFile($value);
        //         $multi_path[]=$multiple_path;
        //     }
        //    $upload_file=implode(" ",$multi_path);
        // }
        // if($request->multiPhNumbers!=null){
        // $multiple_mobile=$request->multiPhNumbers;
        // $implode_mobile =implode(" ",$multiple_mobile);
        // // dd($implode_mobile);
        // }
        // if($request->softwareLanguage!=null){
        // $soft_lang=$request->softwareLanguage;
        // $implode_software =implode(" ",$soft_lang);
        // }
        // if($request->languagesKnown!=null){
        // $lang_known=$request->softwareLanguage;
        // $implode_language =implode(" ",$soft_lang);
        // }
        $create=[
            'first_name'=>$request->firstName,
            'last_name'=>$request->lastName,
            'age'=>$request->age,
            'address'=>$request->address,
            'email'=>$request->email,
            'mobileno'=>$request->phnNumber,
            'multiple_mobile'=>$request->multiPhNumbers,
            'message'=>$request->message,
            'district'=>$request->district,
            'state'=>$request->state,
            'country'=>$request->country,
            // 'software_language'=>$request->softwareLanguage,
            'gender'=>$request->gender,
            'language_known'=>$request->languagesKnown,
            'dob'=>$request->birthDate,
            'file'=>$path,
            'multiple_file'=>$multi_path,
         ];
         // dd($create);
         $job_user =JobUserDetail::create($create);



         return $job_user;
      }
      catch(Exception $e)
      {

        Log::info($e->getMessage());
      }

    }
 }