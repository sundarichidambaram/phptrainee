<?php

namespace App\Traits;
use App\Models\CurlNews;
use Auth;
use Log;
use Exception;

trait NewsProcess {   

    public function createNews($req)
    {

        $data =$req->data;
        // dd($data);
        try{
            foreach($data as $value){
                // dd($value->image);
                $create=[
                    'id'=>$value['id'],
                    'title'=>$value['title'],
                    'story'=>$value['story'],
                    'image'=>$value['image']
                
        ];

        $news =CurlNews::create($create);

            }
        }
        catch(Exception $e){
            log::info($e->getMessage());

        }
    }
 }