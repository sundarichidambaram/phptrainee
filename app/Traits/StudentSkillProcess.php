<?php

namespace App\Traits;
use App\Models\StudentSkills;
use Auth;
use Log;
use Exception;
use App\Helpers\FileHelper;

trait StudentSkillProcess {   

    public function createSkill($req,$id)
    {
        $skill_item =$req->skill_item;
        try{
            foreach($skill_item as $value){
                $create=[
                    'student_id'=>$id,
                    'course_name'=>$value['course_name'],
                    'experience'=>$value['experience']
        ];

        $skills =StudentSkills::create($create);

            }
        }
        catch(Exception $e){
            log::info($e->getMessage());

        }
    }
 }