<?php

namespace App\Traits;
use Log;
use Exception;
trait Common {   

    public function getRequestIP()
    {
        $ip = request()->ip();
        try {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        } catch (Exception $e) {

            Log::info($e->getMessage());
        }
        return $ip;
    }
 }