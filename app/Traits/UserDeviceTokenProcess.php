<?php

namespace App\Traits;
use Exception;
use Log;
use App\Models\UserDeviceToken;
trait UserDeviceTokenProcess
{
  
  public function saveDeviceToken($user, $request) 
    {
        try{

            $create=[
                'user_id'=>$user->id,           
            ];
             if(isset($request->platform_token)){
                $create['token']=$request->platform_token;
          }
            if(isset($request->device_id)){
                $create['device_id']=$request->device_id;
          
            }
            UserDeviceToken::create($create);

        }
        catch(Exception $e)
        {
            Log::info($e->getMessage());
        }
     }   

    public function updateDeviceToken($user, $request) 
     {
        try{

             if(isset($request->platform_token)){
                $update['token']=$request->platform_token;
               $res=UserDeviceToken::where([['user_id',$user->id],['device_id',$request->device_id]])->update($update);

                    return $res;
               }
        
              return 0;  
        }
        catch(Exception $e)
        {

            Log::info($e->getMessage());
             return 0;
        } 
     }      

    public function destroyDeviceTokens($user){
          try{
            if($user->userdevicetoken()!=null)
                $user->userdevicetoken()->delete();
          }
        catch(Exception $e)
        {

            Log::info($e->getMessage());
        
        }
    }

    public function checkDeviceToken($user,$request)
    {
      $user_device_token=UserDeviceToken::where([['user_id',$user->id],['device_id',$request->device_id]])->first();
      // dd($user_device_token);
      if($user_device_token!='')
      {
        $this->updateDeviceToken($user,$request);
      }
      else{
        $this->saveDeviceToken($user,$request);
      }
    }

}