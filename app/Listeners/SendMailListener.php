<?php

namespace App\Listeners;

use App\Events\StudentEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\StudentInfoMail;

class SendMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\StudentEvent  $event
     * @return void
     */
    public function handle(StudentEvent $event)
    {
        // dd($event->student);
        Mail::to('ramamoorthi@example.com')->queue(new StudentInfoMail($event->student));
    }
}
