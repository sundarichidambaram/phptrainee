<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Exception;
use Log;


class UploadUserData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'example:create_user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
        $user=new User();
        $user->name='testuser3';
        $user->email='testuser3@mailinator.com';
        $user->password=bcrypt('testuser3');
        $user->user_group_id=2;
        $user->save();
        }
        catch(Exception $e)
        {
          Log::info($e->getMessage());
        } 
    }
}
