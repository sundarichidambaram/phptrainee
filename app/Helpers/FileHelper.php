<?php

namespace App\Helpers;
use Exception;
use Carbon\Carbon;
use Log;

class FileHelper
{   
 public static function uploadFile($file)
    {
      // dd($file);
      $path ='';
      try{
          $path = time().'.'.$file->extension('image');  
            $file->move(public_path('fileupload'),$path);
            // dd($path);
         // $path=\Storage::disk($disk)->putFile($folder, $file,'public');
      }
      catch(Exception $e)
      {

        Log::info($e->getMessage());
      }

        return $path;
    }

    public static function getFilePath($disk,$file)
    {
      $path='';
      try{
           $path=url($file);
           // dd($path);
      }
      catch(Exception $e)
      {
            Log::info($e->getMessage());
      }
        return $path;
    }
}