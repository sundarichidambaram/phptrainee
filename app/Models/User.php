<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_group_id',
    ];   /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function userprofile(){

        return $this->hasOne('App\Models\Userprofile','user_id');
    }

     public function address(){

        return $this->hasMany('App\Models\Address','user_id');
    }
    public function studentdetails(){

        return $this->hasOne('App\Models\StudentDetail','user_id');
    }
     public function userdevicetoken()
    {
        return $this->hasMany('App\Models\UserDeviceToken', 'user_id', 'id');
    }
}
