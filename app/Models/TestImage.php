<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestImage extends Model
{
    use HasFactory;
    protected $tables = 'test_images';

    protected $fillable =['student_id','image'];
}
