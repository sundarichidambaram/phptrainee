<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $table='studentprofile';

    protected $fillable = ['student_id', 'studentname', 'email', 'phoneno', 'city', 'gender', 'department'];

    public function Student(){

        return $this->belongsTo('App\Models\Student','student_id');
    }
}
