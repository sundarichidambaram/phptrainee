<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class StudentDetail extends Model
{
    use HasFactory;
    use PresentableTrait;
    protected $presenter = "App\Presenters\StudentPresenter";

    protected $table = 'student_details';
    protected $fillable = ['name', 'email','password', 'mobileno', 'gender', 'address','image','url','user_id','app_url'];


    public function user()
    {

        return $this->belongsTo('App\Models\User','user_id');
    }

    public function testimage()
    {

        return $this->hasOne('App\Models\TestImage','student_id');
    }
    public function studentskill()
    {

        return $this->hasMany('App\Models\StudentSkill','student_id');
    }
}
