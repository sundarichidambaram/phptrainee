<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentSkills extends Model
{
    use HasFactory;

    protected $tables ='student_skills';

    protected $fillable=['student_id','course_name','image','experience'];

    public function studentdetail(){

        return $this->belongsTo('App\Models\StudentDetail','student_id');
    }
}
