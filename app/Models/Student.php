<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='students';

    protected $fillable = ['studentname', 'email', 'password', 'mobile', 'age', 'gender', 'dob', 'country', 'state', 'image'];
    protected $dates = [ 'deleted_at' ];




    
    public function Profile()
    {

        return $this->hasOne('App\Models\Profile','student_id');
    }

     public function StudentAddress()
     {

        return $this->hasMany('App\Models\StudentAddress','student_id');
     }



    public function state()
    {

        return $this->hasOne('App\Models\State','name');
    }
      public function country()
    {

        return $this->hasOne('App\Models\Country','name');
    }

}
