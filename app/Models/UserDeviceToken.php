<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserDeviceToken extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected  $table='user_device_token';
    protected  $fillable=['user_id','token','device_id'];
    protected $dates=['deleted_at'];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
