<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAddress extends Model
{
    use HasFactory;
    protected $table='studentaddress';

    protected $fillable = ['student_id', 'address'];

        public function Student(){

        return $this->belongsTo('App\Models\Student','student_id');
    }

}
