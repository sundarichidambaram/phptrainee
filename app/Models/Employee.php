<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Employee extends Model
{
    use HasFactory;

     use SoftDeletes;

    protected $table='employee';

    protected $fillable = ['name', 'designation'];
    protected $dates = [ 'deleted_at' ];

    public function employeelink()
    {

        return $this->hasOne('App\Models\EmployeeLink','employee_id');
    }
}
