<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonalData extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='personal_datas';

    protected $fillable = ['name', 'email', 'mobile', 'age'];
    protected $dates = [ 'deleted_at' ];


    public static function createPersonal()
    {
        // dd($key);
        $create=[
            'name'=>'test',
            'email'=>'test@mail.com',
            'mobile' =>1234567891,
            'age' =>21
        ];
            PersonalData::create($create);
    }
    public static function deleteData()
    {
        // dd($student);
        
        $data =PersonalData::where('name','=','test')->delete();
    }
    public static function updatepersonal()
    {
        $update=[
            'name'=>'test',
            'email'=>'test@mail.com',
            'mobile' =>1234567891,
            'age' =>21
        ];
        PersonalData::where('name','=','test1')->update($update);

    }
    public static function restoreData()
    {
        // dd('hi');
        $data =PersonalData::onlyTrashed()->restore();
    }
    public static function forceDeleted()
    {
       $data =PersonalData::onlyTrashed()->forceDelete();
    }
}
