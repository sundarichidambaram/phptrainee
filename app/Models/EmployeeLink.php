<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeLink extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='employee_link';

    protected $fillable = ['address', 'employee_id'];
    protected $dates = [ 'deleted_at' ];

    // public static function createData($employee)
    // {
    //     dd($employee);
        
    // }
}
