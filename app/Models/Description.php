<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    use HasFactory;
     protected $table='description';

    protected $fillable = ['product_id', 'description'];

        public function product(){

        return $this->belongsTo('App\Models\Product','product_id');
        }
}
