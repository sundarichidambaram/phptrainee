<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
     protected $table='product';

    protected $fillable = ['id', 'productname', 'price', 'product_code'];


     public function description()
    {

        return $this->hasOne('App\Models\Description','product_id');
    }

     public function image()
     {

        return $this->hasMany('App\Models\Image','product_id');
     }


}
