<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobUserDetail extends Model
{
    use HasFactory;

     protected $table = 'job_user_details';
    protected $fillable = ['first_name','last_name', 'age','address', 'email', 'mobileno', 'multiple_mobile','message','district','state','country','software_language','gender','language_known','dob','file','multiple_file'];
}
