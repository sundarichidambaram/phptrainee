<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Contact;
use App\Models\Country;
use App\Models\State;
use Brian2694\Toastr\Facades\Toastr;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Validator;
use App\Traits\ActivityLog;
use App\Traits\Common;
use Auth;

class ContactForm extends Component
{
    use ActivityLog, Common;

 use WithFileUploads;

    public $name;
    public $email;
    public $mobileno;
    public $address;
    public $photo;
    public $g_recaptcha;
    public $date_of_birth;
    public $contact_id;
    protected $listeners = ['saveConfirmed'=>'saveContact'];
   
    protected $messages =[
        'name.required' => 'please enter the name',
        'email.required' => 'please enter the email',
        'mobileno.required'=> 'please enter the mobile number',
        'address.required' => 'please enter the address',
        'date_of_birth.required' => 'please enter the dob',
        'photo.required' => 'please upload photo',
    ];
    public function mount($id)
    {
        // dd($id);
        if(!$id==''){
          $contacts = Contact::findOrFail($id);
        $this->name = $contacts->name;
        $this->email = $contacts->email;
        $this->mobileno = $contacts->mobileno;
        $this->address = $contacts->address;
        $this->date_of_birth = $contacts->dob;
        $this->photo = $contacts->image;
        $this->contact_id = $contacts->id;
  
        $this->updateMode = true;
        }
        
    }
    public function render()
    {
      
        return view('livewire.contact.contact-form');
    }
     public function submit()
    {
        // dd('hi');
          $validatedData = $this->validate([
            'name' => 'required|min:6',
            'email' => 'required|email|unique:contacts',
            'mobileno' => 'required|numeric|digits:10|min:0|regex:/[0-9]{10}/',
            'address' => 'required',
            'date_of_birth' =>'required|before:-1years',
            'photo' => 'required|mimes:png,jpg',
            
        ]);
          $this->dispatchBrowserEvent('show-add-confirmation');
  
        // Contact::create($validatedData);
           // dd($validatedData);
          // $user =Auth::user();
          //  $contact= new Contact;
          //  $contact->name =$this->name;
          //  $contact->email = $this->email;
          //  $contact->mobileno =$this->mobileno;
          //  $contact->address =$this->address;
          //  $contact->image =$this->photo->store('images','public');
          //  // $contact->image =$this->photo;
          //  // dd($contact);
          //  $contact->dob =$this->date_of_birth;
          //  $contact->save();


          // $this->dispatchBrowserEvent('swal:modal', [
          //       'type' => 'success',  
          //       'message' => 'User Created Successfully!', 
          //       'text' => 'It will list on the users table soon.'
          //   ]);
           // $this->title='';
        // Toastr::success('Data has been saved successfully! :)', 'Success!!');
        // return redirect()->to('/admin/contacts');
    }
    public function saveContact()
    {
        // dd('hi');
        $user =Auth::user();
           $contact= new Contact;
           $contact->name =$this->name;
           $contact->email = $this->email;
           $contact->mobileno =$this->mobileno;
           $contact->address =$this->address;
           $contact->image =$this->photo->store('images','public');
           // $contact->image =$this->photo;
           // dd($contact);
           $contact->dob =$this->date_of_birth;
           $contact->save();
           
           Toastr::success('Data has been saved successfully! :)', 'Success!!');
           return redirect()->to('/admin/contacts');
    }
    public function update()
    {
        // dd($this->contact_id);
        $validatedData = $this->validate([
            'name' => 'required|min:6',
            'email' => 'required|email',
            'mobileno' => 'required|numeric|digits:10|min:0|regex:/[0-9]{10}/',
            'address' => 'required',
            'date_of_birth' =>'required|before:-1years',
            'photo' => 'required|mimes:png,jpg',
            
        ]);
           $contact=Contact::find($this->contact_id);
           $contact->name =$this->name;
           $contact->email = $this->email;
           $contact->mobileno =$this->mobileno;
           $contact->address =$this->address;
           $contact->image =$this->photo->store('images','public');
           // $contact->image =$this->photo;
           // dd($contact);
           $contact->dob =$this->date_of_birth;
           $contact->save();
           
           Toastr::success('Data has been Updated successfully! :)', 'Success!!');
           return redirect()->to('/admin/contacts');
    }
    
}
