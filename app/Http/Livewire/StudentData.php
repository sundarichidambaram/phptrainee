<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use Livewire\WithFileUploads;
use Auth;

class StudentData extends Component
{
    use WithFileUploads;

    public $name;
    public $email;
    public $contact;
    public $gender;
    public $address;
    public $photo;
    public $url_name;
    public $url_link;
    public $currentStep = 1;

    protected $messages=[
            'name.required'=> 'please enter the name',
            'email.required' => 'please enter email',
            'email.checkemail'=>'email is already taken',
            'contact.required' => 'please enter mobileno',
            'contact.digits' => 'the mobileno must be 10 digits',
            'gender.required' => 'choose your gender',
            'address.required'=> 'please fill the address',
            'photo.required' => 'choose your image',
            'photo.mimes' => 'the image must be a file of type: jpg, png.',
            'url_name.required' => 'please enter the url',
            'url_name.url' => 'your url format is invalid',
            'url_link.required' => 'please enter the url',
            'url_link.regex' => 'please enter first www.', 

        ];

    public function mount()
    {
        $this->currentStep = 1;
        $this->user =Auth::user();
        
        $this->students =StudentDetail::where('user_id',$this->user->id)->first();

    }
    public function firstStepSubmit()
    {

        $user_id =Auth::user()->id;
         $students =StudentDetail::where('user_id',$user_id)->first();
        if($students!=null){
            // return redirect()->to('/student/contact');
            $this->currentStep=2;
        }

          Validator::extend('checkemail', function($attribute, $value, $parameters) {
           $exists=StudentDetail::where('email',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });
        $validatedData = $this->validate([
            'name' => 'required',
            'email' => 'required|email|checkemail',
        ]);


        $user_id =Auth::user()->id;
           // if($students->user_id!=$user_id)
           // {

         $student = new StudentDetail;
         $student->name= $this->name;
         $student->email= $this->email;
         $student->user_id =$user_id;
         $student->save();
         
          // return redirect()->to('/student/contact');
         $this->currentStep=2;

          
    }
    public function secondStepSubmit()
    {
        // dd('hi');
        $user_id =Auth::user()->id;
         $student =StudentDetail::where('user_id',$user_id)->first();

        if($student->mobileno!=null)
        {
            // return redirect()->to('/student/address');
            $this->currentStep=3;
        }

        $validatedData = $this->validate([
            'contact' => 'required|numeric|digits:10',
            'gender' => 'required',
        ]);

         $student->mobileno= $this->contact;
         $student->gender= $this->gender;
         $student->save();
         // return redirect()->to('/student/address');

    }
    public function thirdStepSubmit()
    {
        $user_id =Auth::user()->id;
        $student =StudentDetail::where('user_id',$user_id)->first();
        if($student->address==null){

        $validatedData = $this->validate([
            'address' => 'required',
        ]);

        // $user_id =Auth::user()->id;
        //  $student =StudentDetail::where('user_id',$user_id)->first();
         $student->address= $this->address;
         $student->save();

         // return redirect()->to("/student/image");
         $this->currentStep=4;
        }
        else{
            // return redirect()->to("/student/image");
            $this->currentStep=4;
        }    

    }
    public function fourthStepSubmit()
    {

        $user_id =Auth::user()->id;
         $student =StudentDetail::where('user_id',$user_id)->first();

        if($student->image!=null){
              // return redirect()->to('/student/url');
            $this->currentStep=5;
        }
           
           $validatedData = $this->validate([
            'photo' => 'required|mimes:jpg,png|max:1024',
        ]);
          $file =$this->photo->store('images','public');
         $student->image =$file;
         $student->save();
         // return redirect()->to('/student/url');
         $this->currentStep=5;


    }
    public function submitForm()
    {
        // dd('hi');
        $validatedData = $this->validate([
            'url_name' => 'required|url',
            'url_link' => 'required|regex:/^((?:www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/',
        ]);
         $user_id =Auth::user()->id;
         $student =StudentDetail::where('user_id',$user_id)->first();
         $student->url =$this->url_name;
         $student->app_url =$this->url_link;
         $student->save();
         if($student->url!=null){
           Toastr::success('Data has been saved successfully! :)', 'Success!!');
          return redirect()->to('/admin/form/list');
         }

    }

    public function back($step)
    {
       $this->currentStep =$step;
       // dd($this->currentStep);
    }
    
    public function render()
    {
        // dd($this->currentStep);
        $user_id =Auth::user()->id;
         $students =StudentDetail::where('user_id',$user_id)->first();
         if($students==null){
           return view('livewire.student.student-data');
         }
        if($students->mobileno==null){
            $this->currentStep=2;
            return view('livewire.student.student-contact');
        }
        if($students->address==null){
            $this->currentStep=3;
            return view('livewire.student.student-address');
        }
        if($students->image==null){
            $this->currentStep=4;
            return view('livewire.student.student-image');
        }
         // if($this->currentStep==4){
         //      return view('livewire.student.student-image');
         //    }
        // if($students->url==null){
            $this->currentStep=5;
            return view('livewire.student.url-name');
        // }
    
    }
    
}
