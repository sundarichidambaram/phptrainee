<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentImage;
use App\Models\StudentDetail;
use Livewire\WithFileUploads;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use Auth;

class ImageUpload extends Component
{
  
   use WithFileUploads;
   
   public $photo;
     // public $currentStep = 1;
    public $name, $email;
    // public $password, $address, $gender, $photo,$contact,$url_name;
    public $successMsg = '';
  
    /**
     * Write code on Method
     */
    protected $messages=[
            'name.required'=> 'please enter the name',
            'email.required' => 'please enter email',
            'email.checkemail'=>'email is already taken',
            // 'password.required' =>'please enter password',
            'contact.required' => 'please enter mobileno',
            'contact.digits' => 'the mobileno must be 10 digits',
            'gender.required' => 'choose your gender',
            'address.required'=> 'please fill the address',
            'photo.required' => 'choose your image',
            'photo.mimes' => 'The image must be a file of type: jpg, png.',

        ];

    public function mount()
    {
        $this->user =Auth::user();
        
        $this->students =StudentDetail::where('user_id',$this->user->id)->first();
        // dd($this->students->name);
    }
    
    public function render()
    {
        return view('livewire.image-upload');
    }
  

    public function firstStepSubmit()
    {
        // dd('hi');
        Validator::extend('checkemail', function($attribute, $value, $parameters) {
           $exists=StudentDetail::where('email',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });

        $validatedData = $this->validate([
            'name' => 'required',
            'email' => 'required|email|checkemail',
            // 'password' => 'required',
        ]);
        $user_id =Auth::user()->id;

        $student = new StudentDetail;
         $student->name= $this->name;
         $student->email= $this->email;
         $student->user_id =$user_id;
         $student->save();

 
        // $this->currentStep = 2;
    }
  
    /**
     * Write code on Method
     */
    public function secondStepSubmit()
    {
        $validatedData = $this->validate([
            'status' => 'required',
        ]);
  
        // $this->currentStep = 3;
    }
  
    /**
     * Write code on Method
     */
    public function submitForm()
    {
        Team::create([
            'name' => $this->name,
            'price' => $this->price,
            'detail' => $this->detail,
            'status' => $this->status,
        ]);
  
        $this->successMsg = 'Team successfully created.';
  
        $this->clearForm();
  
        // $this->currentStep = 1;
    }
  
    /**
     * Write code on Method
     */
    public function back($step)
    {
        $this->currentStep = $step;    
    }
  
    /**
     * Write code on Method
     */
    public function clearForm()
    {
        $this->name = '';
        $this->price = '';
        $this->detail = '';
        $this->status = 1;
    }

    // public function submit()
    // {
    //     // dd($this->photo);
    //     // $validateData = $this->validate([
    //     //     'photo' => 'required|max:1024', // 1MB Max
    //     // ]);

    //     $file = $this->photo->store('public/images');
    //     $image =new StudentImage;
    //     $image->image = $file;
    //     $image->save();
    //     // dd($file);
    //     Toastr::success('Image saved successfully! :)', 'Success!!');
    //     return redirect()->to('/image/upload'); 
    // }
    // public function render()
    // {
    //     // dd('hi');
    //     return view('livewire.image-upload');
    // } 
}
