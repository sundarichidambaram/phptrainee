<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Brian2694\Toastr\Facades\Toastr;

class WizardList extends Component
{
    public function mount()
    {
        $this->student =StudentDetail::get();
         $this->students =StudentDetail::orderBy('id','desc')->first();

    }
    public function render()
    {
        return view('livewire.wizard-list');
    }
}
