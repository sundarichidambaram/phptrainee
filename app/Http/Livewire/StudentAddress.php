<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Auth;

class StudentAddress extends Component
{
    public $address;
    public $currentStep = 3;

     protected $messages=[
            'address.required'=> 'please fill the address',

        ];

    public function mount()
    {
        $this->currentStep = 3;
        $this->user =Auth::user();
        
        $student =StudentDetail::where('user_id',$this->user->id)->first();
        $this->address = $student->address;
    }
    public function thirdStepSubmit()
    {
        $user_id =Auth::user()->id;
        $student =StudentDetail::where('user_id',$user_id)->first();
        // if($student->address==null){

        $validatedData = $this->validate([
            'address' => 'required',
        ]);

        $user_id =Auth::user()->id;
         $student =StudentDetail::where('user_id',$user_id)->first();
         $student->address= $this->address;
         $student->save();

         return redirect()->to("/admin/student/image");
        // }
        // else{
        //     return redirect()->to("/student/image");
        // }    

    }
    public function back()
    {
        return redirect()->to('/admin/student/contact');
    }
    public function render()
    {
        return view('livewire.student.student-address');
    }
}
