<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Multiform extends Component
{
    public function render()
    {
        return view('livewire.multiform');
    }
}
