<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Country;
use App\Models\State;
use App\Models\City;

class CountryState extends Component
{

    public $countries;
    public $states;
    public $cities;

    // public $selectedCountry = null;
    public $selectedState = null;
    public $selectedCity = null;

    public function mount()
    {
      // $this->countries =Country::all();
      $this->states =State::all();
      $this->cities =collect();
    }
    public function render()
    {
        return view('livewire.contact.country-state');
    }
    public function updatedselectedState($state)
    {
         // dd($state);
        $this->cities = City::where('state_id',$state)->orderBy('name','asc')->get();
    }
    public function submit()
    {
        // dd($this->selectedState);
    }
}
