<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentSkills;
use App\Models\StudentDetail;
use Brian2694\Toastr\Facades\Toastr;

class StudentSkillList extends Component
{
    public $skill;
    public $student_id;
    public $course_name;
    public $image;
    public $selectAll =false;
    public $skill_id =[];
    protected $listeners = ['deleteConfirmed'=>'deleteSkill','deleteConfirm'=>'deleteSelected'];

    public function mount()
    {
        $this->skill =StudentSkills::where('student_id',$this->student_id)->with('studentdetail')->get();
    }
    public function delete($id)
    {
        $this->delete_id=$id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
        
    }
    public function deleteSkill()
    {
        $student_skill =StudentSkills::where('id',$this->delete_id)->first();

        $student_skill->delete();
        // $this->dispatchBrowserEvent('contactDeleted');
        Toastr::success('Data has been Deleted successfully! :)', 'Success!!');
        return redirect()->to('/admin/skill/list/'.$this->student_id);

        // $this->dispatchBrowserEvent('contactDeleted');
    }
    public function render()
    {
        return view('livewire.student-skill-list');
    }
    public function deleteall(){
         foreach($this->skill_id as $skill){
               if($skill!=0){
                  $this->dispatchBrowserEvent('show-delete-confirm');
               }
         }  
         
    }
    public function deleteSelected()
    {
         $student_skill =StudentSkills::whereIn('id',$this->skill_id)->delete();
        // dd($student_skill);
         $this->skill_id =[];
         $this->selectAll =false;
         Toastr::success('Data has been Deleted successfully! :)', 'Success!!');
        return redirect()->to('/admin/skill/list/'.$this->student_id);
    }
    public function updatedselectAll($value)
    {
       if($value)
       {
        $this->skill_id =StudentSkills::where('student_id',$this->student_id)->pluck('id');
        // dd($skill);
       }
       else{
        $this->skill_id =[];
       }
    }
}
