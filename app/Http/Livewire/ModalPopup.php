<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Contact;
use Brian2694\Toastr\Facades\Toastr;

class ModalPopup extends Component
{
    public $name, $email, $mobileno, $address, $date_of_birth;

    protected function rules(){

       return [
        'name' => 'required|min:6',
        'email' => 'required|email|unique:contacts',
        'mobileno' => 'required|numeric|digits:10|min:0|regex:/[0-9]{10}/',
        'address' => 'required',
        'date_of_birth' =>'required|before:-1years',
      ];
    } 

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    public function saveContact(){
        // dd($this->date_of_birth);

         $validatedData = $this->validate();
 
        $contact= new Contact;
           $contact->name =$this->name;
           $contact->email = $this->email;
           $contact->mobileno =$this->mobileno;
           $contact->address =$this->address;
           $contact->dob =$this->date_of_birth;
           $contact->save();

           Toastr::success('Data has been saved successfully! :)', 'Success!!');
           // $this->dispatchBrowserEvent('close-modal');
           return redirect()->to('/admin/contacts');

    }

    public function render()
    {
        return view('livewire.contact-modal');
    }
}
