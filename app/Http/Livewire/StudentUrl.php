<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Brian2694\Toastr\Facades\Toastr;
// use Illuminate\Support\Facades\Validator;
use Auth;

class StudentUrl extends Component
{

    public $url_name;
    public $url_link;
    public $currentStep = 5;
    protected $messages =[
          'url_name.required' => 'please enter the url',
          'url_name.url' => 'your url format is invalid',
          'url_link.required' => 'please enter the url',
          'url_link.regex' => 'please enter first www.'
      ];

    public function mount()
    {
        $this->currentStep = 5;
        $this->user =Auth::user();
        $this->student =StudentDetail::where('user_id',$this->user->id)->first();
        $student =StudentDetail::where('user_id',$this->user->id)->first();
        // dd($this->student);
        $this->url_name = $student->url;
        $this->url_link = $student->app_url;
    }
    public function submitForm()
    {
        

        $validatedData = $this->validate([
            'url_name' => 'required|url',
            'url_link' => 'required|regex:/^((?:www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/',
            // 'url_link'   => 'regex:preg_match("/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/i", "http://google.com")',

        ]);
         $user_id =Auth::user()->id;
         $student =StudentDetail::where('user_id',$user_id)->first();
         $student->url =$this->url_name;
         $student->app_url =$this->url_link;
         $student->save();
         // if($student->url!=null){
           Toastr::success('Data has been Updated successfully! :)', 'Success!!');
          return redirect()->to('/admin/form/list');
         // }

    }
     public function back()
    {
        return redirect()->to('/admin/student/image');    
    }
    public function render()
    {
        return view('livewire.student.student-url');
    }
}
