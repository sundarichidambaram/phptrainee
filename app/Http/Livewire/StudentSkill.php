<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\StudentSkills;
use Brian2694\Toastr\Facades\Toastr;

class StudentSkill extends Component
{
    use WithFileUploads;

    public $course_name, $image, $student_id;
    public $inputs=[];
    public $i=1;

    // protected function rules(){

    //    return [
    //           'course_name.0'=>'required',
    //           'image.0'=>'required|mimes:jpg,png',
    //           'course_name.*'=>'required',
    //           'image.*'=>'required|mimes:jpg,png',
    //   ];
    // }
    // public function updated($fields)
    // {
    //     $this->validateOnly($fields);
    // }
    protected $messages=[
        'course_name.0.required'=>'please enter the course name',
        'image.0.required'=>'please upload image',
        'image.0.mimes' => 'the image must be png,jpj format',
        'course_name.*.required'=>'please enter the course name',
        'image.*.required'=>'please upload image',
        'image.*.mimes'=>'the image must be png,jpg format',
    ];

    public function add($i){
          
          $i=$i+1;
          $this->i =$i;
          array_push($this->inputs ,$i);
          // dd($this->inputs);
    }
    public function remove($i)
    {
        unset($this->inputs[$i]);
    }
    public function store()
    {
        // dd($this->course_name);
        $validatedData =$this->validate([
              'course_name.0'=>'required',
              'image.0'=>'required|mimes:jpg,png',
              'course_name.*'=>'required',
              'image.*'=>'required|mimes:jpg,png',
      ]);
        
        foreach($this->course_name as $key=>$value)
        {
            $photo =$this->image[$key]->store('images','public');
            StudentSkills::create([
            'student_id'=>$this->student_id,
            'course_name'=>$this->course_name[$key],
            'image'=>$photo,
        ]);

        }

        Toastr::success('Data has been Stored successfully! :)', 'Success!!');
            return redirect()->to('/admin/form/list');
        
    }
    public function render()
    {
        return view('livewire.student-skill');
    }
}
