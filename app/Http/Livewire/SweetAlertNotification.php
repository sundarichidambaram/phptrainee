<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SweetAlertNotification extends Component
{

    protected $listeners = ['remove'];

    public function render()
    {
        return view('livewire.sweet-alert-notification');
    }
    public function alertSuccess()
    {
        $this->dispatchBrowserEvent('swal:modal', [
                'type' => 'success',  
                'message' => 'User Created Successfully!', 
                'text' => 'It will list on the users table soon.'
            ]);
    }

    public function alertConfirm()
    {
        $this->dispatchBrowserEvent('swal:confirm', [
                'type' => 'warning',  
                'message' => 'Are you sure?', 
                'text' => 'If deleted, you will not be able to recover this item!'
            ]);
    }
     public function remove()
    {
        /* Write Delete Logic */
        $this->dispatchBrowserEvent('swal:modal', [
                'type' => 'success',  
                'message' => 'User Delete Successfully!', 
                'text' => 'It will not list on the users table soon.'
            ]);
    }
}
