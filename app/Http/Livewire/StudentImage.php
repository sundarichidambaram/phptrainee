<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Livewire\WithFileUploads;
use Auth;

class StudentImage extends Component
{
    use WithFileUploads;

    public $photo;
    public $currentStep = 4;

     protected $messages=[
            'photo.required' => 'choose your image',
            'photo.mimes' => 'The image must be a file of type: jpg, png.',
        ];

    public function mount()
    {
        $this->currentStep = 4;
        $this->user =Auth::user();
        $this->students =StudentDetail::where('user_id',$this->user->id)->first();
        $student =StudentDetail::where('user_id',$this->user->id)->first();
        $this->photo = $student->image;
    }
    public function fourthStepSubmit()
    {

        $user_id =Auth::user()->id;
         $student =StudentDetail::where('user_id',$user_id)->first();

        // if($student->image!=null){
        //       return redirect()->to('/student/url');
        // }
           
           $validatedData = $this->validate([
            'photo' => 'required|mimes:jpg,png|max:1024',
        ]);
          $file =$this->photo->store('images','public');
         $student->image =$file;
         $student->save();
         return redirect()->to('/admin/student/url');


    }
    public function back()
    {
        return redirect()->to('/admin/student/address');
    }
    public function render()
    {
        // $user_id =Auth::user()->id;
        //  $student =StudentDetail::where('user_id',$user_id)->first();
        return view('livewire.student.update-image');
    }
}
