<?php

namespace App\Http\Livewire;

use Livewire\Component;
// use Livewire\WithPagination;
use App\Models\Contact;
use Illuminate\Support\Facades\Validator;
use Brian2694\Toastr\Facades\Toastr;

class ContactList extends Component
{

    // use WithPagination;
    
    public $search;
    public $start_date;
    public $end_date;
    public $contacts;
    public $delete_id;
    protected $listeners = ['deleteConfirmed'=>'deleteContact'];


    public function mount(){
        $this->contacts =Contact::orderBy('id','desc')->get();
        // $this->contacts =\App\Contact::paginate(5);
    }
    public function delete($id)
    {
        $this->delete_id=$id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
        
    }
    public function deleteContact()
    {
        $contact =Contact::where('id',$this->delete_id)->first();
        $contact->delete();
        // $this->dispatchBrowserEvent('contactDeleted');

        return redirect()->to('/admin/contacts');

        // $this->dispatchBrowserEvent('contactDeleted');
    }
    public function submit()
    {
        if($this->end_date!=null){
            $this->validate([
          'end_date' => 'after:start_date',
      ]);

        }
        
        if($this->search){
            $search =$this->search;
            // dd($search);
            $this->contacts = Contact::where('name','Like','%'.$search.'%')
         ->orWhere('email','like','%'.$search.'%')
         ->orWhere('address','like','%'.$search.'%')->get();

        }
         if($this->start_date){
            $start_date =$this->start_date;
            // dd($start_date);
            if($this->end_date ==null){
                $this->contacts =Contact::where('dob',$start_date)->get();
                // dd($contact);
            }

        }
        if($this->end_date){

           $end_date =$this->end_date;
           // dd($end_date);
           $this->contacts =Contact::whereBetween('dob',[$start_date,$end_date])->get();
           // dd($contact);
        }
        // $this->contacts->paginate(2);
    }
    public function restoreall()
    {
        // dd('hi');
        $contact =Contact::onlyTrashed()->restore();
         $this->dispatchBrowserEvent('contactRestored');
    }
    // public function restore($id)
    // {
    //   $contact =Contact::withTrashed()->find($id)->restore();
    //      return redirect()->to('/contacts');
    // }

    public function render()
    {
        // $contacts =Contact::get();
        
        return view('livewire.contact.contact-list');
 
    }
    
}
