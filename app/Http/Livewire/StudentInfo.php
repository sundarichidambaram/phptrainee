<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Illuminate\Support\Facades\Validator;
use Auth;

class StudentInfo extends Component
{
    public $currentStep=1;
    public $name;
    public $email;

    public function mount()
    {
        $this->currentStep=1;
        $user_id =Auth::user()->id;
        $student =StudentDetail::where('user_id',$user_id)->first();
        $this->name = $student->name;
        $this->email = $student->email;
    }
    public function firstStepSubmit()
    {
        Validator::extend('checkemail', function($attribute, $value, $parameters) {
           $exists=StudentDetail::where('email',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });

        $validatedData = $this->validate([
           'name'=>'required',
           'email'=>'required|email',
       ]);
        $user_id =Auth::user()->id;
        $student =StudentDetail::where('user_id',$user_id)->first();
        $student->name = $this->name;
        $student->email = $this->email;
        $student->save();

        return redirect()->to('/admin/student/contact');
    }
    public function render()
    {
        return view('livewire.student.student-info');
    }
}
