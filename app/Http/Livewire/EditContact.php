<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Contact;
use Brian2694\Toastr\Facades\Toastr;
use Livewire\WithFileUploads;

class EditContact extends Component
{
    use WithFileUploads;

    public $name;
    public $email;
    public $mobileno;
    public $address;
    public $photo;
    public $g_recaptcha;
    public $date_of_birth;
    public $contact_id;

    public function mount($id)
    {
        // dd($this->name);
        $contacts = Contact::findOrFail($id);
        $this->name = $contacts->name;
        $this->email = $contacts->email;
        $this->mobileno = $contacts->mobileno;
        $this->address = $contacts->address;
        $this->date_of_birth = $contacts->dob;
        $this->photo = $contacts->image;
        $this->contact_id = $contacts->id;
  
        $this->updateMode = true;
    }
    public function update()
    {
        // dd($this->contact_id);
        $validatedData = $this->validate([
            'name' => 'required|min:6',
            'email' => 'required|email',
            'mobileno' => 'required|numeric|digits:10|min:0|regex:/[0-9]{10}/',
            'address' => 'required',
            'date_of_birth' =>'required',
            'photo' => 'mimes:png,jpg'
        ]);
        $contact= Contact::find($this->contact_id);
           $contact->name =$this->name;
           $contact->email = $this->email;
           $contact->mobileno =$this->mobileno;
           $contact->address =$this->address;
           $contact->dob =$this->date_of_birth;
           $contact->image =$this->photo->store('images','public');
           $contact->save();
           Toastr::success('Data has been updated successfully! :)', 'Success!!');
        return redirect()->to('/contacts');
    }
    public function render()
    {
         // $contacts = Contact::get();

        return view('livewire.contact.edit-contact'
            // ,
            // ['contact'=>$contacts]
        );
    }
}
