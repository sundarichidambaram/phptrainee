<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Auth;

class StudentContact extends Component
{
    public $contact;
    public $gender;
    public $currentStep = 2;

     protected $messages=[
            'contact.required' => 'please enter mobileno',
            'contact.digits' => 'the mobileno must be 10 digits',
            'gender.required' => 'choose your gender',
        ];
    public function mount()
    {
        $this->currentStep = 2;
        $this->user =Auth::user();
        
        $student =StudentDetail::where('user_id',$this->user->id)->first();
        $this->contact = $student->mobileno;
        $this->gender = $student->gender;
        
    }
    public function secondStepSubmit()
    {
        $user_id =Auth::user()->id;
         $student =StudentDetail::where('user_id',$user_id)->first();

        // if($student->mobileno!=null)
        // {
        //     return redirect()->to('/student/address');
        // }

        $validatedData = $this->validate([
            'contact' => 'required|numeric|digits:10',
            'gender' => 'required',
        ]);

         $student->mobileno= $this->contact;
         $student->gender= $this->gender;
         $student->save();
         return redirect()->to('/admin/student/address');

    }
    public function back()
    {
        return redirect()->to('/admin/student/info');
    }
    public function render()
    {
        return view('livewire.student.student-contact');
    }
}
