<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentDetail;
use Livewire\WithFileUploads;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Student;
use Auth;

class WizardForm extends Component
{
    use WithFileUploads;

    public $currentStep = 1;
    public $name, $email,$password, $address, $gender, $photo,$contact,$url_name;
    public $successMessage = '';
    public $user;
    public $students='';

    // protected $rules = [
    //     'name' => 'required|min:6',
    //     'email' => 'required|email',
    // ];
     protected $messages=[
            'name.required'=> 'please enter the name',
            'email.required' => 'please enter email',
            'email.checkemail'=>'email is already taken',
            // 'password.required' =>'please enter password',
            'contact.required' => 'please enter mobileno',
            'contact.digits' => 'the mobileno must be 10 digits',
            'gender.required' => 'choose your gender',
            'address.required'=> 'please fill the address',
            'photo.required' => 'choose your image',
            'photo.mimes' => 'The image must be a file of type: jpg, png.',

        ];
    
    public function mount()
    {
        $this->user =Auth::user();
        
        $this->students =StudentDetail::where('user_id',$this->user->id)->first();
        // dd($this->students->name);
    }

    public function render()
    {
        return view('livewire.wizard-form');
    }

    public function firstStepSubmit()
    {
        if($this->name){
        // dd($this->name);
        
        Validator::extend('checkemail', function($attribute, $value, $parameters) {
           $exists=StudentDetail::where('email',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });

        $validatedData = $this->validate([
            'name' => 'required',
            'email' => 'required|email|checkemail',
            // 'password' => 'required',
        ]);
        // if($this->name){
        // $user =StudentDetail::orderBy('id','desc')->first();
           $user_id =Auth::user()->id;
           // dd($students);
           // if($students->user_id!=$user_id)
           // {

         $student = new StudentDetail;
         $student->name= $this->name;
         $student->email= $this->email;
         $student->user_id =$user_id;
         $student->save();
          // }

 
        $this->currentStep = 2;
        }
        else{
            $this->currentStep = 2;
        }
    }

    public function secondStepSubmit()
    {
        // dd($this->contact);
        if($this->contact || $this->gender){
            // dd('hi');
        Validator::extend('checkgender', function($attribute, $value, $parameters) {
            $array=array('male','female');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });

        $validatedData = $this->validate([
            'contact' => 'required|numeric|digits:10',
            'gender' => 'required|checkgender',
        ]);

         // $user =StudentDetail::orderBy('id','desc')->first();
        // dd('hi');
        
          $user_id =Auth::user()->id;
          $student =StudentDetail::where('user_id',$user_id)->first();
          $student->mobileno =$this->contact;
          $student->gender =$this->gender;
          // dd($student);
          $student->save();
  
        $this->currentStep = 3;
        }
        else
        {
            $this->currentStep = 3; 
        }
       
    }
    public function thirdStepSubmit()
    {
        if($this->address){

        
        $validatedData = $this->validate([
            'address' => 'required',
        ]);

          // $user =StudentDetail::orderBy('id','desc')->first();
        $user_id =Auth::user()->id;
          // dd($user);
        $student =StudentDetail::where('user_id',$user_id)->first();
        $student->address =$this->address;
        $student->save();

         $this->currentStep = 4;
         }
         else{
             $this->currentStep = 4;
         }
        

    }
    public function fourthStepSubmit()
    {
        if($this->photo){

        $validatedData = $this->validate([
            'photo' => 'required|mimes:jpg,png|max:1024',
        ]);
         // $user =StudentDetail::orderBy('id','desc')->first();
        $user_id =Auth::user()->id;
         $file =$this->photo->store('images','public');
         $student =StudentDetail::where('user_id',$user_id)->first();
         $student->image =$file;
         $student->save();

         $this->currentStep = 5;
        }
        else{
            $this->currentStep = 5;
        }
    }
  
    public function submitForm()
    {
        // dd('hi');
         // $user =StudentDetail::orderBy('id','desc')->first();
        $user_id =Auth::user()->id;
        $student =StudentDetail::where('user_id',$user_id)->first();
         $student->url =$this->url_name;
         $student->save();
  
        Toastr::success('Data has been saved successfully! :)', 'Success!!');
        return redirect()->to('/forms');
    }
 
    public function back($step)
    {
        $this->currentStep = $step;    
    }

    // public function clearForm()
    // {
    //     $this->name = '';
    //     $this->email = '';
    //     $this->password = '';
    //     $this->contact = '';
    //     $this->gender = '';
    //     $this->addres= '';
    //     $this->photo = '';

    // }
}
