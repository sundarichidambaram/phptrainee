<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

use App\Models\StudentAddress;


class StudentAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
              Validator::extend('checkaddress', function($attribute, $value, $parameters) {

           $count=StudentAddress::where('student_id',\Request::segment(2))->count();
                 if($count<=4)
           {
            return true;
           }
           return false;
        });

        
        return [
            'address' => 'required|checkaddress',
            
        ];
    }
    public function messages(){
        return[
            'address.required' => 'please fill the address',
            'address.checkaddress' => 'Only you can store five address',
        ];

    }
}
