<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\StudentDetail;

class StudentDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('checkemail', function($attribute, $value, $parameters) {
           $exists=StudentDetail::where('email',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });
        Validator::extend('checkname', function($attribute, $value, $parameters) {
           $exists=StudentDetail::where('name',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });

        Validator::extend('checkgender', function($attribute, $value, $parameters) {
            $array=array('male','female');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });

        $rules['name'] = ['bail','required','min:7','checkname'];
        $rules['email'] = ['bail','required','email','checkemail'];
        $rules['mobileno'] = ['bail','required','digits:10'];
        $rules['gender'] = ['bail','required','checkgender'];
        $rules['image'] = ['bail','mimes:png,jpg'];
        $rules['address'] = ['bail','required'];
        return $rules;
    }
    public function messages()
    {
        return[
            'name.required' => trans('student.name_req'),
            'email.required' => trans('student.email_req'),
            'mobileno.required' => trans('student.phone_req'),
            'gender.required' => trans('student.gender_req'),
            'gender.checkgender' => trans('student.gender_check'),
            'email.checkemail' => trans('student.email_check'),
            'name.checkname' => trans('student.name_check'),
        ];
    }
}
