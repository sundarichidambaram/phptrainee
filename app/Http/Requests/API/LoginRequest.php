<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Hash;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(requet('username'));
        $this->user = User::where('email', request('username'))->first();
   

        Validator::extend('checkuser', function ($attribute, $value, $parameters, $validator) {
        if(is_null($this->user))
          {       
            return false;
          }
        return true; 
        });

        Validator::extend('checkpassword', function ($attribute, $value, $parameters, $validator) {
        if ($this->user != null && request('password')!=null) 
        {
          $password = $this->user->password;
          if (Hash::check(request('password'), $password)) 
          {
            return true;
          }
          return false;
        }
        return true;
        });

        $rules['username'] = ['bail','required','checkuser'];
        $rules['password'] = ['bail','required','checkpassword'];
        return $rules;
    }
    public function messages()
    {
        return[
            'username.required' =>trans('student.email_req'),
            'username.checkuser' =>trans('student.email_valid'),
            'password.required' =>trans('student.password_req'),
            'password.checkpassword'=>trans('student.password_valid'),
        ];
    }
}
