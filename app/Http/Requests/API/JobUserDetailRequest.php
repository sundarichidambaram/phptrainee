<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\JobUserDetail;

class JobUserDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Validator::extend('checkgender', function($attribute, $value, $parameters) {
        //     $array=array('male','female','others');
            
        //     if(in_array($value,$array))
        //     {
        //         return true;
        //     }
        //     return false;
        // });

        $rules['firstName'] =['bail','required'];
        $rules['lastName'] =['bail','required'];
        $rules['age'] = ['bail','required'];
        $rules['address'] = ['bail','required'];
        $rules['email'] = ['bail','required'];
        $rules['phonenumber'] = ['bail','required','digits:10'];
        $rules['multiphonenumber.*'] = ['bail','required','digits:10'];
        $rules['message'] = ['bail','required'];
        $rules['district'] = ['bail','required'];
        $rules['country'] = ['bail','required'];
       $rules['state'] = ['bail','required'];
       $rules['softwareLanguage.*'] = ['bail','required'];
      
      $rules['gender'] = ['bail','required'];
      $rules['languagesKnown.*'] = ['bail','required'];

      $rules['dob'] = ['bail','required'];
      // $rules['file'] = ['bail','required','mimes:doc,docx,pdf','max:2048'];
      
      
      return $rules;
    }

    public function messages()
    {
        return [
            'firstName.required' => "please enter first_name",
            'lastName.required' => "please enter last_name",
            'age.required' => "please enter your age",
            'address.required' =>"please enter adddress",
            'email.required' => "please enter adddress",
            'dob.required' => "please enter dob",
            'phonenumber.required' => "please enter mobileno",
            'multiphonenumber.*.required'=> "please enter mobileno",
            'message.required' => "please enter message",
            'district.required' => "please select district",
            'country.required' => "please select country",
            'state.required' =>"please select state",
            'gender.required' => "please enter adddress",
            'softwareLanguage.*.required' =>"please select software_language",
            'languagesKnown.*.required' => "please select language",
            // 'file.required' =>"please enter valid file",
            // 'file.mimes' =>'please uplode file doc,docx,pdf formet',
            // 'g-recaptcha-response.required'=> trans('student.captcha_req'),
        ];
    }
}
