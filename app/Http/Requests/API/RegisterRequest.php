<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules['name'] = ['bail','required','min:6'];
        $rules['email'] = ['bail','required','email','unique:users'];
        $rules['password'] = ['bail','required','min:8'];
        return $rules;
        // 'name' => 'required|string|between:2,100',
        //     'email' => 'required|string|email|max:100|unique:users',
        //     'password' => 'required|string|confirmed|min:6',
    }
    public function messages()
    {
        return[
            'name.required' =>trans('student.name_req'),
            'email.required'=>trans('student.email_req'),
            'email.unique' =>trans('student.email_check'),
            'password' =>trans('student.password_req'),
        ];
    }
}
