<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\StudentSkills;

class StudentSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $req =$this->all();
        // dd(request()->id);
        Validator::extend('checkskill',function($attribute,$value,$parameters){
            $skills =array('beginner','experienced','master','grand master');
            if(in_array($value,$skills))
            {
              return true;
            }
            return false;
        });
        Validator::extend('checkcourse',function($attribute,$value,$parameters){
            $course =StudentSkills::where('student_id',$this->id)->where('course_name',$value)->exists();
            if($course)
           {
            return false;
           }
           return true;
        });
        
        return [
            'skill_item.*.course_name'=>'bail|required|checkcourse',
            'skill_item.*.experience'=>'bail|required|checkskill',
        ];
    }
    public function messages(){
        return [
            'skill_item.*.course_name.required'=>trans('student.course_req'),
            'skill_item.*.experience.required'=>trans('student.skill_req'),
            'skill_item.*.experience.checkskill'=>trans('student.skill_check'),
            'skill_item.*.course_name.checkcourse'=>trans('student.course_check'),
        ];
    }

    public function all($keys = null){

        if(empty($keys)){
            return parent::json()->all();
        }

        return collect(parent::json()->all())->only($keys)->toArray();
    }
}
