<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Profile;
use App\Models\Student;



class StudentProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         Validator::extend('checkdepartment', function($attribute, $value, $parameters) {
            $array=array('cse','civil','mech');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });

         Validator::extend('checkgender', function($attribute, $value, $parameters) {
            $array=array('male','female');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });
            Validator::extend('checkprofile', function($attribute, $value, $parameters) {

           $exists=Profile::where('id',\Request::segment(3))->exists();
          if($exists)
           {
            return false;
           }
           return true;
        });


        return [
                 'sname' => 'required|checkprofile',
                'email' => 'required|email',
                'phoneno' => 'required|numeric|digits:10|min:0|regex:/[0-9]{10}/',
                'city' => 'required',
                'gender' => 'required|checkgender',
                'department' => 'required|checkdepartment',
             
  
        ];
    }
     public function messages()
    {
        return [
            'sname.required' => 'Please enter your fullname',
            'email.required' => 'Please enter your email id',
            'phoneno.required' => 'Please enter your phone no',
            'city.required' => 'please enter your city',
            'gender.required' => 'choose your gender',
            'department.required' => 'choose your department',
            'department.checkdepartment' => 'choose your valid department',
            'sname.checkprofile' => 'The student profile is already exist',

            //'profile' => 'invalid'

        ];
    }
}
