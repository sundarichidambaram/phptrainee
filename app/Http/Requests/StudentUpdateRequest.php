<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Student;

class StudentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('checkcountry', function($attribute, $value, $parameters) {
            $array=array('India','US','Uk');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });
        Validator::extend('checkdropdown', function($attribute, $value, $parameters) {
            $array=array('phone_id','email_id');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });


        Validator::extend('checkemail', function($attribute, $value, $parameters) {
           $exists=Student::where('email',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });

        
            $rules['sname'] =['required'];
      $rules['dropdown'] =['required','checkdropdown'];

      if(request('dropdown')=='email_id')
      {
        $rules['email'] = ['required'];
      }
      if(request('dropdown')=='phone_id')
      {
        $rules['mobileno'] = ['required'];
      }

      // $rules['email'] = ['checkemail'];
      $rules['password'] = ['required','min:8'];
      $rules['age'] = ['required'];
      // if(request('dob')){

      // }
      $rules['gender'] = ['required'];
      $rules['dob'] = ['required'];

      if(request('email'))
      {
        $rules['dob'] = ['before:2000-01-01'];
      }
      $rules['image'] = ['required','image','mimes:png,jpg'];
      // $rules['country'] = ['required','checkcountry'];
      // $rules['state'] = ['required'];
      // $rules['g-recaptcha-response'] = ['required',new ReCaptcha];
      
      return $rules;
    }
    public function messages()
    {
        return [
            'sname.required' => trans('student.name_req'),
            'dropdown.required' => trans('student.dropdown_req'),
            'email.required' => trans('student.email_req'),
            'password.required' => trans('student.password_req'),
            'mobileno.required' => trans('student.phone_req'),
            'age.required' => trans('student.age_req'),
            'gender.required' => trans('student.gender_req'),
            'dob.required' => trans('student.dob_req'),
            'dob.before' => trans('student.dob_before'),
            'country.required' => trans('student.country_req'),
            'image.required' => trans('student.image_req'),
            'gender.checkgender' => trans('student.gender_check'),
            'email.checkemail' => trans('student.email_check'),
            'country.checkcountry' => trans('student.country_req'),
            'state.required' => trans('student.state_req'),
            // 'image.checkimage' =>trans('student.image_check'),
            'g-recaptcha-response.required'=> trans('student.captcha_req'),

        ];
    }
}
