<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Image;



class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
          Validator::extend('checkimage', function($attribute, $value, $parameters) {
         // $image=Image::where('product_id',\Request::segment(3))->count();
             $extension=request('image')->extension();
            $image=Image::where('image_type',$extension)->count();
         // dd($image);
         
         
          //dd($extension);     
        if($extension=="jpg"){
            if($image<5){
                return true;
            }     
        }
        else if($extension=="png"){
            if($image<5){
                return true;
            }
        }
        return false;  
    });



        return [
            'image' => 'required|image|checkimage|mimes:png,jpg|max:2048',
        ];
    }
    public function messages(){
        return[
            'image.required' => 'Please upload valid image',
            'image.mimes' => 'please upload image jpeg,png,jpg,gif,svf format',
            'image.checkimage' => 'you can store only five images jpg and png format',
        ];
    }
}
