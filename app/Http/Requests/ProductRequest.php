<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Rules\ProductPrice;
use App\Models\Product;



class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         /*Validator::extend('checkprice', 
             function ($attribute, $value, $fail) {
            $value = floatval($value);

            if ($value < 1000 && $value >= 50000) {
                $fail($attribute . ' is invalid');
            }
    
         });*/
          /*Validator::extend('checkprice', function($attribute, $value, $parameters) {
            
            if($value>=1000 && $value<=3000)
            {
                return true;
            }

            return false;
        });*/
            Validator::extend('checkprice', function($attribute, $value, $parameters) {
            $count=Product::count();
                
            if($count<3){

                return true;
             }
                else if($count==4){
                    if($value>=1000 && $value<=3000){

                  return true;
            }
         }
                else if($count>4){
                    if($value>=5000 && $value<=10000){

                  return true;
            }
         }
            
                   
            return false;
          
        });
             /* Validator::extend('checkamount', function($attribute, $value, $parameters) {
            $count=Product::where('price',\Request::segment(1))->count();
                 if($count<=4){
                    if($value>=5000 && $value<=10000){

                  return true;
            }
         }
     });*/
          
         Validator::extend('checkcode', function($attribute, $value, $parameters) {
           $exists=Product::where('product_code',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });

        $rules['price'] = ['required','numeric','checkprice'/*,new ProductPrice*/]; 
         $rules['name'] = ['required']; 
          $rules['product_code'] = ['required','checkcode'];      

        return $rules;
         /*[
           'name' => 'required',
            'price' => 'required|numeric|checkprice',
            'product_code' => 'required|checkcode'
        ];*/
    }
    public function messages(){
        return[
            'name.required' => 'Please enter your name',
        'price.required' => 'Please enter the price',
        'price.checkprice' => 'The price must be 1000 to 3000',
        'product_code.checkcode' => 'The code is already exist',
         //'price.checkamount' => 'The price must be 5000 to 10000',


    ];
    }
}
//