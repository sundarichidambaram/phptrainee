<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Rules\ReCaptcha;
use App\Models\Student;
use Carbon\Carbon;

class StudentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        Validator::extend('checkcountry', function($attribute, $value, $parameters) {
            $array=array('India','US','Uk');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });
        Validator::extend('checkdropdown', function($attribute, $value, $parameters) {
            $array=array('phone_id','email_id');
            
            if(in_array($value,$array))
            {
                return true;
            }
            return false;
        });


        Validator::extend('checkemail', function($attribute, $value, $parameters) {
           $exists=Student::where('email',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });
        Validator::extend('checkname', function($attribute, $value, $parameters) {
           $exists=Student::where('studentname',$value)->exists();
           if($exists)
           {
            return false;
           }
           return true;
        });

       /* Validator::extend('checkimage', function($attribute, $value, $parameters) {
          $extension=request('image')->extension();
          dd($extension);
          //$file = pathinfo(public_path('fileupload'),$count);
         
        if($count<4){
            return true;
    
        }
        return false;
       
           //$rules = ['image|mimes:png,jpg'];
         /*if ($file<=5){
                   return true;
               
          }
          return false;*/
     

       //$count = 0;
        //$ImageRules = ['image|mimes:png,jpg' ,'sometimes|image|mimes:png,jpg'];
       //$imageRule = [];

    /* for ($i = 0; $i < (5 - $count); $i++) {
        $rule = ($i == 0) ? $ImageRules[0] :$ImageRules[1] ;
        $key = 'image' . $i;
        $imageRule[$key] = $rule;
}
      });*/
       //$images = ['mimes|png,jpg'];
         

        // return [ 
        //         'sname' => 'required',
        //         'email' => 'required|email|checkemail',
        //         'password' => ['required','min:8'],
        //         // 'regex:/[A-Z]/','regex:/[a-z]/','regex:/[0-9]/','regex:/[!@#$%^&*]/'],
        //         'mobileno' => 'required|numeric|digits:10|min:0|regex:/[0-9]{10}/',
        //         'age' => 'required|numeric|min:18|max:100',
        //         'gender' => 'required',
        //         'dob' => 'required|before:-10years',
        //         'image' => 'required|image|mimes:png,jpg',
        //         'country' => 'required|checkcountry',
        //         'state' => 'required',
        //         'g-recaptcha-response' => ['required', new ReCaptcha]

        //         // 'g-recaptcha-response' =>function($attribute, $value, $fail) {
        //         //     $secretkey = env('RECAPTCHA_SECRET_KEY');
        //         //     $response = $value;
        //         //     $userIp = $_SERVER['REMOTE_ADDR'];
        //         //     $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretkey&response=$response&remotip=$userIp";
        //         //     $response = \file_get_contents($url);
        //         //     $response = json_decode($response);
        //         //     // dd($response);
        //         //     if(!$response->success){
                            
        //         //      \Session::put('g_recaptcha','please check recaptcha');
        //         //      \Session::put('alert-class','alert-danger');
        //         //      $fail($attribute. 'please check recaptcha');
        //         //     }
        //         // }    

        //  ];
      $rules['sname'] =['required','min:7','checkname'];
      $rules['dropdown'] =['required','checkdropdown'];

      if(request('dropdown')=='email_id')
      {
        $rules['email'] = ['required'];
      }
      if(request('dropdown')=='phone_id')
      {
        $rules['mobileno'] = ['required','digits:10'];
      }

      // $rules['email'] = ['checkemail'];
      $rules['password'] = ['required','min:8'];
      // $rules['age'] = ['required'];

      if(request('dob')){
        $date =request('dob');
        // $year = Carbon::parse($date)->format('Y');
        $now = Carbon::now()->year;
        $start_date= new \DateTime($date);
        $start_now= new \DateTime($now);
        $year_diff = $start_date->diff($start_now);
        $year =$year_diff->y;
        // dd($year);
        if($year<23)
        {
          $rules['age'] = ['required'];
        }
      }
      $rules['gender'] = ['required'];
      $rules['dob'] = ['required'];

      if(request('email'))
      {
        $rules['dob'] = ['before:2000-01-01'];
      }
      $rules['image'] = ['required','image','mimes:png,jpg'];
      $rules['country'] = ['required','checkcountry'];
      $rules['state'] = ['required'];
      $rules['g-recaptcha-response'] = ['required',new ReCaptcha];
      
      return $rules;
    }

    public function messages()
    {
        return [
            'sname.required' => trans('student.name_req'),
            'dropdown.required' => trans('student.dropdown_req'),
            'email.required' => trans('student.email_req'),
            'password.required' => trans('student.password_req'),
            'mobileno.required' => trans('student.phone_req'),
            'age.required' => trans('student.age_req'),
            'gender.required' => trans('student.gender_req'),
            'dob.required' => trans('student.dob_req'),
            'dob.before' => trans('student.dob_before'),
            'country.required' => trans('student.country_req'),
            'image.required' => trans('student.image_req'),
            'gender.checkgender' => trans('student.gender_check'),
            'email.checkemail' => trans('student.email_check'),
            'country.checkcountry' => trans('student.country_req'),
            'state.required' => trans('student.state_req'),
            // 'image.checkimage' =>trans('student.image_check'),
            'g-recaptcha-response.required'=> trans('student.captcha_req'),

        ];
    }
}
