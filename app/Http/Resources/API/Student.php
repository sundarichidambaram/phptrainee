<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;

class Student extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        // $num ='+91';
        $file =$this->image;
         // dd($file);
        $name =$this->present()->firstName();
        $address =$this->present()->firstAddress();
        $fullname =$this->present()->fullName();
        $phoneno =$this->present()->mobilenumber();
        // dd($phoneno);
        if($file!=null)
        {
            $image =$this->present()->getFilePathForDisplay($file);
            // dd($image);
        }
        else{
            $image =$this->present()->getFilePathForDisplay('default_profile.png');
        }
        

        return[
            'id'=>$this->id,
            'name'=>$fullname,
            'email'=>$this->email,
            'mobileno'=>$phoneno,
            'gender'=>$this->gender,
            'address'=>$address,
            'image'=>$image,
        ];
    }
}
