<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LivewireController extends Controller
{
    public function detail()
    {
        return view('studentdetail.student_detail');
    }
    public function contact()
    {
        return view('studentdetail.student_contact');
    }
    public function address()
    {
        return view('studentdetail.student_address');
    }
    public function image()
    {
        return view('studentdetail.student_image');
    }
    public function urlname()
    {
        return view('studentdetail.student_url');
    }
    public function info()
    {
        return view('studentdetail.student_info');
    }
    public function sweetalert()
    {
        return view('sweet_alert');
    }
    public function popup()
    {
        return view('modal_popup');
    }
}
