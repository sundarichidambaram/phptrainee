<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\EmployeeLink;

class EmployeeController extends Controller
{
    public function create()
    {
        return view('employee.create_form');
    }
    public function store(EmployeeRequest $request)
    {
        try{

        // dd($request);
        $employee = new Employee;
        $employee->name = $request->name;
        $employee->designation = $request->designation;
        // $employee->address = $request->address;
        $employee->save();
    }
    catch(Exception $e){
            Log::info($e->getMessage());
        }

        Toastr::success('Data has been saved successfully! :)', 'Success!!');
        return Redirect::to(url('/admin/employees/'));

    }
    public function index()
    {
        $employee =Employee::with('employeelink')->paginate(5);
        return view('employee.show_form',[
            'employee'=>$employee
        ]);
    }
    public function edit($id)
    {
        $employee =Employee::find($id);
        $employee_link =EmployeeLink::where('employee_id',$id)->first();
        return view('employee.edit_form',[
          'employee'=>$employee,
          'employee_link'=>$employee_link
      ]);
    }
    public function updated(EmployeeRequest $request,$id)
    {
        try{
      $employee= Employee::find($id);
      $employee->name = $request->name;
      $employee->designation = $request->designation;
      $employee->save();
    }
     catch(Exception $e){
            Log::info($e->getMessage());
        }

      Toastr::success('Data has been Updated successfully! :)', 'Success!!');
        return Redirect::to(url('/admin/employees/'));
    }
    public function delete($id)
    {
      $employee =Employee::where('id',$id)->with('employeelink')->first();
      $employee->delete();

      Toastr::success('Data has been Deleted successfully! :)', 'Success!!');
        return Redirect::to(url('/admin/employees/'));
    }
    public function restore()
    {
        $employee =Employee::onlyTrashed()->restore();
        return Redirect::to(url('admin/employees/'));

    }
    public function deletelist()
    {
        // dd('hi');
        $employee =Employee::onlyTrashed()->get();

        return view('employee.delete_list',['employee'=>$employee]);
         return Redirect::to(url('/admin/employees'));
    }
    public function restored($id){
       
        $employee=Employee::withTrashed()->find($id);
        $employee->restore();

         Toastr::success('Data has been Restore successfully! :)', 'Success!!');
        return Redirect::to(url('admin/employee/trashlist'));
    }
    public function forcedelete($id)
    {
        $employee =Employee::where('id',$id)->with('employeelink')->onlyTrashed()->first();
        // dd($employee);
        $employee->forceDelete();
        return Redirect::to(url('admin/employee/trashlist'));
    }
}
