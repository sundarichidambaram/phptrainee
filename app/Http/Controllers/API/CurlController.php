<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Traits\NewsProcess;
use GuzzleHttp\Client;
use App\Traits\ActivityLog;
use App\Traits\Common;
class CurlController extends Controller
{
    use NewsProcess, ActivityLog, Common;

    public function store()
    {
      try{
    $client = new Client(['base_uri' => 'https://bcx-dev.cryptoexchange4u.com/']);

     $response = $client->request('GET', 'api/news',['verify' => false]);
        
     $response=$response->getBody();
     $response_decode =json_decode($response,true);
     $req = new \Illuminate\Http\Request($response_decode);
      dd($req);
     $news =$this->createNews($req);
       
       return response()->json([
            'success'=>true,
            'message' =>trans('student.data_success'),                             
           ],200);

      } 
     catch(Exception $e){
            Log::info($e->getMessage());

       return response()->json([
        'success'=>false,
        'message' =>trans('student.data_error'),
       ],500); 

        }

    }
}
