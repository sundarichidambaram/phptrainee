<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\RegisterRequest;
use App\Models\UserGroup;
use App\Traits\ActivityLog;
use App\Traits\Common;
use App\Models\User;
use Exception;
use Log;

class RegisterController extends Controller
{
    use ActivityLog ,Common;

    public function register(RegisterRequest $request)
    {

        try{
        // dd($request);
        // $user_id =$request->id;
        // dd($user_id);
        $create_user=[
            'name'=>ucfirst($request->name),

        ];
        $user_group =UserGroup::create($create_user);
         
         $user =UserGroup::orderBy('id','desc')->first();
         $user_id =$user->id;
         // dd($user_id);
       
        $create=[
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'user_group_id'=>$user_id,
        ];
        // dd($create);
       
        $user =User::create($create);
        // dd($user);

        $ip = $this->getRequestIP();
                  $this->doActivityLog(
                      $user_group,
                      $user,
                      ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
                      'REGISTER',
                      'Register Successfully'
                  );

        return response()->json([
            'success'=>true,
            'message' =>trans('student.data_success'),                             
           ],200);

       }
        catch(Exception $e){
        Log::info($e->getMessage());
        return response()->json([
        'success'=>false,
        'message' =>trans('student.data_error'),
       ],500); 
       
      }
    }
}
