<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\LoginRequest;
use App\Traits\ActivityLog;
use App\Traits\Common;
use App\Models\User;
use Auth;
use Exception;
use Log;
use App\Traits\UserAuthenticationTokenProcess;
use App\Traits\UserDeviceTokenProcess;

class LoginController extends Controller
{
    use ActivityLog ,Common, UserAuthenticationTokenProcess, UserDeviceTokenProcess;
    
    public function login(LoginRequest $request){ 
        // dd($request);
        try{
               $token='';
               // dd($token);
             if((Auth::attempt(['email' => request('username'), 'password' => request('password')])))
           {

             
                $user = Auth::user();
                $token =  $user->createToken('ExampleApp')->plainTextToken;
                // dd($token);
                $this->checkDeviceToken($user,$request);
                // dd($token);
                 
                  // $token=$this->createToken($user);
            
                  }

                  $ip = $this->getRequestIP();
                  $this->doActivityLog(
                      $user,
                      $user,
                      ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
                      'LOGIN',
                      'Logged in'
                  );

                return response()->json([
                                'success'=>true,
                                'token' => $token,
                            ],200);
            
          }
          catch(Exception $e)
          {
             Log::info($e->getMessage());
                return response()->json([
                                'success'=>false,
                                'message' =>'Try after sometime',
                               ],500); 
          }
          
    }

    public function logout(Request $request)
    {
        // dd($request);
              if (Auth::user()) {
              $this->destroyAuthToken(Auth::user(),$request);
              $this->destroyDeviceTokens(Auth::user());

                return response()->json([
                  'success' => true,
                  'message' => 'Logout successfully',
              ],200);
              }else {
                return response()->json([
                  'success' => false,
                  'message' => 'Unable to Logout'
                ],500);
              }
     }
}
