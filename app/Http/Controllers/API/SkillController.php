<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\StudentSkillRequest;
use App\Http\Resources\API\StudentsSkill as StudentSkillResource;
use App\Traits\StudentSkillProcess;
use App\Models\StudentSkills;
use App\Traits\ActivityLog;
use App\Traits\Common;

use Illuminate\Http\Request;

class SkillController extends Controller
{
    use StudentSkillProcess, ActivityLog, Common;

    public function store(StudentSkillRequest $request,$id)
    {
        // dd($request->skill_item);
        try{
           // dd($request);
            $user =Auth()->user();
            $body = $request->getContent();
            // dd($body);
           $body_decode=json_decode($body,true); 

           // dd($body_decode); 
            $req = new \Illuminate\Http\Request($body_decode);
            // dd($req);
            $skill =$this->createSkill($req,$id);

          //   $ip = $this->getRequestIP();
           
          // $this->doActivityLog(
          //     $skill,
          //     $user,
          //     ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
          //     'ADD_STUDENT_SKILL',
          //     'Student skill added'
          // );

            return response()->json([
            'success'=>true,
            'message' =>trans('student.data_success'),                             
           ],200);
        }
        
        catch(Exception $e){
            Log::info($e->getMessage());

       return response()->json([
        'success'=>false,
        'message' =>trans('student.data_error'),
       ],500); 

        }
    }

    public function index(Request $request)
    {

        // $search=''; 
        // $student_skill =StudentSkills::paginate(10);
        if(isset($request->search)){
            $course =$request->search;
            $student_skill =StudentSkills::where('course_name',$course)->orWhere('experience',$course);
        }
       
        $student_skill =$student_skill->paginate(10);
        $student_skill =StudentSkillResource::collection($student_skill);
        // dd($student_skill);
        return $student_skill;
    }
}
