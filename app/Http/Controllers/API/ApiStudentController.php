<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\API\Student as StudentResource;
use App\Http\Requests\API\StudentDetailRequest;
use App\Models\StudentDetail;
use App\Traits\StudentProcess;
use Illuminate\Support\Facades\Mail;
use App\Mail\StudentInfoMail;
use App\Traits\ActivityLog;
use App\Traits\Common;
use Exception;
use Log;
use Auth;
use App\Events\StudentEvent;

class ApiStudentController extends Controller
{
    use StudentProcess, ActivityLog, Common;
 
    public function store(StudentDetailRequest $request)
    {
        // dd($request);
        try{
          $user=Auth::user();
          $user_id=$user->id;
          // dd($user);

        //   $create=[
        //     'name'=>$request->name,
        //     'email'=>$request->email,
        //     'mobileno'=>$request->mobileno,
        //     'gender'=>$request->gender,
        //     'address'=>$request->address,
        //     // 'user_id'=>$user_id,
        // ];
        // $student =StudentDetail::create($create);
        $student=$this->createStudent($request,$user_id); 
        
        // event(new StudentEvent($student));
        // Mail::to('ramamoorthi@example.com')->queue(new StudentInfoMail($student));

        $ip = $this->getRequestIP();
           
          $this->doActivityLog(
              $student,
              $user,
              ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
              'ADD_STUDENT',
              'Student Details added'
          );           
            

          return response()->json([
            'success'=>true,
            'message' =>trans('student.data_success'),                             
           ],200);
      }
      catch(Exception $e){
        Log::info($e->getMessage());

       return response()->json([
        'success'=>false,
        'message' =>trans('student.data_error'),
       ],500); 
       
      }   

    }
    public function index()
    {

        // dd(Auth::user());
       $student =StudentDetail::paginate(20);
       // dd($student);
       $student=StudentResource::collection($student);
       return $student;
    }
}
