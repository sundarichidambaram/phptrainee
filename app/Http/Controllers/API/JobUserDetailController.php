<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\JobUserDetailRequest;
use App\Traits\StudentProcess;
use Illuminate\Http\Request;
use App\Traits\ActivityLog;
use App\Traits\Common;
use Exception;
use Log;
use Auth;
class JobUserDetailController extends Controller
{
      use StudentProcess, ActivityLog, Common;
    //
     public function store(JobUserDetailRequest $request)
    {
    
        try{
            $user=Auth::user();
        $job_user=$this->createUserjob($request); 
                    
          return response()->json([
            'success'=>true,
            'message' =>trans('student.data_success'),                             
           ],200);
      }
      catch(Exception $e){
        Log::info($e->getMessage());

       return response()->json([
        'success'=>false,
        'message' =>trans('student.data_error'),
       ],500); 
       
      }   

    }
     public function stored(Request $request)
    {
    
        try{
            $user=Auth::user();
        $job_user=$this->createUserjobdetail($request); 
                    
          return response()->json([
            'success'=>true,
            'message' =>trans('student.data_success'),                             
           ],200);
      }
      catch(Exception $e){
        Log::info($e->getMessage());

       return response()->json([
        'success'=>false,
        'message' =>trans('student.data_error'),
       ],500); 
       
      }   

    }
}
