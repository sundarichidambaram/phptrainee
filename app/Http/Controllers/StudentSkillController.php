<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Redirect;
use App\Models\StudentDetail;
use App\Models\StudentSkills;

class StudentSkillController extends Controller
{
    public function create($id)
    {
        // dd($id);
        return view('student_skills',['student_id'=>$id]);
    }

    public function index($id)
    {
        // dd('hi');
        // $skill =StudentSkills::where('student_id',$id)->with('studentdetail')->get();
        // dd($skill);
        return view('student_skill_list',
            ['id'=>$id]);
    }
    public function destroy($id)
    {
        // dd($student_id);
        $student =StudentSkills::where('id',$id)->first();
        $student_id =$student->student_id;
        // dd($student_id);
         $skill =StudentSkills::where('id',$id)->delete();

         Toastr::success('Data has been Updated successfully! :)', 'Success!!');
        return Redirect::to(url('admin/skill/list/'.$student_id));
    }
}
