<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

use App\Models\Product;
use App\Models\Image;
use App\Models\Description;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProductMail;
use Brian2694\Toastr\Facades\Toastr;
use Exception;
use Log;








class ProductController extends Controller
{
    public function create(){
       // $product = Product::get();

        return view('product.create_form');

    } 

    public function store(ProductRequest $request){
            try{
            $product = new Product;
            $product->productname = $request->name;
            $product->price = $request->price;
            $product->product_code =$request->product_code;
            $product->save();
         } 
            catch(Exception $e){
               Log::info($e->getMessage());
            }
            Mail::to('ramamoorthi@example.com')->queue(new ProductMail($product));
            // \Session::put('successmessage','Successfully created');
            Toastr::success('Data has been saved successfully! :)', 'Success!!');
            return Redirect::to(url('product/'));

    }   

     public function show(){
        $product=Product::get();
        //dd($student);

        return view('product.show_list', ['product'=>$product]);

   }
    public function edit($id)
    {
        $product = Product::find($id);

        return view('product.edit_form', ['product'=> $product]
        );
    }

     public function update(ProductRequest $request,$id){
            try{
            $product = Product::find($id);
            $product->productname = $request->name;
            $product->price = $request->price;
                        $product->product_code =$request->product_code;
            $product->save();
          } 
             catch(Exception $e){
               Log::info($e->getMessage());
             }
            // \Session::put('successmessage','Successfully updated');
             Toastr::success('Data has been saved successfully! :)', 'Success!!');
            return Redirect::to(url('product/edit/'.$id));

    }   


  /*public function delete($id){

        $product=Product::find($id);
        $product->delete();
         \Session::put('successmessage','Successfully deleted');
            return Redirect::to(url('products/'));

    }*/
 
}
