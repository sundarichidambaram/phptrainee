<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImageuploadController extends Controller
{
    

    public function create()
    {
        return view('imageupload');
    }

      public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
  
        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('fileupload'), $imageName);
   
        return back()
            ->with('You have successfully upload file.')
            ->withImageName($imageName);
   
    }
}
