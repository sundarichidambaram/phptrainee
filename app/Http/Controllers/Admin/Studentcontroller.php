<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Http\Requests\StudentFormRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Models\Studentsubjects;
use App\Models\StudentAddress;
use App\Models\Profile;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Models\Country;
use App\Models\State;
use App\Mail\StudentMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Contact;
use Brian2694\Toastr\Facades\Toastr;
use App\Traits\ActivityLog;
use App\Traits\Common;
use App\Helpers\FileHelper;
use Exception;
use Log;
use Auth;

class Studentcontroller extends Controller
{
    use ActivityLog ,Common;
    
    protected function create(){


        //Mail::to('ramamoorthi@example.com')->send(new StudentMail());
        $countries=Country::get();
        $state=State::get();
       
        return view("student.create_form",
            ['countries'=>$countries],
            ['state' =>$state]);
        // $user=Auth::user();
        //      $lists=Student::orderby('id','desc')->get();
        //      $lists=StudentResource::collection($lists);
        //      return $lists;
    } 

        public function store(StudentFormRequest $request){
            // dd($request);
          try{
            $user =Auth::user();
            $student = new Student;
            $student->studentname = $request->sname;
            $student->email = $request->email;
            $student->password = bcrypt($request->password);
            $student->mobile = $request->mobileno;
            $student->age = $request->age;
            $student->gender = $request->gender;
            $student->dob = date('Y-m-d',strtotime($request->dob));
            $student->country = $request->country;
            $student->state = $request->state;
            $imageName = time().'.'.$request->file('image')->extension();  
            $request->file('image')->move(public_path('fileupload'), $imageName);
            // $file =$request->image;
            //  $path = FileHelper::uploadFile(env('FILESYSTEM_DISK'), 'avatar', $file);
            $student->image = $imageName;

            $student->save();

            $ip = $this->getRequestIP();
            // dd($ip);
          $this->doActivityLog(
              $student,
              $user,
              ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
              'ADD_STUDENT',
              'Student Details added'
          );
        }
        catch(Exception $e)
        {
            Log::info($e->getMessage());
        }
           // function(){
        // event(new StudentEvent($student));
                  Mail::to('ramamoorthi@example.com')->queue(new StudentMail($student));

            //}

           // return 'student created successfully. <a href="'.url('student'). '">click here</a>';
        // \Session::put('successmessage','Successfully created');
        Toastr::success('Data has been saved successfully! :)', 'Success!!');
        return Redirect::to(url('/admin/students/'));
               }


    public function show(){
        $student=Student::paginate(10);        //$countries=Country::get();

     //dd($student);

        return view('student.show_list', ['student'=>$student]);

    
    }
    public function edit($id)
    {
        $student = Student::find($id);
        $countries=Country::get();
       $states=State::get();
       // dd($states);
       
        //$state = State::find($id);

        return view('student.edit_form',
            ['student'=> $student,
            'countries'=>$countries,//,['states'=>$states]
            'state' => $states]
        );
    }


       public function update(StudentUpdateRequest $request, $id)
    {
        // dd($request);
        try{

            $user =Auth::user();
        $student = Student::find($id);
        $student->studentname = $request->sname;
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->mobile = $request->mobileno;
        $student->age = $request->age;
        $student->gender = $request->gender;
        $student->dob = $request->dob;
        $student->country = $request->country;
        // $student->state = $request->state;
        $imageName = time().'.'.$request->file('image')->extension();  
        $request->file('image')->move(public_path('fileupload'), $imageName);
        $student->image = $imageName;

        $student->save();
        $ip = $this->getRequestIP();
            // dd($ip);
          $this->doActivityLog(
              $student,
              $user,
              ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
              'UPDATE_STUDENT',
              'Student Details Updated'
          );
        }
        catch(Exception $e){
            Log::info($e->getMessage());
        }
        Mail::to('ramamoorthi@example.com')->queue(new StudentMail($student));
       // \Session::put('successmessage','Successfully updated');
        Toastr::success('Data has been Updated successfully! :)', 'Success!!');
        return Redirect::to(url('admin/students/'));
    }


    public function destroy($id){

       // $student=Studedent::find($id);
        $user =Auth::user();
        $student=Student::where('id',$id)->with('StudentAddress')->first();
        $student->delete();
        $ip = $this->getRequestIP();
            // dd($ip);
          $this->doActivityLog(
              $student,
              $user,
              ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
              'DELETE_STUDENT',
              'Student Detail Deleted'
          );

        //return 'Student Deleted Successfully!! <a href="'.url('').'">Click here</a>';
        // \Session::put('successmessage','Successfully updated');
        return Redirect::to(url('admin/students/'));

    }
    public function restores()
    {
        $student =Student::onlyTrashed()->restore();
        // dd($student);
        return Redirect::to(url('admin/students/'));

    }
    public function restorelist()
    {
        // dd('hi');
        $user =Auth::user();
        $student =Student::onlyTrashed()->get();

        // dd($contact);
        return view('student.delete_list',['students'=>$student]);
         // return Redirect::to(url('contacts/'));
    }
    public function restored($id)
    {
        // dd($id);
        $student =Student::onlyTrashed()->find($id);
        $student->restore();
        return Redirect::to(url('admin/delete/list'));
    }
    public function forcedelete($id)
    {
        $student =Student::onlyTrashed()->find($id);
        $student->forceDelete();
        return Redirect::to(url('admin/delete/list'));
    }

    public function contact()
    {
         $id='';
         // dd($id);
        return view('contact.contact_form',['id'=>$id]);
    }
    public function contacts()
    {
        return view('contact.contact_list');
    }
    public function dropdown()
    {
        return view('contact.dropdown');
    }
    public function imageupload()
    {
        // dd('hi');
        return view('contact.image_upload');
    }
    public function editcontact($id)
    {
        // dd($id);
        return view('contact.contact_form',['id'=>$id]);
    }
    public function deletecontact($id)
    {
        // dd('hi');
        $user =Auth::user();
        $contact = Contact::find( $id );
        $contact->delete();

        $ip = $this->getRequestIP();
            // dd($ip);
          $this->doActivityLog(
              $contact,
              $user,
              ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
              'DELETE_CONTACT',
              'Contact Detail Deleted'
          );

        Toastr::success('Data has been Deleted successfully! :)', 'Success!!');
         return Redirect::to(url('admin/contacts/'));
    }
    public function deletelist()
    {
        // dd('hi');
        // $user =Auth::user();
        $contact =Contact::onlyTrashed()->get();
        // dd($contact);
        return view('contact.restore_list',['contacts'=>$contact]);
         // return Redirect::to(url('contacts/'));
    }
    public function restore($id){
        $user =Auth::user();
        $contact=Contact::withTrashed()->find($id);
        $contact->restore();

        $ip = $this->getRequestIP();
            // dd($ip);
          $this->doActivityLog(
              $contact,
              $user,
              ['ip' => $ip, 'details' => $_SERVER['HTTP_USER_AGENT']],
              'RESTORE_CONTACT',
              'Contact Detail Restored'
          );

         Toastr::success('Data has been Restore successfully! :)', 'Success!!');
        return Redirect::to(url('admin/restore/list'));
    }
    public function wizard()
    {
        return view('wizard_form');
    }
    public function wizardlist()
    {
        return view('wizard_list');
    }
    public function multiform()
    {
        return view('multi_form');
    }


}
