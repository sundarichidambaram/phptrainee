<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\DescriptionRequest;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Image;
use App\Models\Description;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Brian2694\Toastr\Facades\Toastr;
use Exception;
use Log;





class DescriptionController extends Controller
{
     public function create($id){
        $description = Product::find($id);

        return view('product.description.create_description',['description'=>$description]);
    }

     public function store(DescriptionRequest $request,$id){
            try{
            $description = new Description;
            $description->product_id =$id;
            $description->description = $request->descript;
            $description->save();
        }
            catch(Exception $e){
                Log::info($e->getMessage());
            }
            // \Session::put('successmessage','Successfully created');
            Toastr::success('Description has been saved successfully! :)', 'Success!!');
            return Redirect::to(url('product/description/'.$id));

    }   

    public function show($id){
        $descript=Product::where('id',$id)->with('Description')->first();
        //dd($address);

        return view('product.description.show_description', ['descript'=>$descript]);


     }
         public function index($id){
        $description=Description::where('id',$id)->with('Product')->first();
        //dd($profile);

        return view('product.description.product_description', ['description'=>$description]);

    }
   
}
