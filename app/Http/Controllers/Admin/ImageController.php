<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Models\Product;
use App\Models\Description;
use App\Models\Image;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Brian2694\Toastr\Facades\Toastr;
use Exception;
use Log;





class ImageController extends Controller
{
    public function create($id){
        $image = Product::find($id);

        return view('product.image.create_image',['image'=>$image]);
    }
    public function store(ImageRequest $request,$id){
        try{

        $image = new Image;
        $image->product_id =$id;
        $extension= $request->file('image')->extension();
        $imageName = time().'.'.$request->file('image')->extension();  
        $request->file('image')->move(public_path('imageupload'), $imageName);
        $image->image = $imageName;
        //$imageType = $request->image->extension(); 
        //$request->image->move(public_path('imageupload'), $imageType);

         $image->image_type =$extension; 
       // dd($image);
        $image->save();

    } 
        catch(Exception $e){
            Log::info($e->getMessage());
        }
        // \Session::put('successmessage','Successfully created');
        Toastr::success('Image has been saved successfully! :)', 'Success!!');
        return Redirect::to(url('product/image/'.$id));


    }
    public function show($id){
        $image=Product::where('id',$id)->with('Image')->first();
        //dd($address);

        return view('product.image.show_image', ['image'=>$image]);
    }
      public function index(){
        $image=Image::with('product')->get();
        //dd($address);

        return view('product.image.product_image', ['images'=>$image]);


      }   
}
