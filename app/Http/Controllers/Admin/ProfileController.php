<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StudentProfileRequest;
use App\Models\Student;
use App\Models\Profile;
use App\Models\StudentAddress;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProfileMail;
use Brian2694\Toastr\Facades\Toastr;
use Exception;
use Log;






class ProfileController extends Controller
{
     public function create($id){
        $profile = Student::find($id);

        return view('student.profile.create_form',['profile'=>$profile]);
    }

    public function store(StudentProfileRequest $request,$id){
        try{
        $profile = new Profile;
        $profile->student_id =$id;
        $profile->studentname = $request->sname;
        $profile->email = $request->email;
        $profile->phoneno = $request->phoneno;
        $profile->city = $request->city;
        $profile->gender = $request->gender;
        $profile->department = $request->department;
        $profile->save();
        }
        catch(Exception $e){
            Log::info($e->getMessage());
        }
        
        Mail::to('ramamoorthi@example.com')->queue(new profileMail($profile));
        // \Session::put('successmessage','Successfully created');
        Toastr::success('Data has been saved successfully! :)', 'Success!!');
        return Redirect::to(url('profile/'.$id));




    }

    public function show($id){
         $profile=Student::where('id',$id)->with('Profile')->first();
         //dd($profile);

        return view('student.profile.show_form', ['profilelist'=>$profile]);




    }
    public function index($id){
        $profile=Profile::where('id',$id)->with('Student')->first();
        //dd($profile);

        return view('student.profile.list', ['profile'=>$profile]);


      } 

    public function addform()
    {
        return view('add_form');
    }   

}
