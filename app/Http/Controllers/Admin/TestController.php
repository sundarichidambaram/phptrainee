<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;

class TestController extends Controller
{
    //
    public function index(){
        global $collection;

       $collection = collect([
    [
        'id' => '1',
        'name' => 'Computer',
        'price'=>'50000',
        'description' => 'A computer is a machine or device that performs processes, calculations'
    ],
    [
         'id' => '2',
        'name' => 'Watch',
        'price'=>'3000',
        'description' => 'A watch is a portable timepiece intended to be carried or worn by a person'
    ],

    [
        'id' => '3',
        'name' => 'Car',
        'price'=> '500000',
        'description' => 'A car  is a wheeled motor vehicle that is used for transportation'
    ],

    [
         'id' => '4',
        'name' => 'Bike',
        'price'=> '100000',
        'description' => 'bicycle, also called bike, two-wheeled steerable machine that is pedaled by the riders feet'
    ],


    [
         'id' => '5',
        'name' => 'Laptop',
        'price'=> '30000',
        'description' => 'A laptop computer, sometimes called a notebook computer'
    ],


    [
         'id' => '6',
        'name' => 'Mobile',
        'price'=> '10000',
        'description' => 'Portable device for connecting to a telecommunications network in order to transmit and receive voice, video, or other data'
    ],

    [
         'id' => '7',
        'name' => 'Television',
        'price'=> '25000',
        'description' => 'The electronic delivery of moving images and sound from a source to a receiver.'
    ],


    [
         'id' => '8',
        'name' => 'Washing Machine',
        'price'=> '40000',
        'description' => 'A washing machine (laundry machine, clothes washer, washer, or simply wash) is a home appliance used to wash laundry'
    ],


    [
         'id' => '9',
        'name' => 'Air conditioner',
        'price'=> '19999',
        'description' => 'Air-conditioning is that process used to create and maintain certain temperature,
                         relative humidity and air purity conditions in indoor spaces'
    ],


    [
         'id' => '10',
        'name' => 'Bicycle',
        'price'=>'15000',
        'description' => 'bicycle, also called bike, two-wheeled steerable machine that is pedaled by the riders feet.'
    ],


    [
         'id' => '11',
        'name' => 'R15 Bike',
        'price'=> '300000',
        'description' => 'Most expensive Bike and fastest'
    ],


    [
         'id' => '12',
        'name' => 'Smart Watch',
        'price' =>'6000',
        'description' => 'A smartwatch is a wearable computing device that closely resembles a wristwatch or other time-keeping device'
    ],


    [
         'id' => '13',
        'name' => 'Rolce Royels',
        'price' =>'1000000',
        'description' => 'Most expensive car in the world'
    ],


    [
         'id' => '14',
        'name' => 'iphone',
        'price'=> '25000',
        'description' => 'Expensive mobile phone'
    ],


    [
         'id' => '15',
        'name' => 'Hero honda',
        'price' =>'89000',
        'description' => 'Motorcycle, also called bike, two-wheeled steerable machine that is pedaled by the riders feet.'
    ],


    [
         'id' => '16',
        'name' => 'umberlla',
        'price'=> '1000',
        'description' => 'umbrella, a portable, hand-held device that is used for protection against rain and sunlight.'
    ],


    [
         'id' => '17',
        'name' => 'Tesla',
        'price' => '500000',
        'description' => 'Automatic car'
    ],


    [
         'id' => '18',
        'name' => 'Bmw car',
        'price'=>'1500000',
        'description' => ' BMW is a German multinational manufacturer of performance luxury vehicles and motorcycles'
    ],


    [
         'id' => '19',
        'name' => 'Bus',
        'price' => '2500000',
        'description' => 'a large motor vehicle, having a long body, equipped with seats or benches for passengers, usually operating as part of a scheduled service'
    ],

    [
         'id' => '20',
        'name' => 'Airplane',
        'price'=> '100000000',
        'description' => 'airplane, also called aeroplane or plane, any of a class of fixed-wing aircraft that is heavier than air, propelled by a screw propeller or a high-velocity jet, and supported by the dynamic reaction of the air against its wings.'
    ],



       

    ]);

             //dd($collection);
         $sorted = $collection->sortBy('name');
 
         $sorted->values()->all();
         //dd($sorted);
                /* $rq =Request();
         $this->request = $rq;
         return $this;*/

           //$this->collection($collection);
   
 }

     public function collection(){
        
         $collection = $this->index();
         global $collection;
        //dd($collection);
        
      
             $filter_price = $collection->filter(function ($value) {
        return data_get($value, 'price') < 20000;
    });
    $filtered = $filter_price->all();
    dd($filtered);
 }
    public function asc(){

         $collection = $this->index();
         global $collection;
           $sorted = $collection->sortBy('price');
 
         $sorted->values()->all();
         //dd($sorted);

    }
    public function minimum(){

          $collection = $this->index();
          global $collection;

          $min = $collection->where('price', $collection->min('price'))->first();

          //dd($min);
    }
      public function maximum(){
          
          $collection = $this->index();
          global $collection;

          $max = $collection->where('price', $collection->max('price'))->first();

          //dd($max);
    }
    public function average(){
          
          $collection = $this->index();
          global $collection;

          $average = $collection->avg('price');

          //dd($average);
    }

    public function sum(){
          
          $collection = $this->index();
          global $collection;

          $sum = $collection->sum('price');

          //dd($sum);
    }
    public function string(){

       /* $original = 'aa',;
        $change = '@@';
        $string ='balmacaan aa';
        $result = str_replace($original, $change, $string);
        //dd($result);

        $name = 'ee';
        $symbol = '$$';*/


        $string = 'The event will take place between aa and ee';
        $search = array('aa','ee');
        $replace = array('@@','$$' );
        $replaced = ($search, $replace, $string);
        dd($replaced);
    }
}
