<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StudentAddressRequest;
use App\Models\Student;
use App\Models\StudentAddress;
use App\Models\Profile;
use Illuminate\Support\Facades\Mail;
use App\Mail\AddressMail;
use Brian2694\Toastr\Facades\Toastr;
use Exception;
use Log;




use Illuminate\Support\Facades\Redirect;

class AddressController extends Controller
{
     public function create($id){
        $address = Student::find($id);

        return view('student.address.create_form',['address'=>$address]);
    }
      

    public function store(StudentAddressRequest $request,$id)
    {   

      try{
        //dd($address);
        $address =  new StudentAddress;
        $address->student_id =$id;
        $address->address = $request->address;
        // $address->save();
         Mail::to('ramamoorthi@example.com')->queue(new AddressMail($address));
     } 
        catch(Exception $e){
          log::info($e->getMessage());
        }
        Mail::to('ramamoorthi@example.com')->queue(new AddressMail($address));
        // \Session::put('successmessage','Successfully created');
        Toastr::success('Data has been saved successfully! :)', 'Success!!');
        return Redirect::to(url('student/address/'.$id));
        
    }

     public function show($id){
        $address=Student::where('id',$id)->with('StudentAddress')->first();
        //dd($address);

        return view('student.address.show_form', ['addresslist'=>$address]);




    }

     public function belong(){
        $address=StudentAddress::with('Student')->get();
        //dd($address);

        return view('student.address.list', ['address'=>$address]);


      }   


}
