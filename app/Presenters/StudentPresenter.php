<?php
namespace App\Presenters;
use Laracasts\Presenter\Presenter;
use App\Helpers\FileHelper;

class StudentPresenter extends Presenter
{
    
    public function firstName(){
       
             return ucfirst($this->name);

    }
    public function firstAddress(){
       
             return ucfirst($this->address);

    }
    public function fullName()
    {   
        
        // $count =strlen($this->name);
        $html=$this->name.'<br>'.$this->email;
        // dd($html);
        // $html={!!("$this->name\n $this->email")!!};
        // $count =strlen($this->name);
        // // dd($count);
        // $html= wordwrap($htmls,5);
        return $html; 
    }
    public function mobilenumber()
    {
        $num ='+91';
        // dd($num);
        $number =$num.$this->mobileno;
        // dd($number);
        return $number;
    }
    public function getFilePathForDisplay($file)
    {
        // dd($file);
        return FileHelper::getFilePath(NULL,$file);
    }
}