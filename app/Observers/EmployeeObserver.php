<?php

namespace App\Observers;

use App\Models\Employee;
use App\Models\EmployeeLink;

class EmployeeObserver
{
    /**
     * Handle the Employee "created" event.
     *
     * @param  \App\Models\Employee  $employee
     * @return void
     */
    public function created(Employee $employee)
    {
        $address =request()->address;
        $employee_id =$employee->id;
        // dd($employee_id);
        $create=[
            'address'=> $address,
            'employee_id'=>$employee_id,
        ];
        EmployeeLink::create($create);
    }

    /**
     * Handle the Employee "updated" event.
     *
     * @param  \App\Models\Employee  $employee
     * @return void
     */
    public function updated(Employee $employee)
    {
        // dd($employee);
         $address =request()->address;
        $employee_id =$employee->id;
        // dd($employee_id);
        $data=EmployeeLink::where('employee_id',$employee_id)->first();
        $data->address =$address;
        $data->employee_id =$employee_id;
        $data->save();
    }

    /**
     * Handle the Employee "deleted" event.
     *
     * @param  \App\Models\Employee  $employee
     * @return void
     */
    public function deleted(Employee $employee)
    {
        $employee_id =$employee->id;
        // dd($employee);
        $employee_link =EmployeeLink::where('employee_id',$employee_id)->first();
        $employee_link->delete();
    }

    /**
     * Handle the Employee "restored" event.
     *
     * @param  \App\Models\Employee  $employee
     * @return void
     */
    public function restored(Employee $employee)
    {
        // dd($employee);
        $employee_id =$employee->id;
        $employee_link =EmployeeLink::where('employee_id',$employee_id)->onlyTrashed()->restore();
    }

    /**
     * Handle the Employee "force deleted" event.
     *
     * @param  \App\Models\Employee  $employee
     * @return void
     */
    public function forceDeleted(Employee $employee)
    {
        // $data =EmployeeLink::where('employee_id',$employee_id)->onlyTrashed()->forceDelete();
    }
}
