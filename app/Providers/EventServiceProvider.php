<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\StudentEvent;
use App\Listeners\SendMailNotification;
use App\Observers\StudentObserver;
use App\Observers\EmployeeObserver;
use App\Models\Student;
use App\Models\Employee;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        StudentEvent::class => [
            SendMailListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Student::observe(StudentObserver::class);
        Employee::observe(EmployeeObserver::class);
    }
}
